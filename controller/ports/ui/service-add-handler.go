// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"net/http"

	"go.nlx.io/nlx/controller/app/apiapp/command"
	"go.nlx.io/nlx/controller/app/uiapp/query"
)

func (s *Server) addServiceGetHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	inwayAddresses, err := s.uiApp.Queries.ListInwayAddresses.Handle(ctx, &query.ListInwayAddressesArgs{
		AuthData: authData,
	})
	if err != nil {
		s.logger.Error("get Inway addresses for form", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	page := addServicePage{
		BasePage:       s.basePage,
		InwayAddresses: inwayAddresses,
		Form: &AddServicePageForm{
			Name:         "",
			EndpointURL:  "",
			InwayAddress: "",
		},
	}

	page.render(w)
}

func (s *Server) addServicePostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	inwayAddresses, err := s.uiApp.Queries.ListInwayAddresses.Handle(ctx, &query.ListInwayAddressesArgs{
		AuthData: authData,
	})
	if err != nil {
		s.logger.Error("get Inway addresses for form", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	formValues, err := decodeAddServicePageForm(r)
	if err != nil {
		s.logger.Error("failed to read form values", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	page := addServicePage{
		BasePage:       s.basePage,
		InwayAddresses: inwayAddresses,
		Form:           formValues,
	}

	err = s.apiApp.Commands.CreateService.Handle(context.Background(), &command.CreateServiceArgs{
		AuthData:     authData,
		Name:         formValues.Name,
		EndpointURL:  formValues.EndpointURL,
		InwayAddress: formValues.InwayAddress,
	})
	if err != nil {
		s.logger.Error("create service", err)

		page.renderWithError(w, err)

		return
	}

	page.renderWithSuccess(s.i18n.Translate("The service has been added"), w)
}
