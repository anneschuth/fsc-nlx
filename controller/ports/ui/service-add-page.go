// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"net/http"

	"github.com/pkg/errors"
)

type addServicePage struct {
	*BasePage
	BaseAuthenticatedPage

	SuccessMessage string
	ErrorMessage   string
	InwayAddresses []string
	Form           *AddServicePageForm
}

type AddServicePageForm struct {
	Name         string
	EndpointURL  string
	InwayAddress string
}

func decodeAddServicePageForm(r *http.Request) (*AddServicePageForm, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse form")
	}

	var result AddServicePageForm

	err = decoder.Decode(&result, r.Form)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode form values")
	}

	return &result, nil
}

func (p *addServicePage) renderWithError(w http.ResponseWriter, err error) {
	statusCode, message := mapError(err)

	w.WriteHeader(statusCode)

	p.ErrorMessage = message

	p.render(w)
}

func (p *addServicePage) renderWithSuccess(successMessage string, w http.ResponseWriter) {
	p.SuccessMessage = successMessage
	p.render(w)
}

func (p *addServicePage) render(w http.ResponseWriter) {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/add-service.html",
		)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
}
