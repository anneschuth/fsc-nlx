// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/controller/app/apiapp/query"
)

//nolint:funlen // this is a test
func TestContractState(t *testing.T) {
	t.Parallel()

	var tests = map[string]struct {
		contract      *query.Contract
		expectedState contractState
		clock         clock.Clock
	}{
		"proposed": {
			contract: &query.Contract{
				CreatedAt:  time.Unix(1684938020, 0),
				ValidFrom:  time.Unix(1684938020, 0),
				ValidUntil: time.Unix(1694938020, 0),
				Peers: map[string]*query.Peer{
					"12345678901234567890": {
						ID:   "12345678901234567890",
						Name: "Peer A",
					},
					"12345678901234567891": {
						ID:   "12345678901234567891",
						Name: "Peer B",
					},
				},
				AcceptSignatures: map[string]query.Signature{},
				RejectSignatures: map[string]query.Signature{},
				RevokeSignatures: map[string]query.Signature{},
				HasRejected:      false,
				HasAccepted:      false,
				HasRevoked:       false,
				PeerRegistrationGrant: &query.PeerRegistrationGrant{
					DirectoryPeerID: "12345678901234567890",
					PeerID:          "12345678901234567891",
					PeerName:        "Peer B",
				},
			},

			clock:         clock.NewMock(time.Unix(1694938000, 0)),
			expectedState: proposed,
		},
		"valid": {
			//nolint:dupl // this is not a duplicate
			contract: &query.Contract{
				CreatedAt:  time.Unix(1684938020, 0),
				ValidFrom:  time.Unix(1684938020, 0),
				ValidUntil: time.Unix(1694938020, 0),
				Peers: map[string]*query.Peer{
					"12345678901234567890": {
						ID:   "12345678901234567890",
						Name: "Peer A",
					},
					"12345678901234567891": {
						ID:   "12345678901234567891",
						Name: "Peer B",
					},
				},
				AcceptSignatures: map[string]query.Signature{
					"12345678901234567890": {
						SignedAt: time.Unix(1684938020, 0),
					},
					"12345678901234567891": {
						SignedAt: time.Unix(1684938020, 0),
					},
				},
				RejectSignatures: map[string]query.Signature{},
				RevokeSignatures: map[string]query.Signature{},
				PeerRegistrationGrant: &query.PeerRegistrationGrant{
					DirectoryPeerID: "12345678901234567890",
					PeerID:          "12345678901234567891",
					PeerName:        "Peer B",
				},
			},
			expectedState: valid,
			clock:         clock.NewMock(time.Unix(1694938000, 0)),
		},
		"valid_delegation": {
			//nolint:dupl // this is not a duplicate
			contract: &query.Contract{
				CreatedAt:  time.Unix(1684938020, 0),
				ValidFrom:  time.Unix(1684938020, 0),
				ValidUntil: time.Unix(1694938020, 0),
				Peers: map[string]*query.Peer{
					"12345678901234567890": {
						ID:   "12345678901234567890",
						Name: "Peer A",
					},
					"12345678901234567891": {
						ID:   "12345678901234567891",
						Name: "Peer B",
					},
					"12345678901234567892": {
						ID:   "12345678901234567892",
						Name: "Peer C",
					},
				},
				AcceptSignatures: map[string]query.Signature{
					"12345678901234567890": {
						SignedAt: time.Unix(1684938020, 0),
					},
					"12345678901234567891": {
						SignedAt: time.Unix(1684938020, 0),
					},
					"12345678901234567892": {
						SignedAt: time.Unix(1684938020, 0),
					},
				},
				RejectSignatures: map[string]query.Signature{},
				RevokeSignatures: map[string]query.Signature{},
				DelegatedServiceConnectionGrants: []*query.DelegatedServiceConnectionGrant{
					{
						DelegatorPeerID:             "12345678901234567892",
						OutwayPeerID:                "12345678901234567891",
						OutwayCertificateThumbprint: "_0yCZurQcAtocDFSdbWcwhFJNzQHoZw1paeFr7D2G-c",
						ServicePeerID:               "12345678901234567890",
						ServiceName:                 "mock-service",
					},
				},
			},
			expectedState: valid,
			clock:         clock.NewMock(time.Unix(1694938000, 0)),
		},
		"rejected_because_of_signature": {
			contract: &query.Contract{
				CreatedAt:  time.Unix(1684938020, 0),
				ValidFrom:  time.Unix(1684938020, 0),
				ValidUntil: time.Unix(1694938020, 0),
				Peers: map[string]*query.Peer{
					"12345678901234567890": {
						ID:   "12345678901234567890",
						Name: "Peer A",
					},
					"12345678901234567891": {
						ID:   "12345678901234567891",
						Name: "Peer B",
					},
				},
				AcceptSignatures: map[string]query.Signature{
					"12345678901234567890": {
						SignedAt: time.Unix(1684938020, 0),
					},
				},
				RejectSignatures: map[string]query.Signature{
					"12345678901234567891": {
						SignedAt: time.Unix(1684938020, 0),
					},
				},
				RevokeSignatures: map[string]query.Signature{},
				PeerRegistrationGrant: &query.PeerRegistrationGrant{
					DirectoryPeerID: "12345678901234567890",
					PeerID:          "12345678901234567891",
					PeerName:        "Peer B",
				},
			},
			clock:         clock.NewMock(time.Unix(1694938000, 0)),
			expectedState: rejected,
		},
		"rejected_because_of_valid_until_reached": {
			contract: &query.Contract{
				CreatedAt:  time.Unix(1684938020, 0),
				ValidFrom:  time.Unix(1684938020, 0),
				ValidUntil: time.Unix(1694938020, 0),
				Peers: map[string]*query.Peer{
					"12345678901234567890": {
						ID:   "12345678901234567890",
						Name: "Peer A",
					},
					"12345678901234567891": {
						ID:   "12345678901234567891",
						Name: "Peer B",
					},
				},
				AcceptSignatures: map[string]query.Signature{
					"12345678901234567890": {
						SignedAt: time.Unix(1684938020, 0),
					},
				},
				RejectSignatures: map[string]query.Signature{},
				RevokeSignatures: map[string]query.Signature{},
				PeerRegistrationGrant: &query.PeerRegistrationGrant{
					DirectoryPeerID: "12345678901234567890",
					PeerID:          "12345678901234567891",
					PeerName:        "Peer B",
				},
			},
			clock:         clock.NewMock(time.Unix(1694938021, 0)),
			expectedState: rejected,
		},
		"revoked": {
			contract: &query.Contract{
				CreatedAt:  time.Unix(1684938020, 0),
				ValidFrom:  time.Unix(1684938020, 0),
				ValidUntil: time.Unix(1694938020, 0),
				Peers: map[string]*query.Peer{
					"12345678901234567890": {
						ID:   "12345678901234567890",
						Name: "Peer A",
					},
					"12345678901234567891": {
						ID:   "12345678901234567891",
						Name: "Peer B",
					},
				},
				AcceptSignatures: map[string]query.Signature{
					"12345678901234567890": {
						SignedAt: time.Unix(1684938020, 0),
					},
				},
				RejectSignatures: map[string]query.Signature{},
				RevokeSignatures: map[string]query.Signature{
					"12345678901234567891": {
						SignedAt: time.Unix(1684938020, 0),
					},
				},
				PeerRegistrationGrant: &query.PeerRegistrationGrant{
					DirectoryPeerID: "12345678901234567890",
					PeerID:          "12345678901234567891",
					PeerName:        "Peer B",
				},
			},
			clock:         clock.NewMock(time.Unix(1694938000, 0)),
			expectedState: revoked,
		},
		//nolint:dupl // is not a duplicate
		"expired": {
			contract: &query.Contract{
				CreatedAt:  time.Unix(1684938020, 0),
				ValidFrom:  time.Unix(1684938020, 0),
				ValidUntil: time.Unix(1694938020, 0),
				Peers: map[string]*query.Peer{
					"12345678901234567890": {
						ID:   "12345678901234567890",
						Name: "Peer A",
					},
					"12345678901234567891": {
						ID:   "12345678901234567891",
						Name: "Peer B",
					},
				},
				AcceptSignatures: map[string]query.Signature{
					"12345678901234567890": {
						SignedAt: time.Unix(1684938020, 0),
					},
					"12345678901234567891": {
						SignedAt: time.Unix(1684938020, 0),
					},
				},
				RejectSignatures: map[string]query.Signature{},
				RevokeSignatures: map[string]query.Signature{},
				PeerRegistrationGrant: &query.PeerRegistrationGrant{
					DirectoryPeerID: "12345678901234567890",
					PeerID:          "12345678901234567891",
					PeerName:        "Peer B",
				},
			},
			clock:         clock.NewMock(time.Unix(1694938021, 0)),
			expectedState: expired,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()
			state := getContractState(tt.contract, tt.clock)

			assert.Equal(t, tt.expectedState, state)
		})
	}
}
