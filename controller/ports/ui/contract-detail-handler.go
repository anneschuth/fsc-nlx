// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"

	"go.nlx.io/nlx/controller/app/apiapp/query"
)

type contractState string

const (
	rejected contractState = "Rejected"
	revoked  contractState = "Revoked"
	proposed contractState = "Proposed"
	valid    contractState = "Valid"
	expired  contractState = "Expired"
)

func (s *Server) contractDetailHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	contentHash := chi.URLParam(r, "hash")

	contract, err := s.apiApp.Queries.GetContract.Handle(ctx, &query.GetContractArgs{
		AuthData:    authData,
		ContentHash: contentHash,
	})
	if err != nil {
		s.logger.Error("could not get contract", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	id, err := uuid.FromBytes(contract.ID)
	if err != nil {
		s.logger.Error("could not render contract detail page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	peers := getPeers(contract)
	peerRegistrationGrants := getPeerRegistrationGrants(contract.PeerRegistrationGrant)
	servicePublicationGrants := getServicePublicationGrants(contract.ServicePublicationGrants)
	serviceConnectionGrants := getServiceConnectionGrants(contract.ServiceConnectionGrants)
	delegatedServiceConnectionGrants := getDelegatedServiceConnectionGrants(contract.DelegatedServiceConnectionGrants)
	delegatedServicePublicationGrants := getDelegatedServicePublicationGrants(contract.DelegatedServicePublicationGrants)

	state := getContractState(contract, s.clock)

	page := contractDetailPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathContractsPage,
		},
		ID:                                id.String(),
		State:                             string(state),
		Hash:                              contentHash,
		HashAlgorithm:                     hashAlgorithmString(contract.HashAlgorithm),
		GroupID:                           contract.GroupID,
		Organizations:                     peers,
		CreatedAt:                         contract.CreatedAt.Format("02-01-2006 om 15:04"),
		ValidFrom:                         contract.ValidFrom.Format("02-01-2006 om 15:04"),
		ValidUntil:                        contract.ValidUntil.Format("02-01-2006 om 15:04"),
		PeerRegistrationGrants:            peerRegistrationGrants,
		ServicePublicationGrants:          servicePublicationGrants,
		ServiceConnectionGrants:           serviceConnectionGrants,
		DelegatedServicePublicationGrants: delegatedServicePublicationGrants,
		DelegatedServiceConnectionGrants:  delegatedServiceConnectionGrants,
		DisableRejectButton:               contract.HasRejected || contract.HasAccepted,
		DisableAcceptButton:               contract.HasRejected || contract.HasAccepted || contract.HasRevoked,
		DisableRevokeButton:               contract.HasRevoked || !contract.HasAccepted,
		IsActive:                          state == valid,
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render contract detail page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}

func hashAlgorithmString(hashAlg query.HashAlg) string {
	switch hashAlg {
	case query.HashAlgSHA3_512:
		return "SHA3-512"
	default:
		return "Unknown"
	}
}

func getPeers(contract *query.Contract) []*Peer {
	peerMaps := map[string]*Peer{}

	for peerID, peer := range contract.Peers {
		contractPeer := &Peer{
			Name:   peer.Name,
			PeerID: peer.ID,
		}

		signature, ok := contract.RejectSignatures[peerID]
		if ok {
			contractPeer.RejectSignatureSignedAt = signature.SignedAt.Format("02-01-2006 om 15:04")
		}

		signature, ok = contract.AcceptSignatures[peerID]
		if ok {
			contractPeer.AcceptSignatureSignedAt = signature.SignedAt.Format("02-01-2006 om 15:04")
		}

		signature, ok = contract.RevokeSignatures[peerID]
		if ok {
			contractPeer.RevokeSignatureSignedAt = signature.SignedAt.Format("02-01-2006 om 15:04")
		}

		peerMaps[peerID] = contractPeer
	}

	peers := make([]*Peer, 0)

	for _, peer := range peerMaps {
		peers = append(peers, peer)
	}

	return peers
}

func getPeerRegistrationGrants(grant *query.PeerRegistrationGrant) []*PeerRegistrationGrant {
	result := make([]*PeerRegistrationGrant, 0)

	if grant != nil {
		result = append(result, &PeerRegistrationGrant{
			Hash:            grant.Hash,
			DirectoryPeerID: grant.DirectoryPeerID,
			PeerID:          grant.PeerID,
			PeerName:        grant.PeerName,
		})
	}

	return result
}

func getServicePublicationGrants(grants []*query.ServicePublicationGrant) []*ServicePublicationGrant {
	result := make([]*ServicePublicationGrant, len(grants))

	for i, grant := range grants {
		result[i] = &ServicePublicationGrant{
			Hash:            grant.Hash,
			DirectoryPeerID: grant.DirectoryPeerID,
			ServicePeerID:   grant.ServicePeerID,
			ServiceName:     grant.ServiceName,
		}
	}

	return result
}

func getServiceConnectionGrants(grants []*query.ServiceConnectionGrant) []*ServiceConnectionGrant {
	result := make([]*ServiceConnectionGrant, len(grants))

	for i, grant := range grants {
		result[i] = &ServiceConnectionGrant{
			Hash:                        grant.Hash,
			ServicePeerID:               grant.ServicePeerID,
			ServiceName:                 grant.ServiceName,
			OutwayPeerID:                grant.OutwayPeerID,
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
		}
	}

	return result
}

func getDelegatedServiceConnectionGrants(grants []*query.DelegatedServiceConnectionGrant) []*DelegatedServiceConnectionGrant {
	result := make([]*DelegatedServiceConnectionGrant, len(grants))

	for i, grant := range grants {
		result[i] = &DelegatedServiceConnectionGrant{
			Hash:                        grant.Hash,
			DelegatorPeerID:             grant.DelegatorPeerID,
			OutwayPeerID:                grant.OutwayPeerID,
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
			ServicePeerID:               grant.ServicePeerID,
			ServiceName:                 grant.ServiceName,
		}
	}

	return result
}

func getDelegatedServicePublicationGrants(grants []*query.DelegatedServicePublicationGrant) []*DelegatedServicePublicationGrant {
	result := make([]*DelegatedServicePublicationGrant, len(grants))

	for i, grant := range grants {
		result[i] = &DelegatedServicePublicationGrant{
			Hash:            grant.Hash,
			DirectoryPeerID: grant.DirectoryPeerID,
			DelegatorPeerID: grant.DelegatorPeerID,
			ServicePeerID:   grant.ServicePeerID,
			ServiceName:     grant.ServiceName,
		}
	}

	return result
}
