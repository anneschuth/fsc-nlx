// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"

	"go.nlx.io/nlx/common/logger"

	"go.nlx.io/nlx/controller/app/apiapp/query"
)

//nolint:dupl // looks the same as servicesHandler but is different.
func (s *Server) directoryHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	resp, err := s.apiApp.Queries.ListPeerServices.Handle(ctx, &query.ListPeerServicesArgs{
		AuthData: authData,
	})
	if err != nil {
		s.logger.Error("could not render directory page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	services := mapQueryToUIPeerServices(s.logger, resp)

	page := directoryPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathDirectoryPage,
			Title:                       s.i18n.Translate("Directory"),
			Description:                 "",
			Username:                    "admin",
			Peer:                        s.getPeerInfo(ctx, authData),
		},
		AmountOfServices: len(services),
		Services:         services,
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render directory page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}

func mapQueryToUIPeerServices(lgr *logger.Logger, qServices query.PeerServices) []*directoryPageService {
	services := make([]*directoryPageService, len(qServices))

	for i, s := range qServices {
		switch service := s.(type) {
		case *query.DelegatedPeerService:
			services[i] = &directoryPageService{
				Name:         service.Name,
				PeerID:       service.DelegatorID,
				PeerName:     service.DelegatorName,
				ProviderID:   service.PeerID,
				ProviderName: service.PeerName,
			}
		case *query.PeerService:
			services[i] = &directoryPageService{
				Name:     service.Name,
				PeerID:   service.PeerID,
				PeerName: service.PeerName,
			}
		default:
			lgr.Info(fmt.Sprintf("unknown service type %v", service))
		}
	}

	return services
}
