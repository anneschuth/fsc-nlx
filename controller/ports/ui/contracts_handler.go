// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"

	"github.com/google/uuid"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/controller/app/apiapp/query"
)

// nolint:dupl // this is not a duplicate of the directory
func (s *Server) contractsHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	grantTypeFilter := r.URL.Query().Get("grant_type")

	resp, err := s.apiApp.Queries.ListContracts.Handle(ctx, &query.ListContractsArgs{
		AuthData: authData,
		Filters: []*query.ListContractsFilter{{
			GrantType: query.GrantType(grantTypeFilter),
		}},
	})

	if err != nil {
		s.logger.Error("could not render contracts page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	contracts, err := mapQueryToUIContracts(resp, s.clock)
	if err != nil {
		s.logger.Error("could not render contracts page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	page := contractsPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathContractsPage,
			Title:                       s.i18n.Translate("Contracts"),
			Description:                 s.i18n.Translate("Manage the contracts of your organization."),
			Username:                    "admin",
			Peer:                        s.getPeerInfo(ctx, authData),
		},
		GrantTypeFilters: getGrantTypeFilters(),
		GrantType:        grantTypeFilter,
		Contracts:        contracts,
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render contracts page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}

func mapQueryToUIContracts(qContracts query.Contracts, clk clock.Clock) ([]*contractsPageContract, error) {
	contracts := make([]*contractsPageContract, len(qContracts))

	for i, c := range qContracts {
		id, err := uuid.FromBytes(c.ID)
		if err != nil {
			return nil, fmt.Errorf("failed to convert contract id from bytes to uuid: %w", err)
		}

		state := getContractState(c, clk)

		contract := &contractsPageContract{
			ID:                                id.String(),
			IsActive:                          state == valid,
			Hash:                              c.Hash,
			CreatedAt:                         c.CreatedAt.Format("02-01-2006 om 15:04"),
			ServicePublicationGrants:          make([]*ServicePublicationGrant, len(c.ServicePublicationGrants)),
			ServiceConnectionGrants:           make([]*ServiceConnectionGrant, len(c.ServiceConnectionGrants)),
			DelegatedServicePublicationGrants: make([]*DelegatedServicePublicationGrant, len(c.DelegatedServicePublicationGrants)),
			DelegatedServiceConnectionGrants:  make([]*DelegatedServiceConnectionGrant, len(c.DelegatedServiceConnectionGrants)),
		}

		if c.PeerRegistrationGrant != nil {
			grant := c.PeerRegistrationGrant

			contract.PeerRegistrationGrant = &PeerRegistrationGrant{
				DirectoryPeerID: grant.DirectoryPeerID,
				PeerID:          grant.PeerID,
				PeerName:        grant.PeerName,
			}
		}

		for i, grant := range c.ServicePublicationGrants {
			contract.ServicePublicationGrants[i] = &ServicePublicationGrant{
				DirectoryPeerID: grant.DirectoryPeerID,
				ServicePeerID:   grant.ServicePeerID,
				ServiceName:     grant.ServiceName,
			}
		}

		for i, grant := range c.ServiceConnectionGrants {
			contract.ServiceConnectionGrants[i] = &ServiceConnectionGrant{
				ServicePeerID:               grant.ServicePeerID,
				ServiceName:                 grant.ServiceName,
				OutwayPeerID:                grant.OutwayPeerID,
				OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
			}
		}

		for i, grant := range c.DelegatedServicePublicationGrants {
			contract.DelegatedServicePublicationGrants[i] = &DelegatedServicePublicationGrant{
				DirectoryPeerID: grant.DirectoryPeerID,
				DelegatorPeerID: grant.DelegatorPeerID,
				ServicePeerID:   grant.ServicePeerID,
				ServiceName:     grant.ServiceName,
			}
		}

		for i, grant := range c.DelegatedServiceConnectionGrants {
			contract.DelegatedServiceConnectionGrants[i] = &DelegatedServiceConnectionGrant{
				DelegatorPeerID:             grant.DelegatorPeerID,
				OutwayPeerID:                grant.OutwayPeerID,
				OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
				ServicePeerID:               grant.ServicePeerID,
				ServiceName:                 grant.ServiceName,
			}
		}

		contracts[i] = contract
	}

	return contracts, nil
}

func getGrantTypeFilters() map[query.GrantType]string {
	return map[query.GrantType]string{
		query.GrantTypePeerRegistration:            "Peer Registration",
		query.GrantTypeServicePublication:          "Service Publication",
		query.GrantTypeServiceConnection:           "Service Connection",
		query.GrantTypeDelegatedServicePublication: "Delegated Service Publication",
		query.GrantTypeDelegatedServiceConnection:  "Delegated Service Connection",
	}
}
