// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package authentication

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
)

type Authenticator interface {
	OnlyAuthenticated(h http.HandlerFunc) http.HandlerFunc
	MountRoutes(r chi.Router)
}

type ContextKey string

const ContextAuthenticationDataKey ContextKey = "authenticationData"

type AuthType string

const (
	AuthTypeUnspecified AuthType = ""
	AuthTypeNone        AuthType = "none"
	AuthTypeOIDC        AuthType = "oidc"
)

var authTypes []AuthType = []AuthType{AuthTypeNone, AuthTypeOIDC}

func ValidStringAuthTypes() []string {
	s := make([]string, len(authTypes))

	for i, a := range authTypes {
		s[i] = string(a)
	}

	return s
}

func AuthTypeFromString(t string) (AuthType, error) {
	switch AuthType(t) {
	case AuthTypeNone:
		return AuthTypeNone, nil
	case AuthTypeOIDC:
		return AuthTypeOIDC, nil
	default:
		return AuthTypeUnspecified, fmt.Errorf("")
	}
}
