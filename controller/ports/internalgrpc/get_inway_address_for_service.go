// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/ports/internalgrpc/api"
)

func (s *Server) GetInwayAddressForService(ctx context.Context, req *api.GetInwayAddressForServiceRequest) (*api.GetInwayAddressForServiceResponse, error) {
	s.logger.Info("rpc request GetInwayAddressForService")

	address, err := s.app.Queries.GetInwayAddressForService.Handle(ctx, req.GroupId, req.ServiceName)
	if err != nil {
		s.logger.Error(fmt.Sprintf("error executing get inway address for service query. groupID '%s', serviceName '%s'", req.GroupId, req.ServiceName), err)
		return nil, ResponseFromError(err)
	}

	return &api.GetInwayAddressForServiceResponse{Address: address.String()}, nil
}
