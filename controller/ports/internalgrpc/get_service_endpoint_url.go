// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"

	"go.nlx.io/nlx/controller/ports/internalgrpc/api"
)

func (s *Server) GetServiceEndpointURL(ctx context.Context, req *api.GetServiceEndpointURLRequest) (*api.GetServiceEndpointURLResponse, error) {
	s.logger.Info("rpc request GetServiceEndpointURL")

	endpointURL, err := s.app.Queries.GetServiceEndpointURL.Handle(ctx, req.ServiceName, req.InwayAddress)
	if err != nil {
		s.logger.Error("error executing get service endpoint URL query", err)
		return nil, ResponseFromError(err)
	}

	return &api.GetServiceEndpointURLResponse{EndpointUrl: endpointURL.String()}, nil
}
