// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/controller/app/uiapp/query"
	"go.nlx.io/nlx/controller/pkg/authentication"
)

// nolint:funlen // these tests should not fit 100 lines
func TestListInwayAddresses(t *testing.T) {
	t.Parallel()

	groupID := "directory1.com"

	validArgs := &query.ListInwayAddressesArgs{
		AuthData: &authentication.OIDCData{},
	}

	tests := map[string]struct {
		setup   func(ctx context.Context, m *mocks)
		args    *query.ListInwayAddressesArgs
		want    []string
		wantErr error
	}{
		"when_storage_errors": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.storage.
					EXPECT().
					ListInwayAddresses(ctx, groupID).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: &query.InternalError{},
		},
		"when_unauthenticated": {
			args: func(a query.ListInwayAddressesArgs) *query.ListInwayAddressesArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &query.AuthenticationError{},
		},
		"happy_flow": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.storage.EXPECT().
					ListInwayAddresses(ctx, groupID).
					Return([]string{"inway1.local.nlx:443", "inway2.local.nlx:443"}, nil)
			},
			want: []string{"inway1.local.nlx:443", "inway2.local.nlx:443"},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewListInwayAddressesHandler(groupID, mocks.storage)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			actual, err := h.Handle(ctx, tt.args)

			assert.Equal(t, tt.want, actual)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
