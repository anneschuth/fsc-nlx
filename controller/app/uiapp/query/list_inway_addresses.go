// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/controller/pkg/authentication"
)

type ListInwayAddressesHandler struct {
	storage storage.Storage
	groupID string
}

func NewListInwayAddressesHandler(groupID string, s storage.Storage) (*ListInwayAddressesHandler, error) {
	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	if groupID == "" {
		return nil, fmt.Errorf("group ID is required")
	}

	return &ListInwayAddressesHandler{
		groupID: groupID,
		storage: s,
	}, nil
}

type ListInwayAddressesArgs struct {
	AuthData authentication.Data
}

func (h *ListInwayAddressesHandler) Handle(ctx context.Context, args *ListInwayAddressesArgs) ([]string, error) {
	err := authentication.Authenticate(args.AuthData)
	if err != nil {
		return nil, mapError(err, "could not authenticate")
	}

	result, err := h.storage.ListInwayAddresses(ctx, h.groupID)
	if err != nil {
		return nil, mapError(err, "could not get inway addresses from storage")
	}

	return result, nil
}
