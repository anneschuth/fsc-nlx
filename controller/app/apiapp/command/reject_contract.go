// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // looks like accept contract but is different
package command

import (
	"context"
	"fmt"
	"go.nlx.io/nlx/common/logger"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/controller/pkg/authentication"
)

type RejectContractHandler struct {
	manager manager.Manager
	lgr     *logger.Logger
}

func NewRejectContractHandler(m manager.Manager, l *logger.Logger) (*RejectContractHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	return &RejectContractHandler{
		manager: m,
		lgr:     l,
	}, nil
}

type RejectContractArgs struct {
	AuthData    authentication.Data
	ContentHash string
}

func (h *RejectContractHandler) Handle(ctx context.Context, args *RejectContractArgs) error {
	err := authentication.Authenticate(args.AuthData)
	if err != nil {
		return mapError(err, "could not authenticate")
	}

	if args.ContentHash == "" {
		return newValidationError("contentHash cannot be empty")
	}

	err = h.manager.RejectContract(ctx, args.ContentHash)
	if err != nil {
		h.lgr.Error("failed to reject contract", err)

		return mapError(err, "could not reject contract in manager")
	}

	return nil
}
