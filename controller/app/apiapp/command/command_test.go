// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command_test

import (
	"go.nlx.io/nlx/common/logger"
	discard_logger "go.nlx.io/nlx/common/logger/discard"
	"testing"

	mock_clock "go.nlx.io/nlx/common/clock/mock"
	mock_idgenerator "go.nlx.io/nlx/controller/adapters/idgenerator/mock"
	mock_manager "go.nlx.io/nlx/controller/adapters/manager/mock"
	mock_storage "go.nlx.io/nlx/controller/adapters/storage/mock"
)

type mocks struct {
	manager *mock_manager.MockManager
	storage *mock_storage.MockStorage
	clock   *mock_clock.MockClock
	id      *mock_idgenerator.MockIDGenerator
	logger  *logger.Logger
}

func newMocks(t *testing.T) *mocks {
	return &mocks{
		manager: mock_manager.NewMockManager(t),
		storage: mock_storage.NewMockStorage(t),
		clock:   mock_clock.NewMockClock(t),
		id:      mock_idgenerator.NewMockIDGenerator(t),
		logger:  discard_logger.New(),
	}
}
