// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // looks like accept contract but is different
package command

import (
	"context"
	"fmt"
	"go.nlx.io/nlx/common/logger"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/controller/pkg/authentication"
)

type RevokeContractHandler struct {
	manager manager.Manager
	lgr     *logger.Logger
}

func NewRevokeContractHandler(m manager.Manager, l *logger.Logger) (*RevokeContractHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	return &RevokeContractHandler{
		manager: m,
		lgr:     l,
	}, nil
}

type RevokeContractArgs struct {
	AuthData    authentication.Data
	ContentHash string
}

func (h *RevokeContractHandler) Handle(ctx context.Context, args *RevokeContractArgs) error {
	err := authentication.Authenticate(args.AuthData)
	if err != nil {
		return mapError(err, "could not authenticate")
	}

	if args.ContentHash == "" {
		return newValidationError("contentHash cannot be empty")
	}

	err = h.manager.RevokeContract(ctx, args.ContentHash)
	if err != nil {
		h.lgr.Error("failed to revoke contract", err)

		return mapError(err, "could not revoke contract in manager")
	}

	return nil
}
