// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // looks like revoke contract but is different
package command

import (
	"context"
	"fmt"
	"go.nlx.io/nlx/common/logger"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/controller/pkg/authentication"
)

type AcceptContractHandler struct {
	manager manager.Manager
	lgr     *logger.Logger
}

func NewAcceptContractHandler(m manager.Manager, l *logger.Logger) (*AcceptContractHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	return &AcceptContractHandler{
		manager: m,
		lgr:     l,
	}, nil
}

type AcceptContractArgs struct {
	AuthData    authentication.Data
	ContentHash string
}

func (h *AcceptContractHandler) Handle(ctx context.Context, args *AcceptContractArgs) error {
	err := authentication.Authenticate(args.AuthData)
	if err != nil {
		return mapError(err, "could not authenticate")
	}

	if args.ContentHash == "" {
		return newValidationError("contentHash cannot be empty")
	}

	err = h.manager.AcceptContract(ctx, args.ContentHash)
	if err != nil {
		h.lgr.Error("failed to accept contract", err)

		return mapError(err, "could not accept contract in manager")
	}

	return nil
}
