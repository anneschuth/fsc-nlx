// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"errors"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/controller/pkg/authentication"
)

type ValidationError struct {
	m string
}

func newValidationError(message string) *ValidationError {
	return &ValidationError{
		m: message,
	}
}

func (v *ValidationError) Error() string {
	return "validation error: " + v.m
}

type InternalError struct {
	m string
}

func newInternalError(message string) *InternalError {
	return &InternalError{
		m: message,
	}
}

func (v *InternalError) Error() string {
	return "internal error: " + v.m
}

type AuthenticationError struct {
	m string
}

func newAuthenticationError(message string) *AuthenticationError {
	return &AuthenticationError{
		m: message,
	}
}

func (v *AuthenticationError) Error() string {
	return "authentication error: " + v.m
}

func mapError(err error, message string) error {
	var e *manager.ValidationError
	if errors.As(err, &e) {
		return fmt.Errorf("%s: %s: %w", message, newValidationError(e.Error()), err)
	}

	var a *authentication.AuthenticationError
	if errors.As(err, &a) {
		return fmt.Errorf("%s: %s: %w", message, newAuthenticationError(a.Error()), err)
	}

	return fmt.Errorf("%s: %w", newInternalError(message), err)
}
