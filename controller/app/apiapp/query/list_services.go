// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"net/url"

	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/controller/pkg/authentication"
)

type ListServicesHandler struct {
	storage storage.Storage
	groupID string
}

type Services []*Service

type Service struct {
	Name         string
	EndpointURL  *url.URL
	InwayAddress string
}

func NewListServicesHandler(groupID string, s storage.Storage) (*ListServicesHandler, error) {
	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	if groupID == "" {
		return nil, fmt.Errorf("group ID is required")
	}

	return &ListServicesHandler{
		storage: s,
		groupID: groupID,
	}, nil
}

type ListServicesArgs struct {
	AuthData authentication.Data
}

func (h *ListServicesHandler) Handle(ctx context.Context, args *ListServicesArgs) (Services, error) {
	err := authentication.Authenticate(args.AuthData)
	if err != nil {
		return nil, mapError(err, "could not authenticate")
	}

	services, err := h.storage.ListServices(ctx, h.groupID)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get services from storage"), err)
	}

	resp := make(Services, len(services))

	for i, s := range services {
		resp[i] = &Service{
			Name:         s.Name,
			EndpointURL:  s.EndpointURL,
			InwayAddress: s.InwayAddress,
		}
	}

	return resp, nil
}
