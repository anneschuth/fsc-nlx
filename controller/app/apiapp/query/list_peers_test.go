/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package query_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/controller/app/apiapp/query"
	"go.nlx.io/nlx/controller/pkg/authentication"
)

// nolint:funlen,dupl // these tests should not fit 100 lines
func TestListPeers(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListPeersArgs{
		AuthData: &authentication.OIDCData{},
	}

	tests := map[string]struct {
		setup   func(context.Context, *mocks)
		args    *query.ListPeersArgs
		want    query.ListPeersPeers
		wantErr error
	}{
		"when_storage_errors": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListPeers(ctx).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: &query.InternalError{},
		},
		"when_unauthenticated": {
			args: func(a query.ListPeersArgs) *query.ListPeersArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &query.AuthenticationError{},
		},
		"happy_flow_one": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListPeers(ctx).
					Return(manager.Peers{
						{
							ID:   "1",
							Name: "PeerOne",
						},
					}, nil)
			},
			want: query.ListPeersPeers{
				{
					ID:   "1",
					Name: "PeerOne",
				},
			},
		},
		"happy_flow_multiple": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListPeers(ctx).
					Return(manager.Peers{
						{
							ID:   "1",
							Name: "PeerOne",
						},
						{
							ID:   "2",
							Name: "PeerTwo",
						},
					}, nil)
			},
			want: query.ListPeersPeers{
				{
					ID:   "1",
					Name: "PeerOne",
				},
				{
					ID:   "2",
					Name: "PeerTwo",
				},
			},
		},
	}

	for name, testcase := range tests {
		testcase := testcase

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewListPeersHandler(mocks.manager, mocks.logger)
			require.NoError(t, err)

			ctx := context.Background()

			if testcase.setup != nil {
				testcase.setup(ctx, mocks)
			}

			actual, err := h.Handle(ctx, testcase.args)

			if testcase.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, testcase.want, actual)
			} else {
				assert.ErrorAs(t, err, &testcase.wantErr)
			}
		})
	}
}
