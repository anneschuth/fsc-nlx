// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/controller/app/apiapp/query"
	"go.nlx.io/nlx/controller/pkg/authentication"
)

// nolint:funlen // these tests should not fit 100 lines
func TestListServices(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListServicesArgs{
		AuthData: &authentication.OIDCData{},
	}

	tests := map[string]struct {
		setup   func(context.Context, *mocks)
		args    *query.ListServicesArgs
		want    query.Services
		wantErr error
	}{
		"when_storage_errors": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.storage.EXPECT().
					ListServices(ctx, "directory1.com").
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: &query.InternalError{},
		},
		"when_unauthenticated": {
			args: func(a query.ListServicesArgs) *query.ListServicesArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &query.AuthenticationError{},
		},
		"happy_flow_one": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.storage.EXPECT().
					ListServices(ctx, "directory1.com").
					Return(storage.Services{
						{
							Name:         "service-1",
							EndpointURL:  newURL(t, "api1.com"),
							InwayAddress: "inway1.com",
						},
					}, nil)
			},
			want: query.Services{
				{
					Name:         "service-1",
					EndpointURL:  newURL(t, "api1.com"),
					InwayAddress: "inway1.com",
				},
			},
		},
		"happy_flow_multiple": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.storage.EXPECT().
					ListServices(ctx, "directory1.com").
					Return(storage.Services{
						{
							Name:         "service-1",
							EndpointURL:  newURL(t, "api1.com"),
							InwayAddress: "inway1.com",
						},
						{
							Name:         "service-2",
							EndpointURL:  newURL(t, "api2.com"),
							InwayAddress: "inway2.com",
						},
						{
							Name:         "service-3",
							EndpointURL:  newURL(t, "api3.com"),
							InwayAddress: "inway3.com",
						},
					}, nil)
			},
			want: query.Services{
				{
					Name:         "service-1",
					EndpointURL:  newURL(t, "api1.com"),
					InwayAddress: "inway1.com",
				},
				{
					Name:         "service-2",
					EndpointURL:  newURL(t, "api2.com"),
					InwayAddress: "inway2.com",
				},
				{
					Name:         "service-3",
					EndpointURL:  newURL(t, "api3.com"),
					InwayAddress: "inway3.com",
				},
			},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewListServicesHandler("directory1.com", mocks.storage)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			actual, err := h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, actual)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
