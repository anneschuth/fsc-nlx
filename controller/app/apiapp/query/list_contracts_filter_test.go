// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/controller/app/apiapp/query"
	"go.nlx.io/nlx/controller/pkg/authentication"
)

// nolint:funlen,dupl // these tests do not fit in 100 lines
func TestListContractsFilters(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListContractsArgs{
		AuthData: &authentication.OIDCData{},
		Filters: []*query.ListContractsFilter{
			{
				ContentHash: "content-hash",
			},
			{
				GrantType: query.GrantTypePeerRegistration,
			},
			{
				GrantType: query.GrantTypeServicePublication,
			},
			{
				GrantType: query.GrantTypeServiceConnection,
			},
			{
				GrantType: query.GrantTypeDelegatedServicePublication,
			},
			{
				GrantType: query.GrantTypeDelegatedServiceConnection,
			},
		},
	}

	var tests = map[string]struct {
		args    *query.ListContractsArgs
		setup   func(context.Context, *mocks)
		want    query.Contracts
		wantErr error
	}{
		"happy_flow_single_filter": {
			args: func(a query.ListContractsArgs) *query.ListContractsArgs {
				a.Filters = []*query.ListContractsFilter{
					{
						GrantType: query.GrantTypeServiceConnection,
					},
				}
				return &a
			}(*validArgs),
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListContracts(ctx, []string{}, []manager.GrantType{
						manager.GrantTypeServiceConnection,
					}).
					Return(manager.Contracts{
						{
							ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb25"),
							GroupID:       "mock-group",
							Hash:          "hash-1",
							HashAlgorithm: manager.HashAlgSHA3_512,
							CreatedAt:     time.Unix(1684938019, 0),
							ValidFrom:     time.Unix(1684938019, 0),
							ValidUntil:    time.Unix(1684939000, 0),
							Peers: map[string]*manager.Peer{
								"12345678901234567890": {
									ID:   "12345678901234567890",
									Name: "Peer A",
								},
								"12345678901234567899": {
									ID:   "12345678901234567899",
									Name: "Peer B",
								},
							},
							RevokeSignatures: map[string]manager.Signature{},
							AcceptSignatures: map[string]manager.Signature{
								"12345678901234567890": {
									SignedAt: time.Unix(1684938020, 0),
								},
							},
							RejectSignatures: map[string]manager.Signature{},
							PeerRegistrationGrant: &manager.PeerRegistrationGrant{
								DirectoryPeerID: "12345678901234567899",
								PeerID:          "12345678901234567890",
								PeerName:        "Peer A",
							},
						},
					}, nil)
			},
			want: query.Contracts{
				{
					ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb25"),
					GroupID:       "mock-group",
					Hash:          "hash-1",
					HashAlgorithm: query.HashAlgSHA3_512,
					CreatedAt:     time.Unix(1684938019, 0),
					ValidFrom:     time.Unix(1684938019, 0),
					ValidUntil:    time.Unix(1684939000, 0),
					Peers: map[string]*query.Peer{
						"12345678901234567890": {
							ID:   "12345678901234567890",
							Name: "Peer A",
						},
						"12345678901234567899": {
							ID:   "12345678901234567899",
							Name: "Peer B",
						},
					},
					RejectSignatures: map[string]query.Signature{},
					AcceptSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938020, 0),
						},
					},
					RevokeSignatures: map[string]query.Signature{},
					PeerRegistrationGrant: &query.PeerRegistrationGrant{
						DirectoryPeerID: "12345678901234567899",
						PeerID:          "12345678901234567890",
						PeerName:        "Peer A",
					},
					ServicePublicationGrants:          []*query.ServicePublicationGrant{},
					ServiceConnectionGrants:           []*query.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants:  []*query.DelegatedServiceConnectionGrant{},
				},
			},
		},
		"happy_flow": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListContracts(ctx, []string{"content-hash"}, []manager.GrantType{
						manager.GrantTypePeerRegistration,
						manager.GrantTypeServicePublication,
						manager.GrantTypeServiceConnection,
						manager.GrantTypeDelegatedServicePublication,
						manager.GrantTypeDelegatedServiceConnection,
					}).
					Return(manager.Contracts{
						{
							ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb25"),
							GroupID:       "mock-group",
							Hash:          "hash-1",
							HashAlgorithm: manager.HashAlgSHA3_512,
							CreatedAt:     time.Unix(1684938019, 0),
							ValidFrom:     time.Unix(1684938019, 0),
							ValidUntil:    time.Unix(1684939000, 0),
							Peers: map[string]*manager.Peer{
								"12345678901234567890": {
									ID:   "12345678901234567890",
									Name: "Peer A",
								},
								"12345678901234567899": {
									ID:   "12345678901234567899",
									Name: "Peer B",
								},
							},
							RevokeSignatures: map[string]manager.Signature{},
							AcceptSignatures: map[string]manager.Signature{
								"12345678901234567890": {
									SignedAt: time.Unix(1684938020, 0),
								},
							},
							RejectSignatures: map[string]manager.Signature{},
							PeerRegistrationGrant: &manager.PeerRegistrationGrant{
								DirectoryPeerID: "12345678901234567899",
								PeerID:          "12345678901234567890",
								PeerName:        "Peer A",
							},
						},
					}, nil)
			},
			want: query.Contracts{
				{
					ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb25"),
					GroupID:       "mock-group",
					Hash:          "hash-1",
					HashAlgorithm: query.HashAlgSHA3_512,
					CreatedAt:     time.Unix(1684938019, 0),
					ValidFrom:     time.Unix(1684938019, 0),
					ValidUntil:    time.Unix(1684939000, 0),
					Peers: map[string]*query.Peer{
						"12345678901234567890": {
							ID:   "12345678901234567890",
							Name: "Peer A",
						},
						"12345678901234567899": {
							ID:   "12345678901234567899",
							Name: "Peer B",
						},
					},
					RejectSignatures: map[string]query.Signature{},
					AcceptSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938020, 0),
						},
					},
					RevokeSignatures: map[string]query.Signature{},
					PeerRegistrationGrant: &query.PeerRegistrationGrant{
						DirectoryPeerID: "12345678901234567899",
						PeerID:          "12345678901234567890",
						PeerName:        "Peer A",
					},
					ServicePublicationGrants:          []*query.ServicePublicationGrant{},
					ServiceConnectionGrants:           []*query.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants:  []*query.DelegatedServiceConnectionGrant{},
				},
			},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewListContractsHandler(mocks.manager)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			actual, err := h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, actual)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
