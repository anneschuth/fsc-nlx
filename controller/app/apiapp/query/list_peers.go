/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package query

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/common/logger"
	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/controller/pkg/authentication"
)

type ListPeersHandler struct {
	manager manager.Manager
	logger  *logger.Logger
}

type ListPeersPeers []*ListPeersPeer

type ListPeersPeer struct {
	ID   string
	Name string
}

func NewListPeersHandler(mngr manager.Manager, lgr *logger.Logger) (*ListPeersHandler, error) {
	if mngr == nil {
		return nil, errors.New("manager is required")
	}

	if lgr == nil {
		return nil, errors.New("logger is required")
	}

	return &ListPeersHandler{
		manager: mngr,
		logger:  lgr,
	}, nil
}

type ListPeersArgs struct {
	AuthData authentication.Data
}

func (h *ListPeersHandler) Handle(ctx context.Context, args *ListPeersArgs) (ListPeersPeers, error) {
	err := authentication.Authenticate(args.AuthData)
	if err != nil {
		return nil, mapError(err, "could not authenticate")
	}

	res, err := h.manager.ListPeers(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "could not list peers from manager")
	}

	peers := make(ListPeersPeers, 0, len(res))

	for _, peer := range res {
		peers = append(peers, &ListPeersPeer{
			ID:   peer.ID,
			Name: peer.Name,
		})
	}

	return peers, nil
}
