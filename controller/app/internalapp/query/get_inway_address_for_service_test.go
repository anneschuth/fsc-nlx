// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"errors"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/controller/app/internalapp/query"
)

func TestGetInwayAddressForService(t *testing.T) {
	t.Parallel()

	urlInway1, err := url.Parse("inway1.com")
	assert.NoError(t, err)

	tests := map[string]struct {
		setup   func(*mocks) context.Context
		want    *url.URL
		wantErr error
	}{
		"when_storage_errors": {
			setup: func(m *mocks) context.Context {
				ctx := context.Background()

				m.storage.EXPECT().
					GetInwayAddressForService(ctx, "directory1.com", "service-1").
					Return(nil, errors.New("unexpected error"))

				return ctx
			},
			wantErr: query.ErrInternalError,
		},
		"happy_flow": {
			setup: func(m *mocks) context.Context {
				ctx := context.Background()

				m.storage.EXPECT().
					GetInwayAddressForService(ctx, "directory1.com", "service-1").
					Return(urlInway1, nil)

				return ctx
			},
			want: urlInway1,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewGetInwayAddressForServiceHandler(mocks.storage)
			require.NoError(t, err)

			ctx := tt.setup(mocks)

			actual, err := h.Handle(ctx, "directory1.com", "service-1")

			assert.ErrorIs(t, err, tt.wantErr)
			assert.Equal(t, tt.want, actual)
		})
	}
}
