// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package commands

import "errors"

var ErrInternalError = errors.New("internal error")
var ErrValidationError = errors.New("validation error")
