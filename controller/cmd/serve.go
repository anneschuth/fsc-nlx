// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	oidc_server "github.com/coreos/go-oidc/v3/oidc"
	"github.com/spf13/cobra"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/common/cmd"
	zaplogger "go.nlx.io/nlx/common/logger/zap"
	"go.nlx.io/nlx/common/logoptions"
	"go.nlx.io/nlx/common/process"
	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/common/version"
	uuidgenerator "go.nlx.io/nlx/controller/adapters/idgenerator/uuid"
	grpcmanager "go.nlx.io/nlx/controller/adapters/manager/grpc"
	postgresstorage "go.nlx.io/nlx/controller/adapters/storage/postgres"
	"go.nlx.io/nlx/controller/app/apiapp"
	"go.nlx.io/nlx/controller/app/internalapp"
	"go.nlx.io/nlx/controller/app/uiapp"
	managerclient "go.nlx.io/nlx/controller/pkg/manager"
	"go.nlx.io/nlx/controller/ports/internalgrpc"
	"go.nlx.io/nlx/controller/ports/rest"
	uiport "go.nlx.io/nlx/controller/ports/ui"
	"go.nlx.io/nlx/controller/ports/ui/authentication"
	"go.nlx.io/nlx/controller/ports/ui/authentication/none"
	"go.nlx.io/nlx/controller/ports/ui/authentication/oidc"
)

type oidcOptions oidc.Options

var serveOpts struct {
	ListenAddressUI       string
	ListenAddressInternal string
	ListenAddressAPI      string
	GroupID               string
	ManagerAddress        string
	Environment           string
	StaticPath            string

	StoragePostgresDSN string

	AuthenticationType string
	oidcOptions

	logoptions.LogOptions
	cmd.TLSOptions
}

//nolint:gochecknoinits,funlen,gocyclo // this is the recommended way to use cobra, also a lot of flags..
func init() {
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressUI, "listen-address-ui", "", "127.0.0.1:3001", "Address for the UI to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressInternal, "listen-address-internal", "", "127.0.0.1:443", "Address for the internal api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressAPI, "listen-address-api", "", "127.0.0.1:444", "Address for the api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")

	serveCommand.Flags().StringVarP(&serveOpts.GroupID, "group-id", "", "", "Group ID of the FSC Group")
	serveCommand.Flags().StringVarP(&serveOpts.ManagerAddress, "manager-address-internal", "", "", "URL to the Manager internal API")

	serveCommand.Flags().StringVarP(&serveOpts.StoragePostgresDSN, "storage-postgres-dsn", "", "", "Postgres Connection URL")

	serveCommand.Flags().StringVarP(&serveOpts.StaticPath, "static-path", "", "public", "Path to the static web files")

	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogType, "log-type", "", "live", "Set the logging config. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger.")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogLevel, "log-level", "", "debug", "Set loglevel")

	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.RootCertFile, "tls-root-cert", "", "", "Absolute or relative path to the root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.CertFile, "tls-cert", "", "", "Absolute or relative path to the cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.KeyFile, "tls-key", "", "", "Absolute or relative path to the key .pem")

	serveCommand.Flags().StringVarP(&serveOpts.AuthenticationType, "authn-type", "", "none", fmt.Sprintf("Type of the authentication adapter, valid values: %s", strings.Join(authentication.ValidStringAuthTypes(), ", ")))
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.SecretKey, "authn-oidc-secret-key", "", "", "Secret key that is used for signing sessions")
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.ClientID, "authn-oidc-client-id", "", "", "The OIDC client ID")
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.ClientSecret, "authn-oidc-client-secret", "", "", "The OIDC client secret")
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.DiscoveryURL, "authn-oidc-discovery-url", "", "", "The OIDC discovery URL")
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.RedirectURL, "authn-oidc-redirect-url", "", "", "The OIDC redirect URL")
	serveCommand.Flags().BoolVarP(&serveOpts.oidcOptions.SessionCookieSecure, "authn-oidc-session-cookie-secure", "", false, "Use 'secure' cookies")

	if err := serveCommand.MarkFlagRequired("group-id"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("manager-address-internal"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("storage-postgres-dsn"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-root-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-key"); err != nil {
		log.Fatal(err)
	}
}

var serveCommand = &cobra.Command{
	Use:   "serve",
	Short: "Start the UI",
	Run: func(cmd *cobra.Command, args []string) {
		p := process.NewProcess()

		logger, err := zaplogger.New(serveOpts.LogOptions.LogLevel, serveOpts.LogOptions.LogType)
		if err != nil {
			log.Fatalf("failed to create logger: %v", err)
		}

		logger.Info(fmt.Sprintf("version info: version: %s source-hash: %s", version.BuildVersion, version.BuildSourceHash))

		if errValidate := common_tls.VerifyPrivateKeyPermissions(serveOpts.KeyFile); errValidate != nil {
			logger.Warn("invalid key permissions", err)
		}

		cert, err := common_tls.NewBundleFromFiles(serveOpts.CertFile, serveOpts.KeyFile, serveOpts.RootCertFile)
		if err != nil {
			logger.Fatal("loading internal TLS files", err)
		}

		ctx := context.Background()

		managerClient, err := managerclient.NewClient(ctx, serveOpts.ManagerAddress, cert)
		if err != nil {
			logger.Fatal("could not create manager client", err)
		}

		mgr, err := grpcmanager.New(managerClient, logger)
		if err != nil {
			logger.Fatal("could not create grpc manager", err)
		}

		db, err := postgresstorage.NewConnection(serveOpts.StoragePostgresDSN)
		if err != nil {
			logger.Fatal("could not create db connection", err)
		}

		storage, err := postgresstorage.New(db)
		if err != nil {
			logger.Fatal("could not create postgres storage", err)
		}

		idGenerator := uuidgenerator.New()

		app, err := apiapp.NewApplication(ctx, &apiapp.NewApplicationArgs{
			Manager:     mgr,
			Storage:     storage,
			Clock:       clock.New(),
			IDGenerator: idGenerator,
			Logger:      logger,
			GroupID:     serveOpts.GroupID,
		})
		if err != nil {
			logger.Fatal("could not create application", err)
		}

		workDir, err := os.Getwd()
		if err != nil {
			logger.Fatal("failed to get work dir", err)
		}

		staticFilesPath := filepath.Join(workDir, serveOpts.StaticPath)

		uiApp, err := uiapp.NewApplication(&uiapp.NewApplicationArgs{
			Context: ctx,
			Storage: storage,
			GroupID: serveOpts.GroupID,
		})

		if err != nil {
			logger.Fatal("failed to create ui app", err)
		}

		authType, err := authentication.AuthTypeFromString(serveOpts.AuthenticationType)
		if err != nil {
			logger.Fatal("unknown authentication type", err)
		}

		var authn authentication.Authenticator

		switch authType {
		case authentication.AuthTypeNone:
			authn = none.New()
		case authentication.AuthTypeOIDC:
			var oidcProvider *oidc_server.Provider

			oidcProvider, err = oidc_server.NewProvider(context.Background(), serveOpts.oidcOptions.DiscoveryURL)
			if err != nil {
				logger.Fatal("could not initialize OIDC provider", err)
			}

			oidcConfig := &oidc_server.Config{
				ClientID: serveOpts.oidcOptions.ClientID,
			}

			verifier := oidcProvider.Verifier(oidcConfig)

			authn, err = oidc.New(db, logger, oidcProvider, verifier, func(idToken *oidc_server.IDToken) (*oidc.IDTokenClaims, error) {
				claims := &oidc.IDTokenClaims{}
				err = idToken.Claims(claims)
				if err != nil {
					return nil, err
				}

				return claims, nil
			}, (*oidc.Options)(&serveOpts.oidcOptions))
			if err != nil {
				logger.Fatal("failed to create oidc authenticator", err)
			}
		}

		uiServer, err := uiport.New(ctx, &uiport.NewServerArgs{
			Locale:        "nl",
			StaticPath:    staticFilesPath,
			GroupID:       serveOpts.GroupID,
			Logger:        logger,
			APIApp:        app,
			UIApp:         uiApp,
			Clock:         clock.New(),
			Authenticator: authn,
		})

		go func() {
			err = uiServer.ListenAndServe(serveOpts.ListenAddressUI)
			if err != nil {
				logger.Fatal("could not listen and serve", err)
			}
		}()

		internalApp, err := internalapp.NewApplication(&internalapp.NewApplicationArgs{
			Context: ctx,
			GroupID: serveOpts.GroupID,
			Storage: storage,
		})
		if err != nil {
			logger.Fatal("could not create application", err)
		}

		grpcServerInternal, err := internalgrpc.New(logger.Logger, internalApp, cert)
		if err != nil {
			logger.Fatal("could not create internal grpc server", err)
		}

		go func() {
			err = grpcServerInternal.ListenAndServe(serveOpts.ListenAddressInternal)
			if err != nil {
				logger.Fatal("could not listen and serve internal grpc server", err)
			}
		}()

		restAPI, err := rest.New(&rest.NewArgs{
			Logger:        logger,
			App:           app,
			Cert:          cert,
			ListenAddress: serveOpts.ListenAddressAPI,
		})

		if err != nil {
			logger.Fatal("could not create rest api", err)
		}

		go func() {
			err = restAPI.ListenAndServeTLS(serveOpts.TLSOptions.CertFile, serveOpts.TLSOptions.KeyFile)
			if err != nil {
				logger.Fatal("could not listen and serve API server", err)
			}
		}()

		p.Wait()

		logger.Info("starting graceful shutdown")

		gracefulCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
		defer cancel()

		err = uiServer.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown server", err)
		}
	},
}
