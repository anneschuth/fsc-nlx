// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package manager

import (
	"context"
	"errors"
	"time"

	"go.nlx.io/nlx/txlog-api/domain/record"
)

type Manager interface {
	// Contracts
	ListContracts(ctx context.Context, contentHashes []string, grantTypes []GrantType) (Contracts, error)
	CreateContract(ctx context.Context, args *CreateContractArgs) error
	AcceptContract(ctx context.Context, contentHash string) error
	RevokeContract(ctx context.Context, contentHash string) error
	RejectContract(ctx context.Context, contentHash string) error

	// Other
	ListServices(ctx context.Context) (Services, error)
	ListTXLogRecords(ctx context.Context, args *ListTXLogRecordsRequest) ([]*TXLogRecord, error)
	GetPeerInfo(ctx context.Context) (*Peer, error)
	ListPeers(ctx context.Context) (Peers, error)
}

var (
	ErrInternalError = errors.New("internal error")
)

type ValidationError struct {
	message string
}

func NewValidationError(message string) *ValidationError {
	return &ValidationError{
		message: message,
	}
}

func (v *ValidationError) Error() string {
	return v.message
}

type Contracts []*Contract

type Contract struct {
	ID                                []byte
	Hash                              string
	HashAlgorithm                     HashAlg
	GroupID                           string
	CreatedAt                         time.Time
	Peers                             map[string]*Peer
	AcceptSignatures                  map[string]Signature
	RejectSignatures                  map[string]Signature
	RevokeSignatures                  map[string]Signature
	ValidFrom                         time.Time
	ValidUntil                        time.Time
	HasRejected                       bool
	HasAccepted                       bool
	HasRevoked                        bool
	PeerRegistrationGrant             *PeerRegistrationGrant
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
}

type Peers []*Peer

type Peer struct {
	ID   string
	Name string
}

type Signature struct {
	SignedAt time.Time
}

type Services []interface{}

type Service struct {
	Name     string
	PeerID   string
	PeerName string
}

type DelegatedService struct {
	Name             string
	ProviderPeerID   string
	ProviderPeerName string
	DelegateeID      string
	DelegateeName    string
}

type HashAlg int32

const (
	HashAlgUnspecified HashAlg = iota
	HashAlgSHA3_512
)

type CreateContractArgs struct {
	ID                                []byte
	HashAlgorithm                     HashAlg
	GroupID                           string
	ContractNotBefore                 time.Time
	ContractNotAfter                  time.Time
	CreatedAt                         time.Time
	PeerRegistrationGrants            []*PeerRegistrationGrant
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
}

type PeerRegistrationGrant struct {
	Hash            string
	DirectoryPeerID string
	PeerID          string
	PeerName        string
}

type ServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

type ServiceConnectionGrant struct {
	Hash                        string
	ServicePeerID               string
	ServiceName                 string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
}

type DelegatedServiceConnectionGrant struct {
	Hash                        string
	DelegatorPeerID             string
	ServicePeerID               string
	ServiceName                 string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
}

type DelegatedServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	DelegatorPeerID string
	ServicePeerID   string
	ServiceName     string
}

type SortOrder uint32

const (
	SortOrderUnspecified SortOrder = iota
	SortOrderAscending
	SortOrderDescending
)

type Pagination struct {
	StartID   string
	Limit     uint32
	SortOrder SortOrder
}

type Period struct {
	Start time.Time
	End   time.Time
}

type TXLogFilter struct {
	Period        *Period
	TransactionID *record.TransactionID
	ServiceName   string
	GrantHash     string
	PeerID        string
}

type ListTXLogRecordsRequest struct {
	Pagination       *Pagination
	Filters          []*TXLogFilter
	DataSourcePeerID string
}

type TXLogRecord struct {
	TransactionID string
	GrantHash     string
	ServiceName   string
	Direction     record.Direction
	Source        interface{}
	Destination   interface{}
	CreatedAt     time.Time
}

type TXLogRecordSource struct {
	OutwayPeerID string
}

type TXLogRecordDelegatedSource struct {
	OutwayPeerID    string
	DelegatorPeerID string
}

type TXLogRecordDestination struct {
	ServicePeerID string
}

type TXLogRecordDelegatedDestination struct {
	ServicePeerID   string
	DelegatorPeerID string
}

type GrantType string

const (
	GrantTypePeerUnspecified             GrantType = ""
	GrantTypePeerRegistration            GrantType = "peer_registration"
	GrantTypeServicePublication          GrantType = "service_publication"
	GrantTypeServiceConnection           GrantType = "service_connection"
	GrantTypeDelegatedServicePublication GrantType = "delegated_service_publication"
	GrantTypeDelegatedServiceConnection  GrantType = "delegated_service_connection"
)
