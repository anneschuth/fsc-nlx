/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package grpcmanager

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (m *grpcManager) ListPeers(ctx context.Context) (manager.Peers, error) {
	res, err := m.client.ListPeers(ctx, &api.ListPeersRequest{})
	if err != nil {
		return nil, errors.Wrap(err, "could not retrieve peers from grpc manager")
	}

	peers := make(manager.Peers, 0, len(res.Peers))

	for _, peer := range res.GetPeers() {
		peers = append(peers, &manager.Peer{
			ID:   peer.Id,
			Name: peer.Name,
		})
	}

	return peers, nil
}
