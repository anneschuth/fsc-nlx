/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package grpcmanager_test

import (
	"testing"

	"go.nlx.io/nlx/common/logger"
	discardlogger "go.nlx.io/nlx/common/logger/discard"
	mock_client "go.nlx.io/nlx/controller/pkg/manager/mock"
)

type mocks struct {
	client *mock_client.MockClient
	lgr    *logger.Logger
}

func newMocks(t *testing.T) *mocks {
	return &mocks{
		client: mock_client.NewMockClient(t),
		lgr:    discardlogger.New(),
	}
}
