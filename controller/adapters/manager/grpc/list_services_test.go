// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpcmanager_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/controller/adapters/manager"
	grpcmanager "go.nlx.io/nlx/controller/adapters/manager/grpc"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

// nolint:funlen // tests exceed 100 lines
func TestListServices(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		setup   func(context.Context, *mocks)
		want    manager.Services
		wantErr error
	}{
		"when_manager_errors": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().
					ListServices(ctx, &api.ListServicesRequest{}).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: errors.New("could not list services from grpc manager: unexpected error"),
		},
		"happy_flow": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().
					ListServices(ctx, &api.ListServicesRequest{}).
					Return(&api.ListServicesResponse{
						Services: []*api.ListServicesResponse_Service{
							{
								Data: &api.ListServicesResponse_Service_Service_{
									Service: &api.ListServicesResponse_Service_Service{
										Name: "basisregister-fictieve-personen",
										Provider: &api.ListServicesResponse_Peer{
											Id:   "12345678901234567890",
											Name: "Gemeente Stijns",
										},
									},
								},
							},
							{
								Data: &api.ListServicesResponse_Service_DelegatedService_{
									DelegatedService: &api.ListServicesResponse_Service_DelegatedService{
										Name: "basisregister-fictieve-kentekens",
										Provider: &api.ListServicesResponse_Peer{
											Id:   "12345678901234567891",
											Name: "RvRD",
										},
										Delegator: &api.ListServicesResponse_Peer{
											Id:   "12345678901234567890",
											Name: "Gemeente Stijns",
										},
									},
								},
							},
						},
					}, nil)
			},
			want: manager.Services{
				&manager.Service{
					Name:     "basisregister-fictieve-personen",
					PeerID:   "12345678901234567890",
					PeerName: "Gemeente Stijns",
				},
				&manager.DelegatedService{
					Name:             "basisregister-fictieve-kentekens",
					ProviderPeerID:   "12345678901234567891",
					ProviderPeerName: "RvRD",
					DelegateeID:      "12345678901234567890",
					DelegateeName:    "Gemeente Stijns",
				},
			},
			wantErr: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			ctx := context.Background()

			tt.setup(ctx, mocks)

			grpcManager, err := grpcmanager.New(mocks.client, mocks.lgr)
			assert.NoError(t, err)

			actual, err := grpcManager.ListServices(ctx)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, actual)
			} else {
				assert.EqualError(t, err, tt.wantErr.Error())
			}
		})
	}
}
