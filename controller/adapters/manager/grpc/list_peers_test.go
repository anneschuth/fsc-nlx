/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package grpcmanager_test

import (
	"context"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/controller/adapters/manager"
	grpcmanager "go.nlx.io/nlx/controller/adapters/manager/grpc"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

// nolint:dupl // these tests should not fit 100 lines
func TestListPeers(t *testing.T) {
	t.Parallel()

	testcases := map[string]struct {
		setup   func(context.Context, *mocks)
		want    manager.Peers
		wantErr error
	}{
		"when_manager_errors": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().
					ListPeers(ctx, &api.ListPeersRequest{}).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: errors.Wrap(errors.New("unexpected error"), "could not retrieve peers from grpc manager"),
		},
		"happy_flow_two_results": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().ListPeers(ctx, &api.ListPeersRequest{}).Return(&api.ListPeersResponse{
					Peers: []*api.ListPeersResponse_Peer{
						{
							Id:   "1",
							Name: "PeerOne",
						},
						{
							Id:   "2",
							Name: "Peer2",
						},
					},
				}, nil)
			},
			want: manager.Peers{
				{
					ID:   "1",
					Name: "PeerOne",
				},
				{
					ID:   "2",
					Name: "Peer2",
				},
			},
		},
	}

	for name, testcase := range testcases {
		testcase := testcase

		t.Run(name, func(t *testing.T) {
			t.Parallel()
			mocks := newMocks(t)

			ctx := context.Background()
			testcase.setup(ctx, mocks)

			grpcManager, err := grpcmanager.New(mocks.client, mocks.lgr)
			assert.NoError(t, err)

			actual, err := grpcManager.ListPeers(ctx)

			if testcase.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, testcase.want, actual)
			} else {
				assert.EqualError(t, err, testcase.wantErr.Error())
			}
		})
	}
}
