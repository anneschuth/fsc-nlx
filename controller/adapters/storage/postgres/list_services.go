// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"fmt"
	"net/url"

	"go.nlx.io/nlx/controller/adapters/storage"
)

func (p *PostgresStorage) ListServices(ctx context.Context, groupID string) (storage.Services, error) {
	rows, err := p.queries.ListServices(ctx, groupID)
	if err != nil {
		return nil, fmt.Errorf("could not list services from database: %w", err)
	}

	services := make(storage.Services, len(rows))

	for i, s := range rows {
		u, err := url.Parse(s.EndpointUrl)
		if err != nil {
			return nil, fmt.Errorf("invalid endpoint URL in database: %w", err)
		}

		services[i] = &storage.Service{
			Name:         s.Name,
			EndpointURL:  u,
			InwayAddress: s.InwayAddress,
		}
	}

	return services, nil
}
