// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"fmt"
	"net/url"

	"go.nlx.io/nlx/controller/adapters/storage/postgres/queries"
)

func (p *PostgresStorage) GetInwayAddressForService(ctx context.Context, groupID, serviceName string) (*url.URL, error) {
	address, err := p.queries.GetInwayAddressForService(ctx, &queries.GetInwayAddressForServiceParams{
		GroupID: groupID,
		Name:    serviceName,
	})
	if err != nil {
		return nil, fmt.Errorf("could not get inway address for service from database: %w", err)
	}

	u, err := url.Parse(address)
	if err != nil {
		return nil, fmt.Errorf("invalid inway address in database: %w", err)
	}

	return u, nil
}
