// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package outway

import (
	"context"
	"crypto/tls"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"time"

	"github.com/pkg/errors"
	"golang.org/x/net/http2"

	"go.nlx.io/nlx/common/httperrors"
	common_tls "go.nlx.io/nlx/common/tls"
	outway_http "go.nlx.io/nlx/outway/http"
)

type HTTPService struct {
	proxy *httputil.ReverseProxy
}

func NewHTTPService(cert *common_tls.CertificateBundle, inwayAddress string) (*HTTPService, error) {
	tlsConfig := cert.TLSConfig()

	roundTripTransport, err := newRoundTripHTTPTransport(tlsConfig)
	if err != nil {
		return nil, errors.Wrap(err, "failed to setup transport")
	}

	endpointURL, err := url.Parse(inwayAddress)
	if err != nil {
		return nil, errors.Wrap(err, "inway address:"+inwayAddress+" is not a valid url")
	}

	proxy := httputil.NewSingleHostReverseProxy(endpointURL)
	proxy.Transport = roundTripTransport
	proxy.ErrorHandler = proxyErrorHandler

	return &HTTPService{
		proxy: proxy,
	}, nil
}

func (s *HTTPService) HandleRequest(w http.ResponseWriter, r *http.Request) {
	s.proxy.ServeHTTP(w, r)
}

func newRoundTripHTTPTransport(tlsConfig *tls.Config) (*http.Transport, error) {
	const (
		timeOut               = 30 * time.Second
		keepAlive             = 30 * time.Second
		maxIdleConns          = 100
		idleConnTimeout       = 10 * time.Second
		tlsHandshakeTimeout   = 10 * time.Second
		expectContinueTimeout = 1 * time.Second
	)

	transport := &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
			a := &net.Dialer{
				Timeout:   timeOut,
				KeepAlive: keepAlive,
			}

			return a.DialContext(ctx, network, addr)
		},
		MaxIdleConns:          maxIdleConns,
		IdleConnTimeout:       idleConnTimeout,
		TLSHandshakeTimeout:   tlsHandshakeTimeout,
		ExpectContinueTimeout: expectContinueTimeout,
		TLSClientConfig:       tlsConfig,
	}
	if err := http2.ConfigureTransport(transport); err != nil {
		return nil, errors.Wrap(err, "failed to add http2 to transport")
	}

	return transport, nil
}

func proxyErrorHandler(w http.ResponseWriter, r *http.Request, _ error) {
	outway_http.WriteError(w, httperrors.O1, httperrors.ServiceUnreachable(r.URL.String()))
}
