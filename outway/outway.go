// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package outway

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/common/monitoring"
	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/common/transactionlog"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/outway/adapters/controller"
	"go.nlx.io/nlx/outway/domain/config"
	"go.nlx.io/nlx/outway/plugins"
)

type loggerHTTPHandler func(logger *zap.Logger, w http.ResponseWriter, r *http.Request)

type Organization struct {
	peerID string
	name   string
}

type Outway struct {
	name               string
	groupID            contract.GroupID
	ctx                context.Context
	wg                 *sync.WaitGroup
	organization       *Organization // the organization running this outway
	externalCert       *common_tls.CertificateBundle
	logger             *zap.Logger
	txlogger           transactionlog.TransactionLogger
	cfg                *config.Config
	controller         controller.Controller
	httpServer         *http.Server
	monitorService     *monitoring.Service
	requestHTTPHandler loggerHTTPHandler
	plugins            []plugins.Plugin
	proxies            *HTTPProxies
	clk                clock.Clock
}

type NewOutwayArgs struct {
	Clock             clock.Clock
	Name              string
	GroupID           string
	Ctx               context.Context
	Logger            *zap.Logger
	Txlogger          transactionlog.TransactionLogger
	Controller        controller.Controller
	ConfigRepository  config.Repository
	MonitoringAddress string
	ExternalCert      *common_tls.CertificateBundle
	InternalCert      *common_tls.CertificateBundle
	AuthServiceURL    string
	AuthCAPath        string
}

func (o *Outway) configureAuthorizationPlugin(authCAPath, authServiceURL string) (*plugins.AuthorizationPlugin, error) {
	if authServiceURL == "" {
		return nil, nil
	}

	if authCAPath == "" {
		return nil, fmt.Errorf("authorization service URL set but no CA for authorization provided")
	}

	authURL, err := url.Parse(authServiceURL)
	if err != nil {
		return nil, err
	}

	if authURL.Scheme != "https" {
		return nil, errors.New("scheme of authorization service URL is not 'https'")
	}

	ca, err := common_tls.NewCertPoolFromFile(authCAPath)
	if err != nil {
		return nil, err
	}

	tlsConfig := common_tls.NewConfig(common_tls.WithTLS12())
	tlsConfig.RootCAs = ca

	return plugins.NewAuthorizationPlugin(&plugins.NewAuthorizationPluginArgs{
		CA:         ca,
		ServiceURL: authURL.String(),
		AuthorizationClient: http.Client{
			Transport: createHTTPTransport(tlsConfig),
		},
		ExternalCertificate: o.externalCert,
	})
}

func New(args *NewOutwayArgs) (*Outway, error) {
	if args.Name == "" {
		return nil, fmt.Errorf("name cannot be empty")
	}

	groupID, err := contract.NewGroupID(args.GroupID)
	if err != nil {
		return nil, fmt.Errorf("invalid groupID in params: %w", err)
	}

	if args.ExternalCert == nil {
		return nil, fmt.Errorf("externalCert is required")
	}

	cert := args.ExternalCert.Certificate()

	if len(cert.Subject.Organization) != 1 {
		return nil, errors.New("cannot obtain organization name from self cert")
	}

	err = common_tls.ValidatePeerID(cert.Subject.SerialNumber)
	if err != nil {
		return nil, fmt.Errorf("validation error for subject serial number from cert: %s", err)
	}

	organizationName := cert.Subject.Organization[0]
	organizationPeerID := cert.Subject.SerialNumber

	if args.ConfigRepository == nil {
		return nil, errors.New("missing config repository")
	}

	proxies, err := NewHTTPProxies(args.ExternalCert)
	if err != nil {
		return nil, errors.Wrap(err, "could not create http proxies struct")
	}

	cfg, err := config.New(args.Clock, args.ConfigRepository)
	if err != nil {
		return nil, errors.Wrap(err, "could not create config")
	}

	o := &Outway{
		ctx:        args.Ctx,
		name:       args.Name,
		groupID:    groupID,
		wg:         &sync.WaitGroup{},
		controller: args.Controller,
		cfg:        cfg,
		logger: args.Logger.With(
			zap.String("outway-organization-name", organizationName),
			zap.String("outway-organization-peer-id", organizationPeerID)),
		txlogger: args.Txlogger,
		organization: &Organization{
			peerID: cert.Subject.SerialNumber,
			name:   organizationName,
		},
		externalCert: args.ExternalCert,
		proxies:      proxies,
		clk:          args.Clock,
	}

	authorizationPlugin, err := o.configureAuthorizationPlugin(args.AuthCAPath, args.AuthServiceURL)
	if err != nil {
		return nil, err
	}

	o.monitorService, err = monitoring.NewMonitoringService(args.MonitoringAddress, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "unable to create monitoring service")
	}

	o.plugins = []plugins.Plugin{
		plugins.NewLogRecordPlugin(o.organization.peerID, o.txlogger),
	}

	if authorizationPlugin != nil {
		o.plugins = append(o.plugins, authorizationPlugin)
	}

	// Strip headers as last step in plugin chain so intermediate steps can still access all headers
	o.plugins = append(o.plugins, plugins.NewStripHeadersPlugin(o.organization.peerID))

	return o, nil
}

func (o *Outway) Run(ctx context.Context) error {
	go o.announceToControllerAPI(ctx)

	return nil
}

func createHTTPTransport(tlsConfig *tls.Config) *http.Transport {
	const (
		timeOut               = 30 * time.Second
		keepAlive             = 30 * time.Second
		maxIdleCons           = 100
		idleConnTimeout       = 20 * time.Second
		tlsHandshakeTimeout   = 10 * time.Second
		expectContinueTimeout = 1 * time.Second
	)

	return &http.Transport{
		DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
			a := &net.Dialer{
				Timeout:   timeOut,
				KeepAlive: keepAlive,
			}

			return a.DialContext(ctx, network, addr)
		},
		MaxIdleConns:          maxIdleCons,
		IdleConnTimeout:       idleConnTimeout,
		TLSHandshakeTimeout:   tlsHandshakeTimeout,
		ExpectContinueTimeout: expectContinueTimeout,
		TLSClientConfig:       tlsConfig,
	}
}
