# Outway

The NLX _Outway_ proxies outgoing requests to an _Inway_.
The _Outway_ process is operated by each organization that is making requests via NLX.
