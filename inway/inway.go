// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package inway

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"regexp"
	"sync"
	"time"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/common/monitoring"
	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/common/transactionlog"
	"go.nlx.io/nlx/inway/adapters/controller"
	"go.nlx.io/nlx/inway/domain/config"
	"go.nlx.io/nlx/inway/plugins"
	"go.nlx.io/nlx/manager/domain/contract"
)

var nameRegex = regexp.MustCompile(`^[a-zA-Z0-9-]{1,100}$`)

const (
	timeOut               = 30 * time.Second
	keepAlive             = 30 * time.Second
	maxIdleCons           = 100
	IdleConnTimeout       = 20 * time.Second
	TLSHandshakeTimeout   = 10 * time.Second
	ExpectContinueTimeout = 1 * time.Second
)

type Peer struct {
	ID   string
	Name string
}

type Inway struct {
	clock             clock.Clock
	name              string
	groupID           contract.GroupID
	peer              Peer
	address           string
	orgCertBundle     *common_tls.CertificateBundle
	logger            *zap.Logger
	serverTLS         *http.Server
	monitoringService *monitoring.Service
	plugins           []plugins.Plugin
	controller        controller.Controller
	servicesLock      sync.RWMutex
	config            *config.Config
}

type Params struct {
	Context                      context.Context
	Logger                       *zap.Logger
	Txlogger                     transactionlog.TransactionLogger
	Name                         string
	GroupID                      string
	Address                      *url.URL
	MonitoringAddress            string
	OrgCertBundle                *common_tls.CertificateBundle
	Controller                   controller.Controller
	AuthServiceURL               string
	AuthCAPath                   string
	ConfigRepository             config.Repository
	ServiceEndpointCacheDuration time.Duration
	Clock                        clock.Clock
}

func NewInway(params *Params) (*Inway, error) {
	logger := params.Logger

	if logger == nil {
		logger = zap.NewNop()
	}

	groupID, err := contract.NewGroupID(params.GroupID)
	if err != nil {
		return nil, fmt.Errorf("invalid groupID in params: %w", err)
	}

	if !nameRegex.MatchString(params.Name) {
		return nil, errors.New("a valid name is required (alphanumeric & dashes, max. 100 characters)")
	}

	orgCert := params.OrgCertBundle.Certificate()

	if len(orgCert.Subject.Organization) != 1 {
		return nil, errors.New("cannot obtain organization name from self cert")
	}

	err = addressIsInOrgCert(params.Address, orgCert)
	if err != nil {
		return nil, err
	}

	if params.Context == nil {
		return nil, errors.New("context is nil. needed to close gracefully")
	}

	cfg, err := config.New(&config.NewConfigArgs{
		Repo:          params.ConfigRepository,
		CacheDuration: time.Minute,
		Clock:         clock.New(),
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create config")
	}

	peerName := orgCert.Subject.Organization[0]
	peerID := orgCert.Subject.SerialNumber

	logger.Info("loaded certificates for inway", zap.String("inway-peer-id", peerID), zap.String("inway-peer-name", peerName))

	i := &Inway{
		logger:  logger.With(zap.String("inway-peer-id", peerName)),
		groupID: groupID,
		peer: Peer{
			ID:   peerID,
			Name: peerName,
		},
		address:       params.Address.String(),
		orgCertBundle: params.OrgCertBundle,
		controller:    params.Controller,
		servicesLock:  sync.RWMutex{},
		plugins: []plugins.Plugin{
			plugins.NewAuthenticationPlugin(),
		},
		config: cfg,
		clock:  params.Clock,
	}

	authorizationPlugin, err := configureAuthorizationPlugin(params.OrgCertBundle, params.AuthCAPath, params.AuthServiceURL)
	if err != nil {
		return nil, err
	}

	i.plugins = append(i.plugins,
		plugins.NewLogRecordPlugin(peerID, params.Txlogger),
	)

	if authorizationPlugin != nil {
		i.plugins = append(i.plugins, authorizationPlugin)
	}

	i.monitoringService, err = monitoring.NewMonitoringService(params.MonitoringAddress, logger)
	if err != nil {
		return nil, errors.Wrap(err, "unable to create monitoring service")
	}

	if params.Name != "" {
		i.name = params.Name
	} else {
		i.name = common_tls.X509CertificateThumbprint(orgCert)
	}

	return i, nil
}

func addressIsInOrgCert(address *url.URL, orgCert *x509.Certificate) error {
	hostname := address.Hostname()

	if hostname == orgCert.Subject.CommonName {
		return nil
	}

	for _, dnsName := range orgCert.DNSNames {
		if hostname == dnsName {
			return nil
		}
	}

	return errors.Errorf("'%s' is not in the list of DNS names of the certificate, %v", hostname, orgCert.DNSNames)
}

func configureAuthorizationPlugin(externalCert *common_tls.CertificateBundle, authCAPath, authServiceURL string) (*plugins.AuthorizationPlugin, error) {
	if authServiceURL == "" {
		return nil, nil
	}

	if authCAPath == "" {
		return nil, fmt.Errorf("authorization service URL set but no CA for authorization provided")
	}

	authURL, err := url.Parse(authServiceURL)
	if err != nil {
		return nil, err
	}

	if authURL.Scheme != "https" {
		return nil, errors.New("scheme of authorization service URL is not 'https'")
	}

	ca, err := common_tls.NewCertPoolFromFile(authCAPath)
	if err != nil {
		return nil, err
	}

	tlsConfig := common_tls.NewConfig(common_tls.WithTLS12())
	tlsConfig.RootCAs = ca

	return plugins.NewAuthorizationPlugin(&plugins.NewAuthorizationPluginArgs{
		CA:         ca,
		ServiceURL: authURL.String(),
		AuthorizationClient: &http.Client{
			Transport: createHTTPTransport(tlsConfig),
		},
		AuthServerEnabled: true,
		ExternalCert:      externalCert,
	})
}

func createHTTPTransport(tlsConfig *tls.Config) *http.Transport {
	return &http.Transport{
		DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
			a := &net.Dialer{
				Timeout:   timeOut,
				KeepAlive: keepAlive,
			}

			return a.DialContext(ctx, network, addr)
		},
		MaxIdleConns:          maxIdleCons,
		IdleConnTimeout:       IdleConnTimeout,
		TLSHandshakeTimeout:   TLSHandshakeTimeout,
		ExpectContinueTimeout: ExpectContinueTimeout,
		TLSClientConfig:       tlsConfig,
	}
}
