// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package inway

import (
	"context"
	"net"
	"net/http"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"go.nlx.io/nlx/inway/adapters/controller"
)

const readHeaderTimeout = time.Second * 60

const timeout = 30 * time.Second
const maxIdleConns = 100
const idleConnTimeout = 90 * time.Second
const tlsHandshakeTimeout = 10 * time.Second

func newRoundTripHTTPTransport() *http.Transport {
	return &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   timeout,
			KeepAlive: timeout,
		}).DialContext,
		MaxIdleConns:          maxIdleConns,
		MaxIdleConnsPerHost:   maxIdleConns,
		IdleConnTimeout:       idleConnTimeout,
		TLSHandshakeTimeout:   tlsHandshakeTimeout,
		ExpectContinueTimeout: 1 * time.Second,
	}
}

func (i *Inway) Run(ctx context.Context, address string) error {
	// TODO: do we still want this?
	err := i.registerToController(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to register to controller API")
	}

	serveMux := http.NewServeMux()
	serveMux.Handle("/.nlx/", http.NotFoundHandler())
	serveMux.HandleFunc("/", i.handleProxyRequest)

	config := i.orgCertBundle.TLSConfig(i.orgCertBundle.WithTLSClientAuth())

	i.serverTLS = &http.Server{
		Addr:              address,
		Handler:           serveMux,
		TLSConfig:         config,
		ReadHeaderTimeout: readHeaderTimeout,
	}

	errorChannel := make(chan error)

	go func() {
		if err := i.serverTLS.ListenAndServeTLS("", ""); err != nil {
			if err != http.ErrServerClosed {
				errorChannel <- errors.Wrap(err, "error listening on TLS server")
			}
		}
	}()

	go func() {
		if err := i.monitoringService.Start(); err != nil {
			if err != http.ErrServerClosed {
				errorChannel <- errors.Wrap(err, "error listening on monitoring service")
			}
		}
	}()

	i.monitoringService.SetReady()

	return <-errorChannel
}

func (i *Inway) Shutdown(ctx context.Context) error {
	i.monitoringService.SetNotReady()

	err := i.serverTLS.Shutdown(ctx)
	if err != http.ErrServerClosed {
		return err
	}

	if err := i.monitoringService.Stop(); err != nil {
		return err
	}

	return nil
}

func (i *Inway) registerToController(ctx context.Context) error {
	register := func() error {
		err := i.controller.RegisterInway(ctx, &controller.RegisterInwayArgs{
			Name:    i.name,
			Address: i.address,
		})
		if err != nil {
			i.logger.Error("failed to register to controller api", zap.Error(err))

			return err
		}

		i.logger.Info("controller api registration successful")

		return nil
	}

	err := backoff.Retry(register, backoff.WithContext(backoff.NewExponentialBackOff(), ctx))
	if err != nil {
		i.logger.Error("permanently failed to register to controller api", zap.Error(err))

		return err
	}

	return nil
}
