// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpcconfig

import (
	"crypto/x509"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/inway/domain/config"
	"go.nlx.io/nlx/inway/pkg/controller"
	"go.nlx.io/nlx/inway/pkg/manager"
)

type Repository struct {
	managerClient    manager.Client
	controllerClient controller.Client
	selfAddress      string
	trustedRootCerts *x509.CertPool
}

type NewGRPCConfigArgs struct {
	SelfAddress      string
	ManagerClient    manager.Client
	ControllerClient controller.Client
	TrustedRootCert  *x509.CertPool
}

func New(args *NewGRPCConfigArgs) (config.Repository, error) {
	if args.ManagerClient == nil {
		return nil, errors.New("manager client is required")
	}

	if args.ControllerClient == nil {
		return nil, errors.New("controller client is required")
	}

	if args.TrustedRootCert == nil {
		return nil, errors.New("trusted root certificates are required")
	}

	if args.SelfAddress == "" {
		return nil, errors.New("self address is required")
	}

	return &Repository{
		selfAddress:      args.SelfAddress,
		managerClient:    args.ManagerClient,
		controllerClient: args.ControllerClient,
		trustedRootCerts: args.TrustedRootCert,
	}, nil
}

func (r *Repository) Close() error {
	if err := r.controllerClient.Close(); err != nil {
		return err
	}

	return r.managerClient.Close()
}
