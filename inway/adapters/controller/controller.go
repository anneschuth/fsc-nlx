// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package controller

import "context"

type Controller interface {
	RegisterInway(ctx context.Context, args *RegisterInwayArgs) error
}

type RegisterInwayArgs struct {
	Name    string
	Address string
}
