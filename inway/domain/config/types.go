// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package config

import (
	"net/url"
	"sync"
	"time"

	"golang.org/x/sync/singleflight"

	"go.nlx.io/nlx/manager/domain/contract"
)

type service struct {
	expiresAt   time.Time
	endpointURL *url.URL
}

type services struct {
	services       map[string]*service
	mut            *sync.RWMutex
	getEndpointURL *singleflight.Group
	cacheDuration  time.Duration // Duration to remember endpoint url
}

type certificates struct {
	certificates   map[contract.CertificateThumbprint]*contract.PeerCertificate
	mut            *sync.RWMutex
	getCertificate *singleflight.Group
}
