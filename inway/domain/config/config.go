// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package config

import (
	"fmt"
	"sync"
	"time"

	"golang.org/x/sync/singleflight"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/manager/domain/contract"
)

type Config struct {
	clock        clock.Clock
	repo         Repository
	services     *services
	certificates *certificates
}

type NewConfigArgs struct {
	Repo          Repository
	CacheDuration time.Duration
	Clock         clock.Clock
}

func New(args *NewConfigArgs) (*Config, error) {
	if args.Repo == nil {
		return nil, fmt.Errorf("repo is required")
	}

	if args.Clock == nil {
		return nil, fmt.Errorf("clock is required")
	}

	return &Config{
		clock: args.Clock,
		repo:  args.Repo,
		services: &services{
			services:       make(map[string]*service),
			mut:            &sync.RWMutex{},
			getEndpointURL: &singleflight.Group{},
			cacheDuration:  args.CacheDuration,
		},
		certificates: &certificates{
			certificates:   make(map[contract.CertificateThumbprint]*contract.PeerCertificate),
			mut:            &sync.RWMutex{},
			getCertificate: &singleflight.Group{},
		},
	}, nil
}
