// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package plugins

import (
	ctx_context "context"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"go.nlx.io/nlx/common/httperrors"
	"go.nlx.io/nlx/common/transactionlog"
	inway_http "go.nlx.io/nlx/inway/http"
	"go.nlx.io/nlx/txlog-api/domain/record"
)

type LogRecordPlugin struct {
	selfPeerID string
	txLogger   transactionlog.TransactionLogger
}

func NewLogRecordPlugin(selfPeerID string, txLogger transactionlog.TransactionLogger) *LogRecordPlugin {
	return &LogRecordPlugin{
		selfPeerID: selfPeerID,
		txLogger:   txLogger,
	}
}

func (plugin *LogRecordPlugin) Serve(next ServeFunc) ServeFunc {
	return func(context *Context) error {
		unparsedID := context.Request.Header.Get("Fsc-Transaction-Id")
		if unparsedID == "" {
			context.Logger.Warn("Received request with missing transaction id", zap.String("connecting_peer_id", context.ConnectionInfo.PeerID))

			inway_http.WriteError(context.Response, httperrors.O1, httperrors.MissingLogRecordID())

			return nil
		}

		id, err := transactionlog.NewTransactionIDFromString(unparsedID)
		if err != nil {
			context.Logger.Warn("Received request with invalid transaction id", zap.String("connecting_peer_id", context.ConnectionInfo.PeerID))

			inway_http.WriteError(context.Response, httperrors.O1, httperrors.MissingLogRecordID())

			return nil
		}

		err = plugin.createLogRecord(context, id)
		if err != nil {
			context.Logger.Error("failed to store transactionlog record", zap.Error(err))

			inway_http.WriteError(context.Response, httperrors.O1, httperrors.ServerError(nil))

			return nil
		}

		return next(context)
	}
}

func (plugin *LogRecordPlugin) createLogRecord(context *Context, id *transactionlog.TransactionID) error {
	var source interface{}
	if context.Token.OutwayDelegatorPeerID == "" {
		source = &transactionlog.RecordSource{
			OutwayPeerID: context.Token.OutwayPeerID,
		}
	} else {
		source = &transactionlog.RecordDelegatedSource{
			OutwayPeerID:    context.Token.OutwayPeerID,
			DelegatorPeerID: context.Token.OutwayDelegatorPeerID,
		}
	}

	var destination interface{}
	if context.Token.ServiceDelegatorPeerID == "" {
		destination = &transactionlog.RecordDestination{
			ServicePeerID: context.Token.ServicePeerID,
		}
	} else {
		destination = &transactionlog.RecordDelegatedDestination{
			ServicePeerID:   context.Token.ServicePeerID,
			DelegatorPeerID: context.Token.ServiceDelegatorPeerID,
		}
	}

	rec := &transactionlog.Record{
		TransactionID: id,
		GroupID:       context.Token.GroupID,
		GrantHash:     context.Token.GrantHash,
		Direction:     record.DirectionIn,
		ServiceName:   context.Token.ServiceName,
		Source:        source,
		Destination:   destination,
		Data: map[string]interface{}{
			"request-path": context.Request.URL.Path,
		},
		CreatedAt: context.RequestReceivedAt,
	}

	if err := plugin.txLogger.AddRecords(ctx_context.TODO(), []*transactionlog.Record{rec}); err != nil {
		return errors.Wrap(err, "unable to add records to txlog")
	}

	return nil
}
