// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package plugins

import (
	"net/http"
	"net/url"
	"time"

	"go.uber.org/zap"

	"go.nlx.io/nlx/common/accesstoken"
)

type Context struct {
	Logger            *zap.Logger
	Response          http.ResponseWriter
	Request           *http.Request
	ConnectionInfo    *ConnectionInfo
	Token             *accesstoken.DecodedToken
	EndpointURL       *url.URL
	RequestReceivedAt time.Time
}

type ConnectionInfo struct {
	PeerID                string
	CertificateThumbprint string
	RawDERCertificates    [][]byte
}

type ServeFunc func(context *Context) error

type Plugin interface {
	Serve(next ServeFunc) ServeFunc
}

func BuildChain(serve ServeFunc, pluginList ...Plugin) ServeFunc {
	if len(pluginList) == 0 {
		return serve
	}

	return pluginList[0].Serve(BuildChain(serve, pluginList[1:]...))
}
