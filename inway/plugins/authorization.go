// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package plugins

import (
	"bytes"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"go.uber.org/zap"

	"go.nlx.io/nlx/common/httperrors"
	common_tls "go.nlx.io/nlx/common/tls"
	inway_http "go.nlx.io/nlx/inway/http"
	"go.nlx.io/nlx/manager/domain/contract"
)

type AuthRequest struct {
	Input *AuthRequestInput `json:"input"`
}

type AuthRequestInput struct {
	Headers         http.Header `json:"headers"`
	Path            string      `json:"path"`
	InwayCertChain  []string    `json:"inway_certificate_chain"`
	OutwayCertChain []string    `json:"outway_certificate_chain"`
}

type authResponse struct {
	Allowed bool               `json:"allowed"`
	Status  authResponseStatus `json:"status"`
}

type authResponseStatus struct {
	Reason string `json:"reason"`
}

type AuthorizationPlugin struct {
	authServerEnabled   bool
	ca                  *x509.CertPool
	serviceURL          string
	authorizationClient *http.Client
	inwayCertChain      []string
}

type NewAuthorizationPluginArgs struct {
	CA                  *x509.CertPool
	AuthorizationClient *http.Client
	ServiceURL          string
	AuthServerEnabled   bool
	ExternalCert        *common_tls.CertificateBundle
}

func NewAuthorizationPlugin(args *NewAuthorizationPluginArgs) (*AuthorizationPlugin, error) {
	if args.ExternalCert == nil {
		return nil, fmt.Errorf("ExternalCert cannot be nil")
	}

	c, err := contract.NewPeerCertFromCertificate(args.ExternalCert.RootCAs(), args.ExternalCert.Cert().Certificate)
	if err != nil {
		return nil, fmt.Errorf("could not create peer cert from args: %w", err)
	}

	return &AuthorizationPlugin{
		authServerEnabled:   args.AuthServerEnabled,
		ca:                  args.CA,
		serviceURL:          args.ServiceURL,
		authorizationClient: args.AuthorizationClient,
		inwayCertChain:      rawDERstoBase64(c.RawDERs()),
	}, err
}

func (plugin *AuthorizationPlugin) Serve(next ServeFunc) ServeFunc {
	return func(context *Context) error {
		if context.Token.OutwayPeerID != context.ConnectionInfo.PeerID ||
			context.Token.OutwayCertificateThumbprint != context.ConnectionInfo.CertificateThumbprint {
			inway_http.WriteError(context.Response, httperrors.O1, httperrors.AccessDenied(context.ConnectionInfo.PeerID, context.ConnectionInfo.CertificateThumbprint))
			return nil
		}

		if plugin.authServerEnabled {
			authResponse, authErr := plugin.authorizeRequest(context.Request.Header, context)
			if authErr != nil {
				context.Logger.Error("error authorizing request", zap.Error(authErr))
				inway_http.WriteError(context.Response, httperrors.IAS1, httperrors.ErrorWhileAuthorizingRequest())

				return nil
			}

			context.Logger.Info(
				"authorization result",
				zap.Bool("authorized", authResponse.Allowed),
			)

			if !authResponse.Allowed {
				inway_http.WriteError(
					context.Response,
					httperrors.IAS1,
					httperrors.Unauthorized(authResponse.Status.Reason),
				)

				return nil
			}
		}

		return next(context)
	}
}

func (plugin *AuthorizationPlugin) authorizeRequest(h http.Header, c *Context) (*authResponse, error) {
	req, err := http.NewRequest(http.MethodPost, plugin.serviceURL, http.NoBody)
	if err != nil {
		return nil, err
	}

	body, err := json.Marshal(&AuthRequest{
		Input: &AuthRequestInput{
			Headers:         h,
			Path:            c.Request.URL.Path,
			InwayCertChain:  plugin.inwayCertChain,
			OutwayCertChain: rawDERstoBase64(c.ConnectionInfo.RawDERCertificates),
		},
	})
	if err != nil {
		return nil, err
	}

	req.Body = io.NopCloser(bytes.NewBuffer(body))

	resp, err := plugin.authorizationClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("authorization service return non 200 status code. status code: %d", resp.StatusCode)
	}

	authResponse := &authResponse{}

	err = json.NewDecoder(resp.Body).Decode(authResponse)
	if err != nil {
		return nil, err
	}

	return authResponse, nil
}

func rawDERstoBase64(rawDERs [][]byte) []string {
	certs := make([]string, len(rawDERs))

	for i, c := range rawDERs {
		certs[i] = base64.URLEncoding.EncodeToString(c)
	}

	return certs
}
