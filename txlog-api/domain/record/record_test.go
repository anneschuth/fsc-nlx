// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package record_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/txlog-api/domain/record"
)

//nolint:funlen // this is a test
func Test_NewRecord(t *testing.T) {
	now := time.Now()

	tests := map[string]struct {
		args    *record.NewRecordArgs
		wantErr error
	}{
		"happy_flow": {
			args: &record.NewRecordArgs{
				TransactionID: "01899c62-eba5-7b58-b68d-000000000001",
				GroupID:       "fsc-local",
				GrantHash:     "$1$4$testhash",
				Direction:     record.DirectionIn,
				ServiceName:   "test-service-2",
				Source: &record.NewRecordSourceArgs{
					OutwayPeerID: "1",
				},
				Destination: &record.NewRecordDestinationArgs{
					ServicePeerID: "3",
				},
				CreatedAt: now,
			},
			wantErr: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			_, err := record.NewRecord(tt.args)
			assert.Equal(t, tt.wantErr, err)
		})
	}
}
