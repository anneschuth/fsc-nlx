// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"net/http"

	"github.com/go-chi/chi/v5"

	"go.nlx.io/nlx/directory-ui/app/query"
)

func (s *Server) serviceDetailHandler(w http.ResponseWriter, r *http.Request) {
	services, err := s.app.Queries.ListServices.Handle(context.Background(), "")
	if err != nil {
		s.logger.Error("list services", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	searchResults := make([]*ServicesSearchResult, len(services))

	for i, service := range services {
		ssr := &ServicesSearchResult{
			ServiceName: service.Name,
			Provider: Peer{
				ID:   service.Provider.ID,
				Name: service.Provider.Name,
			},
		}

		if service.IsDelegatedPublication() {
			ssr.Delegator = &Peer{
				ID:   service.Delegator.ID,
				Name: service.Delegator.Name,
			}
		}

		searchResults[i] = ssr
	}

	page := serviceDetailPage{
		BasePage:    s.basePage,
		Location:    "/",
		Environment: s.environment,
		Introduction: serviceDetailPageIntroduction{
			Title:       "FSC NLX Directory",
			Description: "In deze directory vindt u een overzicht van alle beschikbare API’s per omgeving.",
		},
		SearchResults:       searchResults,
		ServiceDetailDrawer: ServiceDetailDrawer{},
	}

	for _, service := range services {
		if serviceMatchesURLParams(r, service) {
			page.ServiceDetailDrawer.ServiceName = service.Name
			page.ServiceDetailDrawer.Provider = Peer{
				ID:   service.Provider.ID,
				Name: service.Provider.Name,
			}

			if service.IsDelegatedPublication() {
				page.ServiceDetailDrawer.IsDelegatedPublication = true
				page.ServiceDetailDrawer.Delegator = &Peer{
					ID:   service.Delegator.ID,
					Name: service.Delegator.Name,
				}
			}

			break
		}
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("render service detail page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}

func serviceMatchesURLParams(r *http.Request, service *query.Service) bool {
	return service.Name == chi.URLParam(r, "serviceName") &&
		service.Provider.ID == chi.URLParam(r, "peerID") &&
		(chi.URLParam(r, "delegatorID") == "" || service.Delegator.ID == chi.URLParam(r, "delegatorID"))
}
