// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	zaplogger "go.nlx.io/nlx/common/logger/zap"

	"github.com/spf13/cobra"

	"go.nlx.io/nlx/common/cmd"
	"go.nlx.io/nlx/common/logoptions"
	"go.nlx.io/nlx/common/process"
	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/common/version"
	restdirectory "go.nlx.io/nlx/directory-ui/adapters/directory/rest"
	uiport "go.nlx.io/nlx/directory-ui/ports/ui"
	"go.nlx.io/nlx/directory-ui/service"
)

var serveOpts struct {
	ListenAddress           string
	DirectoryManagerAddress string
	GroupID                 string
	Environment             string
	StaticPath              string

	logoptions.LogOptions
	cmd.TLSGroupOptions
}

//nolint:gochecknoinits,funlen,gocyclo // this is the recommended way to use cobra, also a lot of flags..
func init() {
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddress, "listen-address", "", "127.0.0.1:3001", "Address for the UI to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.DirectoryManagerAddress, "directory-manager-address", "", "", "URL of the public Directory Manager")
	serveCommand.Flags().StringVarP(&serveOpts.GroupID, "group-id", "", "", "Group ID of the FSC Group")
	serveCommand.Flags().StringVarP(&serveOpts.Environment, "environment", "", "local", "Environment of this UI. local, acc, demo, preprod or prod.")
	serveCommand.Flags().StringVarP(&serveOpts.StaticPath, "static-path", "", "public", "Path to the static web files")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogType, "log-type", "", "live", "Set the logging config. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger.")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogLevel, "log-level", "", "", "Set loglevel")
	serveCommand.Flags().StringVarP(&serveOpts.TLSGroupOptions.GroupRootCert, "tls-group-root-cert", "", "", "Absolute or relative path to the FSC Group root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSGroupOptions.GroupCertFile, "tls-group-cert", "", "", "Absolute or relative path to the FSC Group cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSGroupOptions.GroupKeyFile, "tls-group-key", "", "", "Absolute or relative path to the FSC Group key .pem")
}

var serveCommand = &cobra.Command{
	Use:   "serve",
	Short: "Start the UI",
	Run: func(cmd *cobra.Command, args []string) {
		p := process.NewProcess()

		logger, err := zaplogger.New(serveOpts.LogOptions.LogLevel, serveOpts.LogOptions.LogType)
		if err != nil {
			log.Fatalf("failed to create logger: %v", err)
		}

		logger.Info(fmt.Sprintf("version info: version: %s source-hash: %s", version.BuildVersion, version.BuildSourceHash))

		if errValidate := common_tls.VerifyPrivateKeyPermissions(serveOpts.GroupKeyFile); errValidate != nil {
			logger.Warn("invalid organization key permissions", err)
		}

		certificate, err := common_tls.NewBundleFromFiles(serveOpts.GroupCertFile, serveOpts.GroupKeyFile, serveOpts.GroupRootCert)
		if err != nil {
			logger.Fatal("loading certificate", err)
		}

		ctx := context.Background()

		directoryRepository, err := restdirectory.NewClient(serveOpts.DirectoryManagerAddress, certificate)
		if err != nil {
			logger.Fatal("create directory client", err)
		}

		app, err := service.NewApplication(&service.NewApplicationArgs{
			Context:             ctx,
			DirectoryRepository: directoryRepository,
		})
		if err != nil {
			logger.Fatal("could not create application", err)
		}

		workDir, err := os.Getwd()
		if err != nil {
			logger.Fatal("failed to get work dir", err)
		}

		staticFilesPath := filepath.Join(workDir, serveOpts.StaticPath)

		uiServer, err := uiport.New(serveOpts.Environment, staticFilesPath, logger, app)

		go func() {
			err = uiServer.ListenAndServe(serveOpts.ListenAddress)
			if err != nil {
				logger.Fatal("could not listen and serve", err)
			}
		}()

		p.Wait()

		logger.Info("starting graceful shutdown")

		gracefulCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
		defer cancel()

		err = uiServer.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown server", err)
		}

		err = directoryRepository.Shutdown()
		if err != nil {
			logger.Error("could not shutdown directory repository", err)
		}
	},
}
