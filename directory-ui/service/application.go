// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package service

import (
	"context"
	"errors"
	"fmt"

	"go.nlx.io/nlx/directory-ui/adapters/directory"
	"go.nlx.io/nlx/directory-ui/app"
	"go.nlx.io/nlx/directory-ui/app/query"
)

type NewApplicationArgs struct {
	Context             context.Context
	DirectoryRepository directory.Repository
}

func NewApplication(args *NewApplicationArgs) (*app.Application, error) {
	if args.DirectoryRepository == nil {
		return nil, errors.New("directory repository is required")
	}

	listServicesHandler, err := query.NewListServicesHandler(args.DirectoryRepository)
	if err != nil {
		return nil, fmt.Errorf("failed to create new list services handler: %s", err)
	}

	application := &app.Application{
		Queries: app.Queries{
			ListServices:     listServicesHandler,
			ListParticipants: query.NewListParticipantsHandler(args.DirectoryRepository),
		},
	}

	return application, nil
}
