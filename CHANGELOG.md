## [0.6.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.6.0...v0.6.1) (2023-10-30)

# [0.6.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.5.0...v0.6.0) (2023-10-27)


### Bug Fixes

* **controller:** add dummy avatar to the user menu ([23c3f59](https://gitlab.com/commonground/nlx/fsc-nlx/commit/23c3f5964dfc77059509ca24d7bcf29768267b27)), closes [fsc-nlx#64](https://gitlab.com/fsc-nlx/issues/64)
* **manager:** pass the Group ID to the Controller gRPC adapter ([9338557](https://gitlab.com/commonground/nlx/fsc-nlx/commit/93385575e65f4789728bc682dc5a53f21200e080)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)
* **manager:** realigned OAS with FSC standard ([0eca77f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0eca77fd395efa9d8cb13d5c9cb09997ae02b8e9)), closes [fsc-nlx#61](https://gitlab.com/fsc-nlx/issues/61)
* **outway:** prevent stripping the X-NLX-Authorization header ([008f699](https://gitlab.com/commonground/nlx/fsc-nlx/commit/008f699ea1102db410db476c20915bcc8e5696c5)), closes [fsc-nlx#32](https://gitlab.com/fsc-nlx/issues/32)


### Features

* **controller:** add check for required flags on startup ([50de330](https://gitlab.com/commonground/nlx/fsc-nlx/commit/50de3303162e8a837d18990b3a4a9b5a275cfa4f)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)
* **controller:** increase with of Outway Certificate Thumbprint field ([ef265df](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ef265dfe0a61660ae86dd4801030232c236e872d)), closes [fsc-nlx#68](https://gitlab.com/fsc-nlx/issues/68)
* **controller:** remove default value for GroupID ([d56ad40](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d56ad40924fce12752fc6841b026072d03971dec)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)
* **controller:** remove unused Settings button from the primary navigation ([508fce3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/508fce3ff7a6f1a582caa835498e3bb847346e08)), closes [fsc-nlx#65](https://gitlab.com/fsc-nlx/issues/65)
* **controller:** rename flag manager-address to manager-address-internal ([708b9a0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/708b9a043f23d17d4ba526b41d786b15ea55e2f1)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)
* **controller:** replace select with datalist element for Outway Cert Thumbprint ([172f6c1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/172f6c106a668451f9f436522042dfddbe80865b)), closes [fsc-nlx#68](https://gitlab.com/fsc-nlx/issues/68)
* **controller:** replace select with datalist element for Outway Cert Thumbprint ([a661121](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a661121b6cc2a7df44575b21715929914d106cc3)), closes [fsc-nlx#68](https://gitlab.com/fsc-nlx/issues/68)
* **controller:** replace the Contract modal to a Contract detail page ([610bd4e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/610bd4ef342d4dd7a1d14d1d02a835a3880e6bce)), closes [fsc-nlx#66](https://gitlab.com/fsc-nlx/issues/66)
* **controller:** use debug as default log level ([84ca50f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/84ca50f17464b75cb898fdf2038abce54182f008)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)
* **manager:** add FSC Logging extension version to the /get_peer_info endpoint ([674689e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/674689ece181e8eacd42795b3b82c0382747198e)), closes [fsc-nlx#53](https://gitlab.com/fsc-nlx/issues/53)
* **manager:** add Peer name filter to external /peers endpoint ([ffa8edb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ffa8edbf8ba7c647e762043395683ae5b48bef86)), closes [nlx#54](https://gitlab.com/nlx/issues/54)
* **manager:** add support for other algorithms in the /get_jwks endpoint ([a0afc17](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a0afc17ef9098608014ca07569fd7c9614cc6c63)), closes [fsc-nlx#55](https://gitlab.com/fsc-nlx/issues/55)
* use base64URL encoding without padding for Contract and Grant hashes ([cf37765](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cf3776570032f0ceb6c6a771787b9ee556c83403)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)

# [0.5.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.5...v0.5.0) (2023-10-11)


### Bug Fixes

* update outway and inway entries on database conflict ([9a1c1e8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9a1c1e83529ff3987e9f41cc3263034c45e2641a)), closes [fsc-nlx#40](https://gitlab.com/fsc-nlx/issues/40)


### Features

* **docs:** use introduction page as entry page ([bd00a1c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bd00a1cb73932b34d073d68e07e2195acdb75522)), closes [fsc-nlx#20](https://gitlab.com/fsc-nlx/issues/20)
* **manager:** add logging on error when fetching services from directory ([99dcff8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/99dcff857634dabc433c1e8ecef7685866b99e65)), closes [fsc-nlx#9](https://gitlab.com/fsc-nlx/issues/9)

## [0.4.5](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.4...v0.4.5) (2023-10-03)


### Bug Fixes

* **controller:** add AuthData to service-add-handler ([8cc4f90](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8cc4f906e665b41632fbaaefbf325bd5cf88c2c4)), closes [#32](https://gitlab.com/commonground/nlx/fsc-nlx/issues/32)

## [0.4.4](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.3...v0.4.4) (2023-10-02)


### Bug Fixes

* **docs:** use correct commonname for txlog api certificate ([a39db53](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a39db53461062feeea252aba3f39ae13d2bd6c85)), closes [fsc-nlx#35](https://gitlab.com/fsc-nlx/issues/35)

## [0.4.3](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.2...v0.4.3) (2023-09-29)


### Bug Fixes

* **controller:** pass authentication data when adding a new contract ([b7cbcf6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b7cbcf63373d9f5c403b5224323f321c723ef963)), closes [fsc-nlx#32](https://gitlab.com/fsc-nlx/issues/32)

## [0.4.2](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.1...v0.4.2) (2023-09-28)

## [0.4.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.0...v0.4.1) (2023-09-28)

# [0.4.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.3.0...v0.4.0) (2023-09-28)


### Features

* **outway:** improve logging ([ed0a913](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ed0a91308414fe858b45a829abd2e593927666fd)), closes [fsc-nlx#32](https://gitlab.com/fsc-nlx/issues/32)

# [0.3.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.7...v0.3.0) (2023-09-27)


### Features

* **controller:** add oidc authentication mechanism ([efad656](https://gitlab.com/commonground/nlx/fsc-nlx/commit/efad6563b05a6c03bac4bd7905911e93b5f6bb8d)), closes [fsc-nlx#1795](https://gitlab.com/fsc-nlx/issues/1795)

## [0.2.7](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.6...v0.2.7) (2023-09-27)

## [0.2.6](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.5...v0.2.6) (2023-09-27)

## [0.2.5](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.4...v0.2.5) (2023-09-27)

## [0.2.4](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.3...v0.2.4) (2023-09-26)

## [0.2.3](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.2...v0.2.3) (2023-09-26)

## [0.2.2](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.1...v0.2.2) (2023-09-15)

## [0.2.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.0...v0.2.1) (2023-09-15)

# [0.2.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.1.1...v0.2.0) (2023-09-14)


### Features

* **directory:** remove preprod and prod options from the Directory UI filter list ([86e36e5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/86e36e5bf24651503ff9cfe4091932fb88622799)), closes [fsc-nlx#24](https://gitlab.com/fsc-nlx/issues/24)
* **directory:** update NLX content to reflect FSC NLX ([82d4a5e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/82d4a5e2ad0c4bf6eb56b42e62d73b86586df033)), closes [fsc-nlx#24](https://gitlab.com/fsc-nlx/issues/24)

## [0.1.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.1.0...v0.1.1) (2023-09-13)

# [0.1.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.5...v0.1.0) (2023-09-12)


### Features

* rename listen-address to listen-address-ui in controller ([5faf247](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5faf247dcaa39b2ca5d1ac0d2e227e1475b96882)), closes [fsc-nlx#12](https://gitlab.com/fsc-nlx/issues/12)


### Reverts

* update node.js to v20.6.0 ([803abd9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/803abd9c9ab9283f63438e196c0d95b8f87c9c5b)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)

## [0.0.5](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.4...v0.0.5) (2023-09-08)


### Bug Fixes

* **helm:** add fsc- prefix to hostnames for the Demo deployment ([7f718a8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7f718a8297887655aee726b976844d5effda758a)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)
* **manager:** show message if Directory Peer Manager address is not passed to Manager ([13756fc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/13756fc37ac462f47d9ede167b5a13f1dda911de)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)
* **manager:** show message if Self Address is not passed to Manager ([cbb6693](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cbb6693557b7f6c49322cdc85b3ae00a3f756ff5)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)

## [0.0.4](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.3...v0.0.4) (2023-09-07)

## [0.0.3](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.2...v0.0.3) (2023-09-07)

## [0.0.2](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.1...v0.0.2) (2023-09-07)

## [0.0.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.0...v0.0.1) (2023-09-07)
