// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

//nolint:funlen // this is a test
func TestSubmitContract(t *testing.T) {
	t.Parallel()

	externalHTTPServer, _ := newService(t.Name())
	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     time.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now(),
			NotAfter:  time.Now(),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             orgB.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
					CertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             orgA.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	acceptSignature, err := contractContent.Accept(orgB.CertBundle.RootCAs(), orgB.CertBundle.Cert(), time.Now())
	assert.NoError(t, err)

	grantData := models.FSCCoreGrant_Data{}
	_ = grantData.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
		Type: models.GRANTTYPESERVICECONNECTION,
		Outway: models.FSCCoreOutway{
			CertificateThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().CertificateThumbprint().Value(),
			PeerId:                contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
		},
		Service: models.FSCCoreService{
			Name:   "parkeerrechten",
			PeerId: contractContent.Grants().ServiceConnectionGrants()[0].Service().Peer().ID().Value(),
		},
	})

	// Act
	submitContractResp, err := client.SubmitContractWithResponse(context.Background(), &models.SubmitContractParams{
		FscManagerAddress: orgB.ManagerAddress,
	}, models.SubmitContractJSONRequestBody(
		models.SubmitContractJSONBody{
			ContractContent: models.FSCCoreContractContent{
				GroupId:       contractContent.GroupID(),
				HashAlgorithm: models.HASHALGORITHMSHA3512,
				Id:            contractContent.ID().String(),
				CreatedAt:     contractContent.CreatedAt().Unix(),
				Grants: []models.FSCCoreGrant{
					{
						Data: grantData,
					},
				},
				Validity: models.FSCCoreValidity{
					NotBefore: contractContent.NotBefore().Unix(),
					NotAfter:  contractContent.NotAfter().Unix(),
				},
			},
			Signature: acceptSignature.JWS(),
		}),
	)

	// Assert
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, submitContractResp.StatusCode())

	contracts, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantHash: &[]string{
			contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
		},
	})
	assert.NoError(t, err)
	assert.Equal(t, 1, len(contracts.JSON200.Contracts))

	expectedAcceptSignatures := models.FSCCoreSignatureMap{
		orgB.GetPeerID(): acceptSignature.JWS(),
	}

	assert.Equal(t, expectedAcceptSignatures, contracts.JSON200.Contracts[0].Signatures.Accept)
}
