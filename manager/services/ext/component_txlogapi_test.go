/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

//nolint:unused // most methods are unused but added to comply with the Manager interface
package externalservice_test

import (
	"context"
	"time"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/adapters/txlog"
	txlogrecord "go.nlx.io/nlx/txlog-api/domain/record"
)

type testTXLogAPI struct{}

func (t *testTXLogAPI) ListRecords(_ context.Context, request *txlog.ListRecordsRequest) ([]*txlog.Record, error) {
	// At least 1 filter must contain the PeerID of the connecting Peer
	var connectingPeerID string
	for _, filter := range request.Filters {
		connectingPeerID = filter.PeerID
	}

	if connectingPeerID == "" {
		return nil, errors.New("missing connecting PeerID")
	}

	if request == nil {
		return nil, errors.New("missing request parameters")
	}

	mockTime := time.Date(2023, 1, 1, 1, 42, 0, 0, time.Local)
	recordDelegatedOut := &txlog.Record{
		GroupID:     "fsc-local",
		GrantHash:   "$1$4$testhash",
		ServiceName: "test-service",
		CreatedAt:   mockTime,
		Source: &txlog.RecordDelegatedSource{
			OutwayPeerID:    "1",
			DelegatorPeerID: "3",
		},
		Destination: &txlog.RecordDelegatedDestination{
			ServicePeerID:   "2",
			DelegatorPeerID: "3",
		},
		Direction:     txlogrecord.DirectionOut,
		TransactionID: "01899c62-eba5-7b58-b68d-000000000001",
	}
	recordDelegatedIn := &txlog.Record{
		GroupID:     "fsc-local",
		GrantHash:   "$1$4$testhash",
		ServiceName: "test-service",
		CreatedAt:   mockTime,
		Source: &txlog.RecordDelegatedSource{
			OutwayPeerID:    "1",
			DelegatorPeerID: "3",
		},
		Destination: &txlog.RecordDelegatedDestination{
			ServicePeerID:   "2",
			DelegatorPeerID: "3",
		},
		Direction:     txlogrecord.DirectionIn,
		TransactionID: "01899c62-eba5-7b58-b68d-000000000001",
	}

	recordOut := &txlog.Record{
		GroupID:     "fsc-local",
		GrantHash:   "$1$4$testhash2",
		ServiceName: "test-service",
		CreatedAt:   mockTime,
		Source: &txlog.RecordSource{
			OutwayPeerID: "1",
		},
		Destination: &txlog.RecordDestination{
			ServicePeerID: "2",
		},
		Direction:     txlogrecord.DirectionOut,
		TransactionID: "01899c62-eba5-7b58-b68d-000000000002",
	}
	recordIn := &txlog.Record{
		GroupID:     "fsc-local",
		GrantHash:   "$1$4$testhash2",
		ServiceName: "test-service",
		CreatedAt:   mockTime,
		Source: &txlog.RecordSource{
			OutwayPeerID: "1",
		},
		Destination: &txlog.RecordDestination{
			ServicePeerID: "2",
		},
		Direction:     txlogrecord.DirectionIn,
		TransactionID: "01899c62-eba5-7b58-b68d-000000000002",
	}

	return []*txlog.Record{
		recordDelegatedOut,
		recordDelegatedIn,
		recordOut,
		recordIn,
	}, nil
}
