// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import "context"

type FakeController struct{}

func newFakeController() *FakeController {
	return &FakeController{}
}

func (c *FakeController) GetInwayAddressForService(_ context.Context, _ string) (string, error) {
	return "https://inway.address.com:443", nil
}

func (c *FakeController) GetServiceEndpointURL(_ context.Context, _, _ string) (string, error) {
	return "https://service.endpoint.com:443", nil
}
