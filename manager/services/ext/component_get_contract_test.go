// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/manager/apps/int/command"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

//nolint:funlen // this is a test
func TestGetContractServiceConnectionGrant(t *testing.T) {
	t.Parallel()

	externalHTTPServer, application := newService(t.Name())
	defer externalHTTPServer.Close()

	// Arrange

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     time.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now(),
			NotAfter:  time.Now(),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgB.GetPeerID(),
					},
					CertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = application.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     contractContent.Hash().Algorithm(),
		ID:                contractContent.ID().Bytes(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				CertificateThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().CertificateThumbprint().Value(),
				OutwayPeerID:          contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				ServicePeerID:         contractContent.Grants().ServiceConnectionGrants()[0].Service().Peer().ID().Value(),
				ServiceName:           contractContent.Grants().ServiceConnectionGrants()[0].Service().Name(),
			},
		},
		CreatedAt: time.Now(),
	})
	assert.NoError(t, err)

	// Act
	contractResponse, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantHash: &[]string{contractContent.Grants().ServiceConnectionGrants()[0].Hash().String()},
	})

	// Assert
	// assert all field in the contract, see submit_contract test for asserting 1 field
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, contractResponse.StatusCode())
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts))
	assert.Equal(t, contractContent.ID().String(), contractResponse.JSON200.Contracts[0].Content.Id)
	assert.Equal(t, contractContent.GroupID(), contractResponse.JSON200.Contracts[0].Content.GroupId)
	assert.Equal(t, contractContent.NotBefore().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotBefore)
	assert.Equal(t, contractContent.NotAfter().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotAfter)

	// Asserting grants
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Content.Grants))

	serviceConnectionGrantType, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.Discriminator()
	assert.NoError(t, err)
	assert.Equal(t, "GRANT_TYPE_SERVICE_CONNECTION", serviceConnectionGrantType)

	serviceConnectionGrant, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.AsFSCCoreGrantServiceConnection()
	assert.NoError(t, err)

	assert.Equal(t, orgB.GetPeerID(), serviceConnectionGrant.Outway.PeerId)
	assert.Equal(t, orgB.CertBundle.CertificateThumbprint(), serviceConnectionGrant.Outway.CertificateThumbprint)
	assert.Equal(t, orgA.GetPeerID(), serviceConnectionGrant.Service.PeerId)
	assert.Equal(t, contractContent.Grants().ServiceConnectionGrants()[0].Service().Name(), serviceConnectionGrant.Service.Name)
	// Asserting signatures
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Signatures.Accept))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Revoke))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Reject))

}

//nolint:funlen // this is a test
func TestGetContractMultipleGrants(t *testing.T) {
	t.Parallel()

	externalHTTPServer, application := newService(t.Name())
	defer externalHTTPServer.Close()

	// Arrange

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgA.CertBundle)
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     time.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now(),
			NotAfter:  time.Now(),
		},
		Grants: []interface{}{
			&contract.NewGrantDelegatedServicePublicationArgs{
				Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             orgA.GetPeerID(),
						Name:           "test-directory",
						ManagerAddress: "https://test-directory.example.com:443",
					},
				},
				Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
					Name: "test-service",
					Peer: &contract.NewPeerArgs{
						ID:   orgA.GetPeerID(),
						Name: orgA.GetName(),
					},
				},
				Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID:   orgB.GetPeerID(),
						Name: orgB.GetName(),
					},
				},
			},
			&contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:   orgB.GetPeerID(),
						Name: "directory",
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:   orgA.GetPeerID(),
						Name: orgA.GetName(),
					},
					Name: "test-service",
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = application.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     contractContent.Hash().Algorithm(),
		ID:                contractContent.ID().Bytes(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantDelegatedServicePublicationArgs{
				DirectoryPeerID: contractContent.Grants().DelegatedServicePublicationGrants()[0].Directory().Peer().ID().Value(),
				ServiceName:     contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Name(),
				ServicePeerID:   contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Peer().ID().Value(),
				DelegatorPeerID: contractContent.Grants().DelegatedServicePublicationGrants()[0].Delegator().Peer().ID().Value(),
			},
			&command.GrantServicePublicationArgs{
				DirectoryPeerID: contractContent.Grants().ServicePublicationGrants()[0].Directory().Peer().ID().Value(),
				ServicePeerID:   contractContent.Grants().ServicePublicationGrants()[0].Service().Peer().ID().Value(),
				ServiceName:     contractContent.Grants().ServicePublicationGrants()[0].Service().Name(),
			},
		},
		CreatedAt: time.Now(),
	})
	assert.NoError(t, err)

	// Act
	contractResponse, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{})

	// Assert
	// assert all field in the contract, see submit_contract test for asserting 1 field
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, contractResponse.StatusCode())
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts))
	assert.Equal(t, contractContent.ID().String(), contractResponse.JSON200.Contracts[0].Content.Id)
	assert.Equal(t, contractContent.GroupID(), contractResponse.JSON200.Contracts[0].Content.GroupId)
	assert.Equal(t, contractContent.NotBefore().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotBefore)
	assert.Equal(t, contractContent.NotAfter().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotAfter)

	// Asserting grants
	assert.Equal(t, 2, len(contractResponse.JSON200.Contracts[0].Content.Grants))

	servicePublicationGrantType, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.Discriminator()
	assert.NoError(t, err)
	assert.Equal(t, "GRANT_TYPE_SERVICE_PUBLICATION", servicePublicationGrantType)

	delegatedServicePublicationGrantType, err := contractResponse.JSON200.Contracts[0].Content.Grants[1].Data.Discriminator()
	assert.NoError(t, err)
	assert.Equal(t, "GRANT_TYPE_DELEGATED_SERVICE_PUBLICATION", delegatedServicePublicationGrantType)

	servicePublicationGrant, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.AsFSCCoreGrantServicePublication()
	assert.NoError(t, err)

	assert.Equal(t, orgA.GetPeerID(), servicePublicationGrant.Service.PeerId)
	assert.Equal(t, contractContent.Grants().ServicePublicationGrants()[0].Service().Name(), servicePublicationGrant.Service.Name)
	assert.Equal(t, orgB.GetPeerID(), servicePublicationGrant.Directory.PeerId)

	serviceDelegatedPublicationGrant, err := contractResponse.JSON200.Contracts[0].Content.Grants[1].Data.AsFSCCoreGrantDelegatedServicePublication()
	assert.NoError(t, err)

	assert.Equal(t, orgB.GetPeerID(), serviceDelegatedPublicationGrant.Delegator.PeerId)
	assert.Equal(t, orgA.GetPeerID(), serviceDelegatedPublicationGrant.Service.PeerId)
	assert.Equal(t, contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Name(), serviceDelegatedPublicationGrant.Service.Name)
	assert.Equal(t, orgB.GetPeerID(), serviceDelegatedPublicationGrant.Directory.PeerId)

	// Asserting signatures
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Signatures.Accept))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Revoke))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Reject))

}

//nolint:funlen // this is a test
func TestGetContractWithGrantType(t *testing.T) {
	t.Parallel()

	externalHTTPServer, application := newService(t.Name())
	defer externalHTTPServer.Close()

	// Arrange
	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgA.CertBundle)
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     time.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now(),
			NotAfter:  time.Now(),
		},
		Grants: []interface{}{
			&contract.NewGrantPeerRegistrationArgs{
				Directory: &contract.NewGrantPeerRegistrationDirectoryArgs{
					PeerID: orgB.GetPeerID(),
				},
				Peer: &contract.NewGrantPeerRegistrationPeerArgs{
					ID:   orgA.GetPeerID(),
					Name: orgA.GetName(),
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = application.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     contractContent.Hash().Algorithm(),
		ID:                contractContent.ID().Bytes(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantPeerRegistrationArgs{
				PeerID:          contractContent.Grants().PeerRegistrationGrant().Peer().ID().Value(),
				DirectoryPeerID: contractContent.Grants().PeerRegistrationGrant().Directory().PeerID().Value(),
				PeerName:        contractContent.Grants().PeerRegistrationGrant().Peer().Name(),
			},
		},
		CreatedAt: time.Now(),
	})
	assert.NoError(t, err)

	// Act
	grantType := models.FSCCoreGrantType("GRANT_TYPE_PEER_REGISTRATION")
	contractResponse, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantType: &grantType,
	})

	// Assert
	// assert all field in the contract, see submit_contract test for asserting 1 field
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, contractResponse.StatusCode())
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts))
	assert.Equal(t, contractContent.ID().String(), contractResponse.JSON200.Contracts[0].Content.Id)
	assert.Equal(t, contractContent.GroupID(), contractResponse.JSON200.Contracts[0].Content.GroupId)
	assert.Equal(t, contractContent.NotBefore().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotBefore)
	assert.Equal(t, contractContent.NotAfter().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotAfter)

	// Asserting grants
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Content.Grants))

	assert.NoError(t, err)

	peerRegistrationGrant, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.AsFSCCoreGrantPeerRegistration()
	assert.NoError(t, err)

	assert.Equal(t, orgB.GetPeerID(), peerRegistrationGrant.Peer.Id)
	assert.Equal(t, contractContent.Grants().PeerRegistrationGrant().Peer().Name(), peerRegistrationGrant.Peer.Name)
	assert.Equal(t, orgB.GetPeerID(), peerRegistrationGrant.Directory.PeerId)

	// Asserting signatures
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Signatures.Accept))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Revoke))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Reject))

}
