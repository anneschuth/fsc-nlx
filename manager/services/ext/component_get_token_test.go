// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/common/accesstoken"
	"go.nlx.io/nlx/manager/apps/int/command"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

func TestGetToken(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name())
	defer externalHTTPServer.Close()

	peerCert, err := contract.NewPeerCertFromCertificate(orgA.CertBundle.RootCAs(), orgA.CertBundle.Cert().Certificate)
	assert.NoError(t, err)

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	clientOrgC, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgC.CertBundle)
	assert.NoError(t, err)

	serviceName := "parkeerrechten"
	groupID := "fsc-local"

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContentServiceConnection, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     time.Now(),
		GroupID:       groupID,
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now(),
			NotAfter:  time.Now().Add(time.Hour),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgB.GetPeerID(),
					},
					CertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgA.GetPeerID(),
					},
					Name: serviceName,
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     contractContentServiceConnection.Hash().Algorithm(),
		ID:                contractContentServiceConnection.ID().Bytes(),
		GroupID:           contractContentServiceConnection.GroupID(),
		ContractNotBefore: contractContentServiceConnection.NotBefore(),
		ContractNotAfter:  contractContentServiceConnection.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				CertificateThumbprint: contractContentServiceConnection.Grants().ServiceConnectionGrants()[0].Outway().CertificateThumbprint().Value(),
				OutwayPeerID:          orgB.GetPeerID(),
				ServicePeerID:         orgA.GetPeerID(),
				ServiceName:           contractContentServiceConnection.Grants().ServiceConnectionGrants()[0].Service().Name(),
			},
		},
		CreatedAt: time.Now(),
	})
	assert.NoError(t, err)

	acceptSignatureOrgB, err := contractContentServiceConnection.Accept(orgB.CertBundle.RootCAs(), orgB.CertBundle.Cert(), time.Now())
	assert.NoError(t, err)

	grantData := models.FSCCoreGrant_Data{}
	_ = grantData.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
		Type: models.GRANTTYPESERVICECONNECTION,
		Outway: models.FSCCoreOutway{
			CertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
			PeerId:                orgB.GetPeerID(),
		},
		Service: models.FSCCoreService{
			Name:   serviceName,
			PeerId: orgA.GetPeerID(),
		},
	})

	resp, err := clientOrgB.AcceptContractWithResponse(context.Background(), contractContentServiceConnection.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: orgB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: models.FSCCoreContractContent{
			CreatedAt: contractContentServiceConnection.CreatedAt().Unix(),
			Grants: []models.FSCCoreGrant{
				{
					Data: grantData,
				},
			},
			GroupId:       contractContentServiceConnection.GroupID(),
			HashAlgorithm: models.HASHALGORITHMSHA3512,
			Id:            contractContentServiceConnection.ID().String(),
			Validity: models.FSCCoreValidity{
				NotAfter:  contractContentServiceConnection.NotAfter().Unix(),
				NotBefore: contractContentServiceConnection.NotBefore().Unix(),
			},
		},
		Signature: acceptSignatureOrgB.JWS(),
	})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	println(string(resp.Body))

	contractContentDelegatedServiceConnection, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     time.Now(),
		GroupID:       groupID,
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now(),
			NotAfter:  time.Now().Add(time.Hour),
		},
		Grants: []interface{}{
			&contract.NewGrantDelegatedServiceConnectionArgs{
				Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgB.GetPeerID(),
					},
					CertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
				},
				Service: &contract.NewGrantDelegatedServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgA.GetPeerID(),
					},
					Name: serviceName,
				},
				Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgC.GetPeerID(),
					},
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     contractContentDelegatedServiceConnection.Hash().Algorithm(),
		ID:                contractContentDelegatedServiceConnection.ID().Bytes(),
		GroupID:           contractContentDelegatedServiceConnection.GroupID(),
		ContractNotBefore: contractContentDelegatedServiceConnection.NotBefore(),
		ContractNotAfter:  contractContentDelegatedServiceConnection.NotAfter(),
		Grants: []interface{}{
			&command.GrantDelegatedServiceConnectionArgs{
				OutwayCertificateThumbprint: contractContentDelegatedServiceConnection.Grants().DelegatedServiceConnectionGrants()[0].Outway().CertificateThumbprint().Value(),
				OutwayPeerID:                orgB.GetPeerID(),
				ServicePeerID:               orgA.GetPeerID(),
				ServiceName:                 contractContentDelegatedServiceConnection.Grants().DelegatedServiceConnectionGrants()[0].Service().Name(),
				DelegatorPeerID:             contractContentDelegatedServiceConnection.Grants().DelegatedServiceConnectionGrants()[0].Delegator().Peer().ID().Value(),
			},
		},
		CreatedAt: time.Now(),
	})
	assert.NoError(t, err)

	acceptSignatureOrgB, err = contractContentDelegatedServiceConnection.Accept(orgB.CertBundle.RootCAs(), orgB.CertBundle.Cert(), time.Now())
	assert.NoError(t, err)

	acceptSignatureOrgC, err := contractContentDelegatedServiceConnection.Accept(orgC.CertBundle.RootCAs(), orgC.CertBundle.Cert(), time.Now())
	assert.NoError(t, err)

	grantData = models.FSCCoreGrant_Data{}
	_ = grantData.FromFSCCoreGrantDelegatedServiceConnection(models.FSCCoreGrantDelegatedServiceConnection{
		Type: models.GRANTTYPEDELEGATEDSERVICECONNECTION,
		Outway: models.FSCCoreOutway{
			CertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
			PeerId:                orgB.CertBundle.GetOrganizationInfo().SerialNumber,
		},
		Service: models.FSCCoreService{
			Name:   serviceName,
			PeerId: orgA.CertBundle.GetOrganizationInfo().SerialNumber,
		},
		Delegator: models.FSCCoreDelegator{
			PeerId: orgC.CertBundle.GetOrganizationInfo().SerialNumber,
		},
	})

	modelContractContentDelegatedConnection := models.AcceptContractJSONRequestBody{
		ContractContent: models.FSCCoreContractContent{
			CreatedAt: contractContentDelegatedServiceConnection.CreatedAt().Unix(),
			Grants: []models.FSCCoreGrant{
				{
					Data: grantData,
				},
			},
			GroupId:       contractContentDelegatedServiceConnection.GroupID(),
			HashAlgorithm: models.HASHALGORITHMSHA3512,
			Id:            contractContentDelegatedServiceConnection.ID().String(),
			Validity: models.FSCCoreValidity{
				NotAfter:  contractContentDelegatedServiceConnection.NotAfter().Unix(),
				NotBefore: contractContentDelegatedServiceConnection.NotBefore().Unix(),
			},
		},
		Signature: acceptSignatureOrgB.JWS(),
	}

	resp, err = clientOrgB.AcceptContractWithResponse(context.Background(), contractContentDelegatedServiceConnection.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: orgB.ManagerAddress,
	}, modelContractContentDelegatedConnection)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.StatusCode())

	modelContractContentDelegatedConnection.Signature = acceptSignatureOrgC.JWS()
	resp, err = clientOrgC.AcceptContractWithResponse(context.Background(), contractContentDelegatedServiceConnection.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: orgC.ManagerAddress,
	}, modelContractContentDelegatedConnection)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.StatusCode())

	tokenExpirationDate := testClock.Now().Add(accessTokenTTL)

	tests := map[string]struct {
		GetTokenParams models.GetTokenFormdataRequestBody
		ExpectedToken  *accesstoken.DecodedToken
	}{
		"service_connection_grant": {
			GetTokenParams: models.GetTokenFormdataRequestBody{
				GrantType: models.ClientCredentials,
				Scope:     contractContentServiceConnection.Grants().ServiceConnectionGrants()[0].Hash().String(),
			},
			ExpectedToken: &accesstoken.DecodedToken{
				GroupID:                     "fsc-local",
				GrantHash:                   contractContentServiceConnection.Grants().ServiceConnectionGrants()[0].Hash().String(),
				OutwayPeerID:                orgB.CertBundle.GetOrganizationInfo().SerialNumber,
				OutwayCertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
				ServiceName:                 serviceName,
				ServiceInwayAddress:         "https://inway.address.com:443",
				ServicePeerID:               orgA.CertBundle.GetOrganizationInfo().SerialNumber,
				ExpiryDate:                  tokenExpirationDate,
			},
		},
		"delegated_service_connection_grant": {
			GetTokenParams: models.GetTokenFormdataRequestBody{
				GrantType: models.ClientCredentials,
				Scope:     contractContentDelegatedServiceConnection.Grants().DelegatedServiceConnectionGrants()[0].Hash().String(),
			},
			ExpectedToken: &accesstoken.DecodedToken{
				GroupID:                     "fsc-local",
				GrantHash:                   contractContentDelegatedServiceConnection.Grants().DelegatedServiceConnectionGrants()[0].Hash().String(),
				OutwayPeerID:                orgB.CertBundle.GetOrganizationInfo().SerialNumber,
				OutwayCertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
				OutwayDelegatorPeerID:       orgC.CertBundle.GetOrganizationInfo().SerialNumber,
				ServiceName:                 serviceName,
				ServiceInwayAddress:         "https://inway.address.com:443",
				ServicePeerID:               orgA.CertBundle.GetOrganizationInfo().SerialNumber,
				ExpiryDate:                  tokenExpirationDate,
			},
		},
	}

	for testName, test := range tests {
		testCase := test

		t.Run(testName, func(t *testing.T) {
			// Act
			tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), testCase.GetTokenParams)

			// Assert
			assert.NoError(t, errReq)
			assert.Equal(t, http.StatusOK, tokenResp.StatusCode())

			receivedToken, errTokenDecode := accesstoken.DecodeFromString(testClock, peerCert, tokenResp.JSON200.AccessToken)
			assert.NoError(t, errTokenDecode)

			assert.Equal(t, testCase.ExpectedToken, receivedToken)
		})
	}
}
