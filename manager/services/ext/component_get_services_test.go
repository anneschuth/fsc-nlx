// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/manager/apps/int/command"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

func TestGetServices(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name())
	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     time.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now(),
			NotAfter:  time.Now().Add(time.Hour),
		},
		Grants: []interface{}{
			&contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             orgB.GetPeerID(),
						Name:           orgB.GetName(),
						ManagerAddress: orgB.ManagerAddress,
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             orgA.GetPeerID(),
						Name:           orgA.GetName(),
						ManagerAddress: orgA.ManagerAddress,
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     contractContent.Hash().Algorithm(),
		ID:                contractContent.ID().Bytes(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServicePublicationArgs{
				DirectoryPeerID: contractContent.Grants().ServicePublicationGrants()[0].Directory().Peer().ID().Value(),
				ServicePeerID:   contractContent.Grants().ServicePublicationGrants()[0].Service().Peer().ID().Value(),
				ServiceName:     contractContent.Grants().ServicePublicationGrants()[0].Service().Name(),
			},
		},
		CreatedAt: time.Now(),
	})
	assert.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(orgB.CertBundle.RootCAs(), orgB.CertBundle.Cert(), time.Now())
	assert.NoError(t, err)

	grantData := models.FSCCoreGrant_Data{}
	_ = grantData.FromFSCCoreGrantServicePublication(models.FSCCoreGrantServicePublication{
		Type: models.GRANTTYPESERVICEPUBLICATION,
		Directory: models.FSCCoreDirectory{
			PeerId: orgB.GetPeerID(),
		},
		Service: models.FSCCoreServicePublication{
			Name:     "parkeerrechten",
			PeerId:   orgA.GetPeerID(),
			Protocol: nil,
		},
	})

	// Note: the 'hash' argument is probably redundant. There's a proposal to adjust the RFC.
	// Hardcoded with the value 'hash' until this is resolved.
	// See https://gitlab.com/commonground/standards/fsc/-/issues/50
	acceptContractResp, err := client.AcceptContractWithResponse(context.Background(), "hash", &models.AcceptContractParams{
		FscManagerAddress: orgB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: models.FSCCoreContractContent{
			CreatedAt: contractContent.CreatedAt().Unix(),
			Grants: []models.FSCCoreGrant{
				{
					Data: grantData,
				},
			},
			GroupId:       contractContent.GroupID(),
			HashAlgorithm: models.HASHALGORITHMSHA3512,
			Id:            contractContent.ID().String(),
			Validity: models.FSCCoreValidity{
				NotAfter:  contractContent.NotAfter().Unix(),
				NotBefore: contractContent.NotBefore().Unix(),
			},
		},
		Signature: acceptSignatureOrgB.JWS(),
	})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, acceptContractResp.StatusCode())
	assert.Equal(t, "", string(acceptContractResp.Body))

	// Act
	peerID := orgA.GetPeerID()
	serviceName := "parkeerrechten"

	res, err := client.GetServicesWithResponse(context.Background(), &models.GetServicesParams{
		Cursor:      nil,
		Limit:       nil,
		SortOrder:   nil,
		PeerId:      &peerID,
		ServiceName: &serviceName,
	})
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, http.StatusOK, res.StatusCode())

	data := models.FSCCoreServiceListing_Data{}

	_ = data.FromFSCCoreServiceListingService(models.FSCCoreServiceListingService{
		Type: models.FSCCoreServiceListingServiceTypeTYPESERVICE,
		Name: "parkeerrechten",
		Peer: models.FSCCorePeer{
			Id:             orgA.GetPeerID(),
			ManagerAddress: orgA.ManagerAddress,
			Name:           orgA.GetName(),
		},
		Protocol: nil,
	})

	expectedServices := []models.FSCCoreServiceListing{
		{
			Data: data,
		},
	}

	assert.Equal(t, expectedServices, res.JSON200.Services)
}
