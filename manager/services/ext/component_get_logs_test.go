// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"path/filepath"
	"testing"
	"time"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
	"go.nlx.io/nlx/testing/testingutils"
)

func TestGetLogs(t *testing.T) {

	t.Parallel()

	externalHTTPServer, _ := newService(t.Name())
	defer externalHTTPServer.Close()

	orgACertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "../", "testing", "pki"), testingutils.NLXTestOrgA)
	assert.NoError(t, err)

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgACertBundle)
	assert.NoError(t, err)

	// Arrange
	// Act
	logs, err := client.GetLogsWithResponse(context.Background(), &models.GetLogsParams{})

	assert.NoError(t, err)

	// Assert
	assert.Len(t, logs.JSON200.Records, 4)
	recordDelegatedOut := logs.JSON200.Records[0]
	recordDelegatedIn := logs.JSON200.Records[1]
	recordOut := logs.JSON200.Records[2]
	recordIn := logs.JSON200.Records[3]

	assert.Equal(t, "$1$4$testhash", recordDelegatedOut.GrantHash)
	assert.Equal(t, "$1$4$testhash", recordDelegatedIn.GrantHash)
	assert.Equal(t, models.DIRECTIONOUTGOING, recordDelegatedOut.Direction)
	assert.Equal(t, models.DIRECTIONINCOMING, recordDelegatedIn.Direction)

	expectedDelegatedTxId, _ := uuid.FromString("01899c62-eba5-7b58-b68d-000000000001")
	assert.Equal(t, models.FSCLoggingTransactionID(expectedDelegatedTxId), recordDelegatedOut.TransactionId)
	assert.Equal(t, models.FSCLoggingTransactionID(expectedDelegatedTxId), recordDelegatedIn.TransactionId)

	assert.Equal(t, "test-service", recordDelegatedOut.ServiceName)
	assert.Equal(t, "test-service", recordDelegatedIn.ServiceName)

	mockTime := time.Date(2023, 1, 1, 1, 42, 0, 0, time.Local)
	assert.Equal(t, mockTime.Unix(), recordDelegatedOut.CreatedAt)
	assert.Equal(t, mockTime.Unix(), recordDelegatedIn.CreatedAt)

	sourceDelegatedOut, _ := recordDelegatedOut.Source.AsFSCLoggingSourceDelegated()
	assert.Equal(t, models.SOURCETYPEDELEGATEDSOURCE, sourceDelegatedOut.Type)
	assert.Equal(t, "1", sourceDelegatedOut.OutwayPeerId)
	assert.Equal(t, "3", sourceDelegatedOut.DelegatorPeerId)

	sourceDelegatedIn, _ := recordDelegatedIn.Source.AsFSCLoggingSourceDelegated()
	assert.Equal(t, "1", sourceDelegatedIn.OutwayPeerId)
	assert.Equal(t, "3", sourceDelegatedIn.DelegatorPeerId)

	destinationDelegatedOut, _ := recordDelegatedOut.Destination.AsFSCLoggingDestinationDelegated()
	assert.Equal(t, "2", destinationDelegatedOut.ServicePeerId)
	assert.Equal(t, "3", destinationDelegatedOut.DelegatorPeerId)

	destinationDelegatedIn, _ := recordDelegatedIn.Destination.AsFSCLoggingDestinationDelegated()
	assert.Equal(t, "2", destinationDelegatedIn.ServicePeerId)
	assert.Equal(t, "3", destinationDelegatedIn.DelegatorPeerId)

	assert.Equal(t, "$1$4$testhash2", recordOut.GrantHash)
	assert.Equal(t, "$1$4$testhash2", recordIn.GrantHash)
	assert.Equal(t, models.DIRECTIONOUTGOING, recordOut.Direction)
	assert.Equal(t, models.DIRECTIONINCOMING, recordIn.Direction)

	expectedTxId, _ := uuid.FromString("01899c62-eba5-7b58-b68d-000000000002")
	assert.Equal(t, models.FSCLoggingTransactionID(expectedTxId), recordOut.TransactionId)
	assert.Equal(t, models.FSCLoggingTransactionID(expectedTxId), recordIn.TransactionId)

	assert.Equal(t, "test-service", recordOut.ServiceName)
	assert.Equal(t, "test-service", recordIn.ServiceName)

	assert.Equal(t, mockTime.Unix(), recordOut.CreatedAt)
	assert.Equal(t, mockTime.Unix(), recordIn.CreatedAt)

	sourceOut, _ := recordOut.Source.AsFSCLoggingSourceDelegated()
	assert.Equal(t, "1", sourceOut.OutwayPeerId)

	sourceIn, _ := recordIn.Source.AsFSCLoggingSourceDelegated()
	assert.Equal(t, "1", sourceIn.OutwayPeerId)

	destinationOut, _ := recordOut.Destination.AsFSCLoggingDestinationDelegated()
	assert.Equal(t, "2", destinationOut.ServicePeerId)

	destinationIn, _ := recordIn.Destination.AsFSCLoggingDestinationDelegated()
	assert.Equal(t, "2", destinationIn.ServicePeerId)
}
