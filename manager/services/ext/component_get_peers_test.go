// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
	"go.nlx.io/nlx/testing/testingutils"
)

func TestGetPeers(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, _ := newService(t.Name())
	defer externalHTTPServer.Close()

	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "../", "testing", "pki"), testingutils.NLXTestOrgA)

	clientOrg, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgCertBundle)
	assert.NoError(t, err)

	orgBCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "../", "testing", "pki"), testingutils.NLXTestOrgB)
	assert.NoError(t, err)

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgBCertBundle)
	assert.NoError(t, err)

	managerAddressOrg := "https://fsc-manager-org.nlx.io:8443"
	managerAddressOrgB := "https://fsc-manager-org-b.nlx.io:8443"

	respAnnounce, err := clientOrg.AnnounceWithResponse(context.Background(), &models.AnnounceParams{FscManagerAddress: managerAddressOrg})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, respAnnounce.StatusCode())

	respAnnounce, err = clientOrgB.AnnounceWithResponse(context.Background(), &models.AnnounceParams{FscManagerAddress: managerAddressOrgB})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, respAnnounce.StatusCode())

	limit := models.FSCCoreQueryPaginationLimit(10)
	sortOrder := models.FSCCoreSortOrderSORTORDERASCENDING
	peerName := orgB.GetName()

	tests := map[string]struct {
		GetPeersParams *models.GetPeersParams
		ExpectedPeers  []models.FSCCorePeer
	}{
		"no_filters": {
			GetPeersParams: &models.GetPeersParams{
				Cursor:    nil,
				Limit:     &limit,
				SortOrder: &sortOrder,
			},
			ExpectedPeers: []models.FSCCorePeer{
				{
					Id:             orgCertBundle.GetOrganizationInfo().SerialNumber,
					ManagerAddress: managerAddressOrg,
					Name:           orgCertBundle.GetOrganizationInfo().Name,
				}, {
					Id:             orgB.GetPeerID(),
					ManagerAddress: managerAddressOrgB,
					Name:           orgB.GetName(),
				},
			},
		},
		"peer_id_filter": {
			GetPeersParams: &models.GetPeersParams{
				Cursor:    nil,
				Limit:     &limit,
				SortOrder: &sortOrder,
				PeerId: &[]models.FSCCorePeerID{
					orgCertBundle.GetOrganizationInfo().SerialNumber,
				},
			},
			ExpectedPeers: []models.FSCCorePeer{
				{
					Id:             orgCertBundle.GetOrganizationInfo().SerialNumber,
					ManagerAddress: managerAddressOrg,
					Name:           orgCertBundle.GetOrganizationInfo().Name,
				},
			},
		}, "peer_name_filter": {
			GetPeersParams: &models.GetPeersParams{
				Cursor:    nil,
				Limit:     &limit,
				SortOrder: &sortOrder,
				PeerName:  &peerName,
			},
			ExpectedPeers: []models.FSCCorePeer{
				{
					Id:             orgB.GetPeerID(),
					ManagerAddress: managerAddressOrgB,
					Name:           orgB.GetName(),
				},
			},
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			// Act
			respPeers, errReq := clientOrg.GetPeersWithResponse(context.Background(), test.GetPeersParams)

			// Assert
			assert.NoError(t, errReq)
			assert.Equal(t, http.StatusOK, respPeers.StatusCode())
			assert.Equal(t, test.ExpectedPeers, respPeers.JSON200.Peers)
		})
	}
}
