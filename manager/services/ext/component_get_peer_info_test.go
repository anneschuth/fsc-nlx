// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

func TestGetPeerInfo(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, _ := newService(t.Name())
	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	// Act
	res, err := client.GetPeerInfoWithResponse(context.Background())
	assert.NoError(t, err)

	// Assert
	delegationVersion := models.FSCCoreDelegationVersionN100
	loggingVersion := models.N100

	assert.Equal(t, http.StatusOK, res.StatusCode())
	assert.Equal(t, orgA.GetPeerID(), res.JSON200.PeerId)
	assert.Equal(t, orgA.GetName(), res.JSON200.PeerName)
	assert.Equal(t, models.FSCCoreEnabledExtensions{
		EXTENSIONDELEGATION:         &delegationVersion,
		EXTENSIONTRANSACTIONLOGGING: &loggingVersion,
	}, res.JSON200.EnabledExtensions)
	assert.Equal(t, models.FSCCoreFscVersionN100, res.JSON200.FscVersion)
}
