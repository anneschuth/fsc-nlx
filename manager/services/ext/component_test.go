// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"go.nlx.io/nlx/common/clock"
	discardlogger "go.nlx.io/nlx/common/logger/discard"
	"go.nlx.io/nlx/common/tls"
	postgresadapter "go.nlx.io/nlx/manager/adapters/storage/postgres"
	internalapp "go.nlx.io/nlx/manager/apps/int"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/internal/peers"
	restport "go.nlx.io/nlx/manager/ports/ext/rest"
	"go.nlx.io/nlx/manager/ports/ext/rest/api"
	externalservice "go.nlx.io/nlx/manager/services/ext"
	internalservice "go.nlx.io/nlx/manager/services/int"
	"go.nlx.io/nlx/testing/testingutils"
)

var (
	testClock      = clock.NewMock(time.Date(2023, 1, 1, 1, 42, 0, 0, time.Local))
	accessTokenTTL = time.Minute
	orgA           *organizationInfo
	orgB           *organizationInfo
	orgC           *organizationInfo
)

func TestMain(m *testing.M) {
	var err error

	orgA, err = newOrganizationInfo(testingutils.NLXTestOrgA, "https://manager.org-a.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	orgB, err = newOrganizationInfo(testingutils.NLXTestOrgB, "https://manager.org-b.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	orgC, err = newOrganizationInfo(testingutils.NLXTestOrgC, "https://manager.org-c.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	m.Run()
}

type organizationInfo struct {
	CertBundle     *tls.CertificateBundle
	ManagerAddress string
}

func newOrganizationInfo(organisationName testingutils.CertificateBundleOrganizationName, managerAddress string) (*organizationInfo, error) {
	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "../", "testing", "pki"), organisationName)
	if err != nil {
		return nil, err
	}

	return &organizationInfo{
		CertBundle:     orgCertBundle,
		ManagerAddress: managerAddress,
	}, nil
}

func (o *organizationInfo) GetName() string {
	return o.CertBundle.GetOrganizationInfo().Name
}

func (o *organizationInfo) GetPeerID() string {
	return o.CertBundle.GetOrganizationInfo().SerialNumber
}

//nolint:funlen,gocyclo // this is a test
func newService(testName string) (*httptest.Server, *internalapp.Application) {
	logger := discardlogger.New()

	orgACertBundle, err := testingutils.GetCertificateBundle(path.Join("..", "..", "../", "testing", "pki"), testingutils.NLXTestOrgA)
	if err != nil {
		log.Fatalf("failed to load organization a cert bundle: %v", err)
	}

	postgresDSN := os.Getenv("POSTGRES_DSN")

	if postgresDSN == "" {
		postgresDSN = "postgres://postgres@localhost:5432?sslmode=disable"
	}

	dbName := strings.ToLower(testName)

	testDB, err := testingutils.CreateTestDatabase(postgresDSN, dbName)
	if err != nil {
		log.Fatalf("failed to setup test database: %v", err)
	}

	db, err := postgresadapter.NewConnection(testDB)
	if err != nil {
		log.Fatalf("failed to create db connection: %v", err)
	}

	err = postgresadapter.PerformMigrations(testDB)
	if err != nil {
		log.Fatalf("failed to perform dbmigrations: %v", err)
	}

	postgresqlRepository, err := postgresadapter.New(orgACertBundle.RootCAs(), db)
	if err != nil {
		log.Fatalf("failed to setup postgresql database: %s", err)
	}

	controllerRepository := newFakeController()

	managerFactory := testManagerFactory{}

	peersAdapter, err := peers.New(&peers.NewPeersArgs{
		Ctx:                     context.Background(),
		Logger:                  logger,
		PeerCert:                orgACertBundle,
		Repository:              postgresqlRepository,
		SelfAddress:             orgA.ManagerAddress,
		DirectoryManagerAddress: "https://directory.nlx.local:443",
		ManagerFactory:          &managerFactory,
	})
	if err != nil {
		log.Fatalf("failed to setup peers adapter: %s", err)
	}

	txlogRepository := testTXLogAPI{}

	app, err := externalservice.NewApplication(&externalservice.NewApplicationArgs{
		Context:              context.Background(),
		Clock:                testClock,
		Logger:               logger,
		Repository:           postgresqlRepository,
		ControllerRepository: controllerRepository,
		TXLogRepository:      &txlogRepository,
		Peers:                peersAdapter,
		AutoSignGrants:       []string{},
		GroupID:              "fsc-local",
		SelfPeerID:           contract.PeerID(orgA.GetPeerID()),
		TrustedRootCAs:       orgACertBundle.RootCAs(),
		AutoSignCertificate:  orgACertBundle.Cert(),
		TokenSignCertificate: orgACertBundle.Cert(),
		TokenTTL:             accessTokenTTL,
	})
	if err != nil {
		log.Fatalf("failed to setup application: %s", err)
	}

	server, err := restport.New(&restport.NewArgs{
		Logger:      logger,
		App:         app,
		Cert:        orgACertBundle,
		SelfAddress: orgA.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to setup rest port: %s", err)
	}

	intApp, err := internalservice.NewApplication(&internalservice.NewApplicationArgs{
		Context:                     context.Background(),
		Clock:                       testClock,
		Logger:                      logger,
		Repository:                  postgresqlRepository,
		Peers:                       peersAdapter,
		TXLog:                       &txlogRepository,
		SelfPeerID:                  contract.PeerID(orgA.GetPeerID()),
		SelfPeerName:                orgA.GetName(),
		TrustedExternalRootCAs:      orgACertBundle.RootCAs(),
		SignatureCertificate:        orgACertBundle.Cert(),
		ControllerRepository:        controllerRepository,
		ManagerFactory:              &managerFactory,
		DirectoryPeerManagerAddress: "https://directory.nlx.local:443",
	})
	if err != nil {
		log.Fatalf("failed to setup internal app: %s", err)
	}

	srv := httptest.NewUnstartedServer(server.Handler())
	srv.TLS = orgACertBundle.TLSConfig(orgACertBundle.WithTLSClientAuth())
	srv.StartTLS()

	return srv, intApp
}

func createExternalManagerAPIClient(managerURL string, certBundle *tls.CertificateBundle) (*api.ClientWithResponses, error) {
	return api.NewClientWithResponses(managerURL, func(c *api.Client) error {
		t := &http.Transport{
			TLSClientConfig: certBundle.TLSConfig(certBundle.WithTLSClientAuth()),
		}
		c.Client = &http.Client{
			Transport: t,
		}
		return nil
	})
}
