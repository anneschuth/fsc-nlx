// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"path/filepath"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/manager/apps/int/command"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
	"go.nlx.io/nlx/testing/testingutils"
)

//nolint:funlen // this is a test
func TestRevokeContract(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name())
	defer externalHTTPServer.Close()

	orgACertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.NLXTestOrgA)
	assert.NoError(t, err)

	orgBCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.NLXTestOrgB)
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     time.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now(),
			NotAfter:  time.Now(),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             orgA.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
					CertificateThumbprint: orgACertBundle.CertificateThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             orgB.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     contractContent.Hash().Algorithm(),
		ID:                contractContent.ID().Bytes(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				CertificateThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().CertificateThumbprint().Value(),
				OutwayPeerID:          orgA.GetPeerID(),
				ServicePeerID:         orgB.GetPeerID(),
				ServiceName:           contractContent.Grants().ServiceConnectionGrants()[0].Service().Name(),
			},
		},
		CreatedAt: time.Now(),
	})
	assert.NoError(t, err)

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgBCertBundle)
	assert.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(orgBCertBundle.RootCAs(), orgBCertBundle.Cert(), time.Now())
	assert.NoError(t, err)

	grantData := models.FSCCoreGrant_Data{}
	_ = grantData.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
		Type: models.GRANTTYPESERVICECONNECTION,
		Outway: models.FSCCoreOutway{
			CertificateThumbprint: orgACertBundle.CertificateThumbprint(),
			PeerId:                orgA.GetPeerID(),
		},
		Service: models.FSCCoreService{
			Name:   "parkeerrechten",
			PeerId: orgB.GetPeerID(),
		},
	})

	requestContractContent := models.FSCCoreContractContent{
		CreatedAt: contractContent.CreatedAt().Unix(),
		Grants: []models.FSCCoreGrant{
			{
				Data: grantData,
			},
		},
		GroupId:       contractContent.GroupID(),
		HashAlgorithm: models.HASHALGORITHMSHA3512,
		Id:            contractContent.ID().String(),
		Validity: models.FSCCoreValidity{
			NotAfter:  contractContent.NotAfter().Unix(),
			NotBefore: contractContent.NotAfter().Unix(),
		},
	}

	// Note: the 'hash' argument is probably redundant. There's a proposal to adjust the RFC.
	// Hardcoded with the value 'hash' until this is resolved.
	// See https://gitlab.com/commonground/standards/fsc/-/issues/50
	acceptContractResp, err := client.AcceptContractWithResponse(context.Background(), "hash", &models.AcceptContractParams{
		FscManagerAddress: orgB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: requestContractContent,
		Signature:       acceptSignatureOrgB.JWS(),
	})

	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, acceptContractResp.StatusCode())

	// Act
	revokeSignatureOrgB, err := contractContent.Revoke(orgBCertBundle.RootCAs(), orgBCertBundle.Cert(), time.Now())
	assert.NoError(t, err)

	// Note: the 'hash' argument is probably redundant. There's a proposal to adjust the RFC.
	// Hardcoded with the value 'hash' until this is resolved.
	// See https://gitlab.com/commonground/standards/fsc/-/issues/50
	revokeContractResp, err := client.RevokeContractWithResponse(context.Background(), "hash", &models.RevokeContractParams{
		FscManagerAddress: orgB.ManagerAddress,
	}, models.RevokeContractJSONRequestBody{
		ContractContent: requestContractContent,
		Signature:       revokeSignatureOrgB.JWS(),
	})

	// Assert
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, revokeContractResp.StatusCode())

	contracts, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantHash: &[]string{
			contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
		},
	})
	assert.NoError(t, err)

	assert.Contains(t, contracts.JSON200.Contracts[0].Signatures.Accept, orgB.GetPeerID())
	assert.Contains(t, contracts.JSON200.Contracts[0].Signatures.Revoke, orgB.GetPeerID())
	assert.Equal(t, revokeSignatureOrgB.JWS(), contracts.JSON200.Contracts[0].Signatures.Revoke[orgB.GetPeerID()])
}
