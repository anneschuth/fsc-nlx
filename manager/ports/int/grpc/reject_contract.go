// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"

	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (s *Server) RejectContract(ctx context.Context, req *api.RejectContractRequest) (*api.RejectContractResponse, error) {
	s.logger.Info("rpc request RejectContract")

	err := s.app.Commands.RejectContract.Handle(ctx,
		req.ContentHash,
	)
	if err != nil {
		s.logger.Error("error executing reject contract command", err)
		return nil, ResponseFromError(err)
	}

	return &api.RejectContractResponse{}, nil
}
