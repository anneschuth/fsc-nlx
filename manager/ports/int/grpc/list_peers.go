/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package internalgrpc

import (
	"context"

	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (s *Server) ListPeers(ctx context.Context, _ *api.ListPeersRequest) (*api.ListPeersResponse, error) {
	s.logger.Info("rpc request list peers")

	res, err := s.app.Queries.ListPeers.Handle(ctx)
	if err != nil {
		s.logger.Warn("could not retrieve peers from directory, defaulting to empty list", err)
		return nil, ResponseFromError(err)
	}

	responsePeers := make([]*api.ListPeersResponse_Peer, 0, len(res))

	for _, peer := range res {
		responsePeers = append(responsePeers, &api.ListPeersResponse_Peer{
			Id:   peer.ID,
			Name: peer.Name,
		})
	}

	return &api.ListPeersResponse{
		Peers: responsePeers,
	}, nil
}
