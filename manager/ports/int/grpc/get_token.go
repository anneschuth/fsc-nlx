// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"

	"go.nlx.io/nlx/manager/apps/int/query"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (s *Server) GetToken(ctx context.Context, req *api.GetTokenRequest) (*api.GetTokenResponse, error) {
	s.logger.Info("rpc request GetTokenRequest")

	res, err := s.app.Queries.GetToken.Handle(ctx, &query.GetTokenHandlerArgs{
		GrantHash:                   req.GrantHash,
		OutwayCertificateThumbprint: req.OutwayCertificateThumbprint,
	})
	if err != nil {
		s.logger.Warn("could not get token from querier", err)
		return nil, ResponseFromError(err)
	}

	return &api.GetTokenResponse{Token: res.Token, TokenInfo: &api.GetTokenResponse_TokenInfo{
		GroupId:                     res.GroupID,
		GrantHash:                   res.GrantHash,
		OutwayPeerId:                res.OutwayPeerID,
		OutwayDelegatorPeerId:       res.OutwayDelegatorPeerID,
		OutwayCertificateThumbprint: res.OutwayCertificateThumbprint,
		ServiceName:                 res.ServiceName,
		ServiceInwayAddress:         res.ServiceInwayAddress,
		ServicePeerId:               res.ServicePeerID,
		ServiceDelegatorPeerId:      res.ServiceDelegatorPeerID,
		ExpiryDate:                  res.ExpiryDate.Unix(),
	}}, nil
}
