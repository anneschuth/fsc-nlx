// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"

	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (s *Server) RevokeContract(ctx context.Context, req *api.RevokeContractRequest) (*api.RevokeContractResponse, error) {
	s.logger.Info("rpc request RevokeContract")

	err := s.app.Commands.RevokeContract.Handle(ctx,
		req.ContentHash,
	)
	if err != nil {
		s.logger.Error("error executing revoke contract command", err)
		return nil, ResponseFromError(err)
	}

	return &api.RevokeContractResponse{}, nil
}
