// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"crypto/x509"
	"encoding/base64"
	"errors"

	"github.com/lestrrat-go/jwx/jwa"
	"github.com/lestrrat-go/jwx/jwk"

	"go.nlx.io/nlx/manager/ports/ext/rest/api"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

func (s *Server) GetJSONWebKeySet(ctx context.Context, _ api.GetJSONWebKeySetRequestObject) (api.GetJSONWebKeySetResponseObject, error) {
	certs, err := s.app.Queries.GetKeySet.Handle(ctx)
	if err != nil {
		s.logger.Error("error executing GetJSONWebKeySet query", err)
		return nil, err
	}

	keys := make([]models.FSCCoreJwk, 0)
	for thumbprint, c := range certs {

		cert, err := x509.ParseCertificates(c.RawDERs[0])
		if err != nil {
			s.logger.Fatal("failed to parse certs", err)
		}

		key, err := jwk.New(cert[0].PublicKey)
		if err != nil {
			s.logger.Fatal("failed to create ECDSA key", err)
		}

		alg, err := x509SignatureAlgorithmToJwkAlg(cert[0].SignatureAlgorithm)
		if err != nil {
			s.logger.Fatal("failed to retrieve alg", err)
		}

		x5c := rawDERstoBase64(c.RawDERs)

		err = key.Set(jwk.KeyIDKey, thumbprint)
		if err != nil {
			return nil, errors.New("cannot set kid on JWK")
		}

		err = key.Set(jwk.AlgorithmKey, alg)
		if err != nil {
			return nil, errors.New("cannot set alg on JWK")
		}

		err = key.Set(jwk.KeyUsageKey, "sig")
		if err != nil {
			return nil, errors.New("cannot set use on JWK")
		}

		err = key.Set(jwk.X509CertChainKey, x5c)
		if err != nil {
			return nil, errors.New("cannot set x5c on JWK")
		}

		err = key.Set(jwk.X509CertThumbprintS256Key, thumbprint)
		if err != nil {
			return nil, errors.New("cannot set x5t#s256 on JWK")
		}

		keyItem, err := convertKey(key, &x5c)
		if err != nil {
			return nil, errors.New("could not convert JWK to response format: " + err.Error())
		}

		keys = append(keys, keyItem)
	}

	jwks := models.FSCCoreJwks{
		Keys: keys,
	}

	response := api.GetJSONWebKeySet200JSONResponse(jwks)

	return response, nil
}

// Cannot use jwt.Key as response object in models.gen.go since jwt.Key is an interface, so transforming to struct
func convertKey(key jwk.Key, x5c *[]string) (models.FSCCoreJwk, error) {
	keyItem := models.FSCCoreJwk{}

	keyItem.Alg = ptr(models.FSCCoreJwkAlg(key.Algorithm()))
	keyItem.Kid = ptr(key.KeyID())
	keyItem.Use = ptr(models.FSCCoreJwkUse(key.KeyUsage()))
	keyItem.Kid = ptr(key.KeyID())
	keyItem.X5c = x5c
	keyItem.X5tS256 = ptr(key.X509CertThumbprintS256())
	keyItem.Kty = models.FSCCoreJwkKty(key.KeyType())

	err := convertPublicKey(&keyItem, key)
	if err != nil {
		return models.FSCCoreJwk{}, errors.New("could not convert public key: " + err.Error())
	}

	return keyItem, nil
}

func ptr[T any](value T) *T {
	return &value
}

func rawDERstoBase64(rawDERs [][]byte) []string {
	certs := make([]string, len(rawDERs))

	for i, c := range rawDERs {
		certs[i] = base64.StdEncoding.EncodeToString(c)
	}

	return certs
}

func convertPublicKey(responseKey *models.FSCCoreJwk, key jwk.Key) error {
	// Get key type specific values
	rawKeys, err := key.AsMap(context.Background())
	if err != nil {
		return errors.New("could not retrieve jwk values")
	}

	switch key.KeyType() {
	case jwa.RSA:
		err := convertRSAPublicKey(responseKey, rawKeys)
		if err != nil {
			return err
		}
	case jwa.EC:
		err := convertECPublicKey(responseKey, rawKeys)
		if err != nil {
			return err
		}
	default:
		return errors.New("unknown 'JWK.typ' only RSA and EC keys are supported")
	}

	return nil
}

func convertECPublicKey(responseKey *models.FSCCoreJwk, rawKeys map[string]interface{}) error {
	ecPublicKey := models.FSCCoreEcPublicKey{}

	if x, ok := rawKeys["x"]; ok {
		xBytes := x.([]byte)
		ecPublicKey.X = &xBytes
	}

	if y, ok := rawKeys["y"]; ok {
		yBytes := y.([]byte)
		ecPublicKey.Y = &yBytes
	}

	if crv, ok := rawKeys["crv"]; ok {
		crvString := crv.(string)
		ecPublicKey.Crv = &crvString
	}

	err := responseKey.FromFSCCoreEcPublicKey(ecPublicKey)
	if err != nil {
		return errors.New("could not create EC public key from jwk")
	}

	return nil
}

func convertRSAPublicKey(responseKey *models.FSCCoreJwk, rawKeys map[string]interface{}) error {
	rsaPublicKey := models.FSCCoreRsaPublicKey{}

	if n, ok := rawKeys["n"]; ok {
		nBytes := n.([]byte)
		rsaPublicKey.N = &nBytes
	}

	if e, ok := rawKeys["e"]; ok {
		eBytes := e.([]byte)
		rsaPublicKey.E = &eBytes
	}

	err := responseKey.FromFSCCoreRsaPublicKey(rsaPublicKey)
	if err != nil {
		return errors.New("could not create RSA public key from jwk")
	}

	return nil
}

func x509SignatureAlgorithmToJwkAlg(algorithm x509.SignatureAlgorithm) (string, error) {
	switch algorithm {
	case x509.SHA256WithRSA:
		return "RS256", nil
	case x509.SHA384WithRSA:
		return "RS384", nil
	case x509.SHA512WithRSA:
		return "RS512", nil
	case x509.ECDSAWithSHA256:
		return "ES256", nil
	case x509.ECDSAWithSHA384:
		return "ES384", nil
	case x509.ECDSAWithSHA512:
		return "ES512", nil
	case x509.SHA256WithRSAPSS:
		return "PS256", nil
	case x509.SHA384WithRSAPSS:
		return "PS384", nil
	case x509.SHA512WithRSAPSS:
		return "PS512", nil
	default:
		return "", errors.New("algorithm not supported in JWK")
	}
}
