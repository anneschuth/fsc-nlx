// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"

	"go.nlx.io/nlx/manager/apps/ext/query"
	"go.nlx.io/nlx/manager/ports/ext/rest/api"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
	"go.nlx.io/nlx/txlog-api/domain/record"
)

func (s *Server) GetLogs(ctx context.Context, request api.GetLogsRequestObject) (api.GetLogsResponseObject, error) {
	peer, err := getConnectingPeerFromCtx(ctx)
	if err != nil {
		return nil, err
	}

	args, err := mapRestToQueryListTXLogArgs(request)
	if err != nil {
		s.logger.Error("error parsing request", err)
		return nil, err
	}

	args.ConnectingPeerID = peer.id

	records, err := s.app.Queries.ListTransactionLogRecords.Handle(ctx, args)
	if err != nil {
		s.logger.Error("error executing list tx logs query", err)
		return nil, err
	}

	recs, err := mapQueryToRestLogs(records)
	if err != nil {
		s.logger.Error("error executing list tx logs query", err)
		return nil, err
	}

	cursor := determineNextCursorForTXLogRecords(records)

	return api.GetLogs200JSONResponse{
		Pagination: models.FSCLoggingPaginationResult{
			NextCursor: &cursor,
		},
		Records: recs,
	}, nil
}

func mapRestToQueryListTXLogArgs(request api.GetLogsRequestObject) (*query.NewListTransactionLogRecordsHandlerArgs, error) {
	sortOrder := query.SortOrderAscending

	if request.Params.SortOrder != nil {
		switch *request.Params.SortOrder {
		case models.FSCLoggingSortOrderSORTORDERASCENDING:
			sortOrder = query.SortOrderAscending
		case models.FSCLoggingSortOrderSORTORDERDESCENDING:
			sortOrder = query.SortOrderDescending
		}
	}

	var cursor string

	if request.Params.Cursor != nil {
		cursor = *request.Params.Cursor
	}

	filters := mapFilters(request)

	return &query.NewListTransactionLogRecordsHandlerArgs{
		Pagination: &query.Pagination{
			StartID:   cursor,
			Limit:     uint32(request.Params.Limit),
			SortOrder: sortOrder,
		},
		Filters: filters,
	}, nil
}

func mapFilters(request api.GetLogsRequestObject) []*query.Filter {
	filters := make([]*query.Filter, 0)

	period := &query.Period{}

	if request.Params.After != nil {
		period.Start = time.Unix(*request.Params.After, 0)
	}

	if request.Params.Before != nil {
		period.End = time.Unix(*request.Params.Before, 0)
	}

	filters = append(filters, &query.Filter{
		Period: period,
	})

	if request.Params.GrantHash != nil {
		for _, g := range *request.Params.GrantHash {
			filters = append(filters, &query.Filter{
				GrantHash: g,
			})
		}
	}

	if request.Params.ServiceName != nil {
		for _, s := range *request.Params.ServiceName {
			filters = append(filters, &query.Filter{
				ServiceName: s,
			})
		}
	}

	if request.Params.TransactionId != nil {
		for _, t := range *request.Params.TransactionId {
			filters = append(filters, &query.Filter{
				TransactionID: t.String(),
			})
		}
	}

	return filters
}

func mapQueryToRestLogs(records []*query.Record) ([]models.FSCLoggingLogRecord, error) {
	recs := make([]models.FSCLoggingLogRecord, len(records))

	for i, r := range records {
		data, err := mapSourceAndDirection(r.Source, r.Destination)
		if err != nil {
			return nil, err
		}

		tID, err := uuid.Parse(r.TransactionID)
		if err != nil {
			return nil, err
		}

		recs[i] = models.FSCLoggingLogRecord{
			TransactionId: tID,
			GrantHash:     r.GrantHash,
			ServiceName:   r.ServiceName,
			Direction:     mapDirection(r.Direction),
			Source:        data.Source,
			Destination:   data.Destination,
			CreatedAt:     r.CreatedAt.Unix(),
		}
	}

	return recs, nil
}

func mapSourceAndDirection(source, destination interface{}) (*models.FSCLoggingLogRecord, error) {
	data := &models.FSCLoggingLogRecord{}

	switch s := source.(type) {
	case *query.RecordSource:
		err := data.Source.FromFSCLoggingSource(models.FSCLoggingSource{
			Type:         models.SOURCETYPESOURCE,
			OutwayPeerId: s.OutwayPeerID,
		})
		if err != nil {
			return nil, err
		}

	case *query.RecordDelegatedSource:
		err := data.Source.FromFSCLoggingSourceDelegated(models.FSCLoggingSourceDelegated{
			Type:            models.SOURCETYPEDELEGATEDSOURCE,
			OutwayPeerId:    s.OutwayPeerID,
			DelegatorPeerId: s.DelegatorPeerID,
		})
		if err != nil {
			return nil, err
		}

	default:
		return nil, fmt.Errorf("unknown source type: %T", s)
	}

	switch d := destination.(type) {
	case *query.RecordDestination:
		err := data.Destination.FromFSCLoggingDestination(models.FSCLoggingDestination{
			Type:          models.DESTINATIONTYPEDESTINATION,
			ServicePeerId: d.ServicePeerID,
		})
		if err != nil {
			return nil, err
		}

	case *query.RecordDelegatedDestination:
		err := data.Destination.FromFSCLoggingDestinationDelegated(models.FSCLoggingDestinationDelegated{
			Type:            models.DESTINATIONTYPEDELEGATEDDESTINATION,
			ServicePeerId:   d.ServicePeerID,
			DelegatorPeerId: d.DelegatorPeerID,
		})
		if err != nil {
			return nil, err
		}

	default:
		return nil, fmt.Errorf("unknown destination type: %T", d)
	}

	return data, nil
}

func mapDirection(d record.Direction) models.FSCLoggingLogRecordDirection {
	switch d {
	case record.DirectionIn:
		return models.DIRECTIONINCOMING
	case record.DirectionOut:
		return models.DIRECTIONOUTGOING
	default:
		return models.DIRECTIONINCOMING
	}
}

func determineNextCursorForTXLogRecords(records []*query.Record) string {
	if len(records) == 0 {
		return ""
	}

	return records[len(records)-1].TransactionID
}
