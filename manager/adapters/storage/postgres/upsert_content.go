// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/adapters/storage/postgres/queries"
	"go.nlx.io/nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) UpsertContent(ctx context.Context, model *contract.Content) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	defer func() {
		err = tx.Rollback()
		if err != nil {
			if errors.Is(err, sql.ErrTxDone) {
				return
			}

			fmt.Printf("cannot rollback database transaction while creating record: %e", err)
		}
	}()

	qtx := r.queries.WithTx(tx)

	isNewContent, err := qtx.UpsertContent(ctx, &queries.UpsertContentParams{
		Hash:           model.Hash().String(),
		HashAlgorithm:  mapHashAlgorithm(model.Hash().Algorithm()),
		ID:             model.ID().Value(),
		GroupID:        model.GroupID(),
		ValidNotBefore: model.NotBefore(),
		ValidNotAfter:  model.NotAfter(),
		CreatedAt:      model.CreatedAt(),
	})
	if err != nil {
		if err != sql.ErrNoRows {
			return errors.Wrap(err, "failed to upsert content")
		}
	}

	if isNewContent {
		for _, grant := range model.Grants() {
			err = createGrant(ctx, model.Hash().String(), grant, qtx)
			if err != nil {
				return err
			}
		}
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

//nolint:gocyclo // difficult to improve complexity without effecting the readability
func createGrant(ctx context.Context, hash string, grant interface{}, qtx *queries.Queries) error {
	switch g := grant.(type) {
	case *contract.GrantPeerRegistration:
		err := qtx.CreatePeerRegistrationGrant(ctx, &queries.CreatePeerRegistrationGrantParams{
			ContentHash:     hash,
			Hash:            g.Hash().String(),
			DirectoryPeerID: g.Directory().PeerID().Value(),
			PeerID:          g.Peer().ID().Value(),
			PeerName:        g.Peer().Name(),
		})
		if err != nil {
			return fmt.Errorf("failed to create peer registration grant: %v", err)
		}
	case *contract.GrantServicePublication:
		err := qtx.CreatePublicationGrant(ctx, &queries.CreatePublicationGrantParams{
			ContentHash:     hash,
			Hash:            g.Hash().String(),
			ServiceName:     g.Service().Name(),
			ServicePeerID:   g.Service().Peer().ID().Value(),
			DirectoryPeerID: g.Directory().Peer().ID().Value(),
		})
		if err != nil {
			return fmt.Errorf("failed to create publication grant: %v", err)
		}
	case *contract.GrantServiceConnection:
		err := qtx.CreateConnectionGrant(ctx, &queries.CreateConnectionGrantParams{
			ContentHash:           hash,
			Hash:                  g.Hash().String(),
			ServiceName:           g.Service().Name(),
			ConsumerPeerID:        g.Outway().Peer().ID().Value(),
			ServicePeerID:         g.Service().Peer().ID().Value(),
			CertificateThumbprint: g.Outway().CertificateThumbprint().Value(),
		})
		if err != nil {
			return fmt.Errorf("failed to create connection grant: %v", err)
		}

	case *contract.GrantDelegatedServiceConnection:
		err := qtx.CreateDelegatedConnectionGrant(ctx, &queries.CreateDelegatedConnectionGrantParams{
			ContentHash:           hash,
			Hash:                  g.Hash().String(),
			ServiceName:           g.Service().Name(),
			OutwayPeerID:          g.Outway().Peer().ID().Value(),
			ServicePeerID:         g.Service().Peer().ID().Value(),
			DelegatorPeerID:       g.Delegator().Peer().ID().Value(),
			CertificateThumbprint: g.Outway().CertificateThumbprint().Value(),
		})
		if err != nil {
			return fmt.Errorf("failed to create delegated connection grant: %v", err)
		}

	case *contract.GrantDelegatedServicePublication:
		err := qtx.CreateDelegatedPublicationGrant(ctx, &queries.CreateDelegatedPublicationGrantParams{
			ContentHash:     hash,
			Hash:            g.Hash().String(),
			ServiceName:     g.Service().Name(),
			DirectoryPeerID: g.Directory().Peer().ID().Value(),
			ServicePeerID:   g.Service().Peer().ID().Value(),
			DelegatorPeerID: g.Delegator().Peer().ID().Value(),
		})
		if err != nil {
			return fmt.Errorf("failed to create delegated publication grant: %v", err)
		}
	}

	return nil
}

func mapHashAlgorithm(c contract.HashAlg) queries.ContractsContentHashAlgorithm {
	switch c {
	case contract.HashAlgSHA3_512:
		return queries.ContractsContentHashAlgorithmSha3512
	default:
		return ""
	}
}
