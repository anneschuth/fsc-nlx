-- Copyright © VNG Realisatie 2022
-- Licensed under the EUPL

-- name: HasValidContractWithPeerRegistrationGrant :one
SELECT
    count(*) > 0 as has_valid_contract
FROM contracts.content as c
         INNER JOIN contracts.grants_peer_registration as gpr
                    ON gpr.content_hash = c.hash
         JOIN contracts.valid_contracts
              ON contracts.valid_contracts.hash = c.hash
WHERE
        gpr.peer_id = $1;
