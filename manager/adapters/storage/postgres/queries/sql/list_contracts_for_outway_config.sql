-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListContractsForOutwayConfig :many
SELECT
    c.id as content_id,
    c.hash as content_hash,
    c.hash_algorithm as content_hash_algorithm,
    c.group_id as content_group_id,
    c.valid_not_before as content_valid_not_before,
    c.valid_not_after as content_valid_not_after,
    c.created_at as content_created_at
FROM contracts.content as c
    LEFT JOIN contracts.grants_service_connection as gc
        ON gc.content_hash = c.hash
    LEFT JOIN contracts.grants_delegated_service_connection as gdsc
         ON gdsc.content_hash = c.hash
    JOIN contracts.valid_contracts
        ON contracts.valid_contracts.hash = c.hash
WHERE
    gc.certificate_thumbprint = sqlc.arg(certificate_thumbprint)::text OR gdsc.certificate_thumbprint = sqlc.arg(certificate_thumbprint)::text
;
