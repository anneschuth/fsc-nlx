// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) HasValidContractWithPeerRegistrationGrant(ctx context.Context, selfPeer contract.PeerID) (bool, error) {
	hasValidContract, err := r.queries.HasValidContractWithPeerRegistrationGrant(ctx, selfPeer.Value())
	if err != nil {
		return false, errors.Wrap(err, "could not list valid contracts with peer registration grant from database")
	}

	return hasValidContract, nil
}
