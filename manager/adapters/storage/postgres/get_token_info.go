// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/adapters/storage/postgres/queries"
	"go.nlx.io/nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) GetTokenInfo(ctx context.Context, outwayPeer, selfPeer contract.PeerID, grantHash *contract.Hash) (*contract.TokenInfo, error) {
	switch grantHash.Type() {
	case contract.HashTypeGrantServiceConnection:
		infoRow, err := r.queries.GetTokenInfoServiceConnection(ctx, &queries.GetTokenInfoServiceConnectionParams{
			Hash:         grantHash.String(),
			OutwayPeerID: outwayPeer.Value(),
			SelfPeerID:   selfPeer.Value(),
		})
		if err != nil {
			return nil, errors.Wrap(err, "could not get service connection info from database")
		}

		return &contract.TokenInfo{
			GroupID:                infoRow.GroupID,
			ServiceName:            infoRow.ServiceName,
			ServiceDelegatorPeerID: infoRow.ServiceDelegatorPeerID.String,
			CertificateThumbprint:  infoRow.CertificateThumbprint,
		}, nil
	case contract.HashTypeGrantDelegatedServiceConnection:
		infoRow, err := r.queries.GetTokenInfoDelegatedServiceConnection(ctx, &queries.GetTokenInfoDelegatedServiceConnectionParams{
			Hash:         grantHash.String(),
			OutwayPeerID: outwayPeer.Value(),
			SelfPeerID:   selfPeer.Value(),
		})
		if err != nil {
			return nil, errors.Wrap(err, "could not get delegated service connection info from database")
		}

		return &contract.TokenInfo{
			GroupID:                infoRow.GroupID,
			ServiceName:            infoRow.ServiceName,
			OutwayDelegatorPeerID:  infoRow.OutwayDelegatorPeerID,
			ServiceDelegatorPeerID: infoRow.ServiceDelegatorPeerID.String,
			CertificateThumbprint:  infoRow.CertificateThumbprint,
		}, nil
	default:
		return nil, fmt.Errorf("invalid grant hash type, only service connection and delegated service connection types are allowed")
	}
}
