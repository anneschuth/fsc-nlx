// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package contract

import (
	"fmt"
	"time"
)

type NewContractArgs struct {
	Content            *NewContentArgs
	PeersCerts         PeersCertificates
	SignaturesAccepted map[string]string
	SignaturesRejected map[string]string
	SignaturesRevoked  map[string]string
}

type Contract struct {
	content            *Content
	signaturesAccepted signaturesAccepted
	signaturesRejected signaturesRejected
	signaturesRevoked  signaturesRevoked
}

func NewContract(args *NewContractArgs) (*Contract, error) {
	if args == nil {
		return nil, fmt.Errorf("new contract args cannot be nil")
	}

	content, err := NewContent(args.Content)
	if err != nil {
		return nil, err
	}

	signaturesAcc, err := newSignaturesAccepted(&newSignaturesArgs{
		Sigs:       mapSignatureArgs(args.SignaturesAccepted),
		Content:    content,
		PeersCerts: args.PeersCerts,
	})
	if err != nil {
		return nil, err
	}

	signaturesRej, err := newSignaturesRejected(&newSignaturesArgs{
		Sigs:       mapSignatureArgs(args.SignaturesRejected),
		Content:    content,
		PeersCerts: args.PeersCerts,
	})
	if err != nil {
		return nil, err
	}

	signaturesRev, err := newSignaturesRevoked(&newSignaturesArgs{
		Sigs:       mapSignatureArgs(args.SignaturesRevoked),
		Content:    content,
		PeersCerts: args.PeersCerts,
	})
	if err != nil {
		return nil, err
	}

	return &Contract{
		content:            content,
		signaturesAccepted: signaturesAcc,
		signaturesRejected: signaturesRej,
		signaturesRevoked:  signaturesRev,
	}, nil
}

func (c *Contract) SignaturesAccepted() signaturesAccepted {
	return c.signaturesAccepted
}

func (c *Contract) SignaturesRejected() signaturesRejected {
	return c.signaturesRejected
}

func (c *Contract) SignaturesRevoked() signaturesRevoked {
	return c.signaturesRevoked
}

func (c *Contract) IsAcceptedBy(p PeerID) bool {
	return c.signaturesAccepted.contains(p)
}

func (c *Contract) IsRejectedBy(p PeerID) bool {
	return c.signaturesRejected.contains(p)
}

func (c *Contract) IsRevokedBy(p PeerID) bool {
	return c.signaturesRevoked.contains(p)
}

func (c *Contract) Content() *Content {
	return c.content
}

// Validate checks if contract is valid, returns an error when contract is not valid
// e.g. checks for valid signatures and expiry dates
func (c *Contract) Validate(now time.Time) error {
	if c.content.NotBefore().After(now) {
		return fmt.Errorf("contract validity is not started yet, starts at %s", c.content.NotBefore())
	}

	if c.content.NotAfter().Before(now) {
		return fmt.Errorf("contract validity is expired, expired at: %s", c.content.NotAfter())
	}

	sigsAccepted := c.SignaturesAccepted()
	sigsRejected := c.SignaturesRejected()
	sigsRevoked := c.SignaturesRevoked()

	for p := range c.Content().PeersIDs() {
		if !sigsAccepted.contains(p) {
			return fmt.Errorf("contract is not accepted by peer: %s", p)
		}

		if sigsRejected.contains(p) {
			return fmt.Errorf("contract is rejected by peer: %s", p)
		}

		if sigsRevoked.contains(p) {
			return fmt.Errorf("contract is revoked by peer: %s", p)
		}
	}

	return nil
}

func duplicateInList(list []string) bool {
	visited := make(map[string]bool, 0)

	for _, s := range list {
		if visited[s] {
			return true
		} else {
			visited[s] = true
		}
	}

	return false
}

func mapSignatureArgs(input map[string]string) map[string]*newSignatureArgs {
	result := make(map[string]*newSignatureArgs)

	if input == nil {
		return result
	}

	for peerID, signature := range input {
		result[peerID] = &newSignatureArgs{
			JWS: signature,
		}
	}

	return result
}
