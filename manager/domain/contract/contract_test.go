// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract_test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/manager/domain/contract"
)

func TestContract(t *testing.T) {
	contractID, err := uuid.Parse("26aad1d8-3653-4e6f-b3ba-70df67a9a0ab")
	assert.NoError(t, err)

	contractIDBytes, err := contractID.MarshalBinary()
	assert.NoError(t, err)

	now := time.Date(2023, 1, 1, 1, 42, 0, 0, time.UTC)

	c, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractIDBytes,
		CreatedAt:     now,
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: now,
			NotAfter:  now,
		},
		Grants: []interface{}{
			&contract.NewGrantPeerRegistrationArgs{
				Directory: &contract.NewGrantPeerRegistrationDirectoryArgs{
					PeerID: "00000000000000000002",
				},
				Peer: &contract.NewGrantPeerRegistrationPeerArgs{
					ID:   "00000000000000000001",
					Name: "test",
				},
			},
		},
	})
	assert.NoError(t, err)

	assert.Equal(t, "$1$1$ASbh_V9DmyQRhpfyRYLTCMGpU0KFF97GnSd82ZXk5PxPEOdPRpcFDpLVC36JM5VjeD7etN3nODwa4rx9vJj7nw", c.Hash().String())
	assert.Equal(t, "$1$2$k7rzGGjZTcjO5fgL56eKZ-KpQifdWo5kd_yaOv4MJxq6J9NvuEjCUwwKTZLZ4no9AccO2WX_gTtVxC3AJa8kFg", c.Grants().PeerRegistrationGrant().Hash().String())
}
