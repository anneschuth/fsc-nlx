// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"fmt"
	"regexp"
	"time"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

var (
	alphaNumericRegex = regexp.MustCompile(`^[a-zA-Z0-9-._]{1,100}$`)
	groupIDRegex      = regexp.MustCompile(`^[a-zA-Z0-9./_-]{1,100}$`)
)

type contentID uuid.UUID

type NewContentArgs struct {
	Validity      *NewValidityArgs
	GroupID       string
	Grants        []interface{}
	HashAlgorithm HashAlg
	ID            []byte
	CreatedAt     time.Time
}

type NewValidityArgs struct {
	NotBefore time.Time
	NotAfter  time.Time
}

type GroupID string

type Content struct {
	hash      *ContentHash
	validity  *validity
	groupID   GroupID
	grants    grants
	id        contentID
	createdAt time.Time
}

type validity struct {
	notBefore time.Time
	notAfter  time.Time
}

func NewContent(args *NewContentArgs) (*Content, error) {
	if args == nil {
		return nil, fmt.Errorf("new content args cannot be nil")
	}

	gID, err := NewGroupID(args.GroupID)
	if err != nil {
		return nil, err
	}

	cID, err := newContentID(args.ID)
	if err != nil {
		return nil, err
	}

	modelGrants, err := newGrants(gID, cID, args.HashAlgorithm, args.Grants)
	if err != nil {
		return nil, err
	}

	validity, err := newValidity(args.Validity)
	if err != nil {
		return nil, err
	}

	if args.CreatedAt.IsZero() {
		return nil, fmt.Errorf("createdAt is required")
	}

	if args.CreatedAt.After(time.Now()) {
		return nil, fmt.Errorf("createdAt cannot be in the future")
	}

	hash, err := newContentHash(&Content{
		id:        cID,
		groupID:   gID,
		validity:  validity,
		grants:    modelGrants,
		createdAt: args.CreatedAt,
	}, args.HashAlgorithm)
	if err != nil {
		return nil, err
	}

	return &Content{
		hash:      hash,
		id:        cID,
		groupID:   gID,
		validity:  validity,
		grants:    modelGrants,
		createdAt: args.CreatedAt,
	}, nil
}

func newValidity(args *NewValidityArgs) (*validity, error) {
	if args == nil {
		return nil, fmt.Errorf("new validity args cannot be nil")
	}

	if args.NotBefore.IsZero() {
		return nil, fmt.Errorf("NotBefore cannot be zero")
	}

	if args.NotAfter.IsZero() {
		return nil, fmt.Errorf("NotAfter cannot be zero")
	}

	if args.NotBefore.After(args.NotAfter) {
		return nil, fmt.Errorf("NotBefore cannot be after NotAfter")
	}

	return &validity{
		notBefore: args.NotBefore,
		notAfter:  args.NotAfter,
	}, nil
}

func newContentID(bytes []byte) (contentID, error) {
	id, err := uuid.FromBytes(bytes)
	if err != nil {
		return contentID{}, errors.Wrap(err, "could not create content ID")
	}

	return contentID(id), nil
}

func NewGroupID(g string) (GroupID, error) {
	if g == "" {
		return GroupID(""), fmt.Errorf("group ID cannot be empty")
	}

	if !groupIDRegex.MatchString(g) {
		return GroupID(""), fmt.Errorf("not a valid group ID")
	}

	return GroupID(g), nil
}

func (g GroupID) String() string {
	return string(g)
}

func (g GroupID) IsEqual(s string) bool {
	return g == GroupID(s)
}

func (c *Content) Hash() *ContentHash {
	return c.hash
}

func (c *Content) NotBefore() time.Time {
	return c.validity.notBefore
}

func (c *Content) NotAfter() time.Time {
	return c.validity.notAfter
}

func (c *Content) Grants() grants {
	return c.grants
}

func (c *Content) ID() contentID {
	return c.id
}

func (c *Content) GroupID() string {
	return string(c.groupID)
}

func (c *Content) CreatedAt() time.Time {
	return c.createdAt
}

// Peers returns all unique peers found in content
func (c *Content) PeersIDs() PeersIDs {
	peers := make(PeersIDs, 0)

	for _, grant := range c.Grants() {
		switch g := grant.(type) {
		case *GrantPeerRegistration:
			peers[g.directory.peerID] = true
			peers[g.peer.id] = true
		case *GrantServicePublication:
			peers[g.directory.peer.id] = true
			peers[g.service.peer.id] = true
		case *GrantServiceConnection:
			peers[g.outway.peer.id] = true
			peers[g.service.peer.id] = true
		case *GrantDelegatedServiceConnection:
			peers[g.outway.peer.id] = true
			peers[g.service.peer.id] = true
			peers[g.delegator.peer.id] = true
		case *GrantDelegatedServicePublication:
			peers[g.directory.peer.id] = true
			peers[g.service.peer.id] = true
			peers[g.delegator.peer.id] = true
		}
	}

	return peers
}

func (c *Content) ContainsPeer(id PeerID) bool {
	_, ok := c.PeersIDs()[id]

	return ok
}

func (id contentID) Bytes() []byte {
	return id[:]
}

func (id contentID) Value() uuid.UUID {
	return uuid.UUID(id)
}

func (id contentID) String() string {
	uid := uuid.UUID(id)
	return uid.String()
}
