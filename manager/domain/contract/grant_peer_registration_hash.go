// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"bytes"
	"sort"

	"github.com/pkg/errors"
)

type grantPeerRegistrationHash Hash

func newGrantPeerRegistrationHash(gID GroupID, cID contentID, alg HashAlg, gc *GrantPeerRegistration) (*grantPeerRegistrationHash, error) {
	h, err := newHash(alg, HashTypeGrantPeerRegistration, getSortedGrantPeerRegistrationBytes(gID, cID, gc))
	if err != nil {
		return nil, errors.Wrap(err, "could not create grant peer registration hash")
	}

	return (*grantPeerRegistrationHash)(h), nil
}

func getSortedGrantPeerRegistrationBytes(gID GroupID, cID contentID, gc *GrantPeerRegistration) []byte {
	byteArrays := make([][]byte, 0)

	byteArrays = append(byteArrays,
		[]byte(gID),
		cID.Bytes(),
		[]byte(gc.directory.peerID),
		[]byte(gc.peer.id),
		[]byte(gc.peer.name),
	)

	sort.Slice(byteArrays, func(i, j int) bool {
		return bytes.Compare(byteArrays[i], byteArrays[j]) < 0
	})

	bytesToHash := make([]byte, 0)
	for _, byteArray := range byteArrays {
		bytesToHash = append(bytesToHash, byteArray...)
	}

	return bytesToHash
}

func (h grantPeerRegistrationHash) String() string {
	return Hash(h).String()
}
