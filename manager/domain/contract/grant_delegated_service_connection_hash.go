// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"bytes"
	"sort"

	"github.com/pkg/errors"
)

type grantDelegatedServiceConnectionHash Hash

func newGrantDelegatedServiceConnectionHash(gID GroupID, cID contentID, alg HashAlg, gc *GrantDelegatedServiceConnection) (*grantDelegatedServiceConnectionHash, error) {
	h, err := newHash(alg, HashTypeGrantDelegatedServiceConnection, getSortedGrantDelegatedConnectionBytes(gID, cID, gc))
	if err != nil {
		return nil, errors.Wrap(err, "could not create grant delegated service connection hash")
	}

	return (*grantDelegatedServiceConnectionHash)(h), nil
}

func getSortedGrantDelegatedConnectionBytes(gID GroupID, cID contentID, gc *GrantDelegatedServiceConnection) []byte {
	byteArrays := make([][]byte, 0)

	byteArrays = append(byteArrays,
		[]byte(gID),
		cID.Bytes(),
		[]byte(gc.outway.peer.id),
		[]byte(gc.delegator.peer.id),
		[]byte(gc.service.peer.id),
		[]byte(gc.service.name),
	)

	byteArrays = append(byteArrays, []byte(gc.outway.certificateThumbprint.Value()))

	sort.Slice(byteArrays, func(i, j int) bool {
		return bytes.Compare(byteArrays[i], byteArrays[j]) < 0
	})

	bytesToHash := make([]byte, 0)
	for _, byteArray := range byteArrays {
		bytesToHash = append(bytesToHash, byteArray...)
	}

	return bytesToHash
}

func (h grantDelegatedServiceConnectionHash) String() string {
	return Hash(h).String()
}
