// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import "fmt"

type NewGrantServiceConnectionArgs struct {
	Outway  *NewGrantServiceConnectionOutwayArgs
	Service *NewGrantServiceConnectionServiceArgs
}

type NewGrantServiceConnectionOutwayArgs struct {
	Peer                  *NewPeerArgs
	CertificateThumbprint string
}

type NewGrantServiceConnectionServiceArgs struct {
	Peer *NewPeerArgs
	Name string
}

type GrantServiceConnection struct {
	hash    *grantServiceConnectionHash
	outway  *grantServiceConnectionOutway
	service *grantServiceConnectionService
}

type grantServiceConnectionOutway struct {
	peer                  *Peer
	certificateThumbprint CertificateThumbprint
}

type grantServiceConnectionService struct {
	peer *Peer
	name serviceName
}

func (g *GrantServiceConnection) Outway() *grantServiceConnectionOutway {
	return g.outway
}

func (g *GrantServiceConnection) Hash() *grantServiceConnectionHash {
	return g.hash
}

func (g *GrantServiceConnection) Service() *grantServiceConnectionService {
	return g.service
}

func (s *grantServiceConnectionService) Peer() *Peer {
	return s.peer
}

func (s *grantServiceConnectionService) Name() string {
	return string(s.name)
}

func (c *grantServiceConnectionOutway) Peer() *Peer {
	return c.peer
}

func (c *grantServiceConnectionOutway) CertificateThumbprint() CertificateThumbprint {
	return c.certificateThumbprint
}

// nolint:dupl // grants are definitively similar but still have distinctive properties
func newGrantServiceConnection(gID GroupID, cID contentID, alg HashAlg, args *NewGrantServiceConnectionArgs) (*GrantServiceConnection, error) {
	if args == nil {
		return nil, fmt.Errorf("new service connection args cannot be nil")
	}

	outway, err := newGrantServiceConnectionOutway(args.Outway)
	if err != nil {
		return nil, err
	}

	service, err := newGrantServiceConnectionService(args.Service)
	if err != nil {
		return nil, err
	}

	h, err := newGrantServiceConnectionHash(gID, cID, alg, &GrantServiceConnection{
		outway:  outway,
		service: service,
	})
	if err != nil {
		return nil, err
	}

	return &GrantServiceConnection{
		hash:    h,
		outway:  outway,
		service: service,
	}, nil
}

func newGrantServiceConnectionService(args *NewGrantServiceConnectionServiceArgs) (*grantServiceConnectionService, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant service connection service args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	name, err := newServiceName(args.Name)
	if err != nil {
		return nil, err
	}

	return &grantServiceConnectionService{
		peer: p,
		name: name,
	}, nil
}

func newGrantServiceConnectionOutway(args *NewGrantServiceConnectionOutwayArgs) (*grantServiceConnectionOutway, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant service connection outway args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	thumbprint, err := NewCertificateThumbprint(args.CertificateThumbprint)
	if err != nil {
		return nil, err
	}

	return &grantServiceConnectionOutway{
		peer:                  p,
		certificateThumbprint: thumbprint,
	}, nil
}
