// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package contract

import (
	"context"
	"time"
)

type Repository interface {
	UpsertContent(ctx context.Context, c *Content) error
	UpsertSignature(ctx context.Context, s *Signature) error
	UpsertCertificate(ctx context.Context, certificate *PeerCertificate) error
	GetContentByHash(ctx context.Context, hash *ContentHash) (*Content, error)
	GetPeersCertsByCertThumbprints(ctx context.Context, thumbprints CertificateThumbprints) (*PeersCertificates, error)
	GetPeerCertsByCertThumbprints(ctx context.Context, id PeerID, thumbprints CertificateThumbprints) (*PeerCertificates, error)
	GetPeerCerts(ctx context.Context, id PeerID) (*PeerCertificates, error)
	ListContractsForOutwayConfig(ctx context.Context, certificateThumbprint string) ([]*Contract, error)
	ListContractsForInwayConfig(ctx context.Context, services []string) ([]*Contract, error)
	ListContractsForPeerByGrantHashes(ctx context.Context, peerID PeerID, grantHashes []string) ([]*Contract, error)
	ListContractsForPeer(ctx context.Context, peerID PeerID, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListContractsForPeerWithServiceConnectionGrant(ctx context.Context, peerID PeerID, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListContractsForPeerWithServicePublicationGrant(ctx context.Context, peerID PeerID, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListContractsForPeerWithPeerRegistrationGrant(ctx context.Context, peerID PeerID, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListContractsForPeerWithDelegatedServiceConnectionGrant(ctx context.Context, peerID PeerID, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListContractsForPeerWithDelegatedServicePublicationGrant(ctx context.Context, peerID PeerID, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListContractsWithPeerRegistrationGrant(ctx context.Context, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListContractsWithServiceConnectionGrant(ctx context.Context, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListContractsWithDelegatedServiceConnectionGrant(ctx context.Context, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListContractsWithServicePublicationGrant(ctx context.Context, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListContractsWithDelegatedServicePublicationGrant(ctx context.Context, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListPeers(ctx context.Context, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Peer, error)
	ListPeersByID(ctx context.Context, peerIDs PeersIDs) (Peers, error)
	ListPeersByName(ctx context.Context, peerName string, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Peer, error)
	ListServices(ctx context.Context, peertID *PeerID, serviceName string, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Service, error)
	UpsertPeer(ctx context.Context, p *Peer) error
	ListServicesDetails(ctx context.Context, self PeerID, services []string) (map[string]*ServiceDetails, error)
	GetTokenInfo(ctx context.Context, outwayPeer PeerID, selfPeer PeerID, grantHash *Hash) (*TokenInfo, error)
	GetServicesForOutway(ctx context.Context, outwayCertThumbprint string) (OutwayServices, error)
	GetTokenProviderPeer(ctx context.Context, grantHash *Hash) (PeerID, error)
	ListAllContracts(ctx context.Context) ([]*Contract, error)
	ListContractsByContentHash(ctx context.Context, contentHashes []string) ([]*Contract, error)
	HasValidContractWithPeerRegistrationGrant(ctx context.Context, selfPeer PeerID) (bool, error)
	Close() error
}

type Service struct {
	ContractID         string
	PeerID             string
	PeerManagerAddress string
	PeerName           string
	DelegatorPeerID    string
	DelegatorPeerName  string
	Name               string
}

type ServiceDetails struct {
	Provider interface{}
}

type Provider struct {
	PeerID string
}

type DelegatedProvider struct {
	PeerID          string
	DelegatorPeerID string
}

type TokenInfo struct {
	GroupID                string
	CertificateThumbprint  string
	ServiceName            string
	ServiceDelegatorPeerID string
	OutwayDelegatorPeerID  string
}

type OutwayServices map[string]*OutwayService

type OutwayService struct {
	PeerID string
	Name   string
}

type TXLogRecords []*TXLogRecord

type TXLogRecord struct {
	TransactionID string
	GrantHash     string
	ServiceName   string
	Direction     TXLogDirection
	Source        interface{}
	Destination   interface{}
	CreatedAt     time.Time
}

type TXLogDirection int32

const (
	TXLogDirectionUnspecified TXLogDirection = iota
	TXLogDirectionIn
	TXLogDirectionOut
)

type TXLogRecordSource struct {
	OutwayPeerID string
}

type TXLogRecordDelegatedSource struct {
	OutwayPeerID    string
	DelegatorPeerID string
}

type TXLogRecordDestination struct {
	ServicePeerID string
}

type TXLogRecordDelegatedDestination struct {
	ServicePeerID   string
	DelegatorPeerID string
}

type SortOrder string

const (
	SortOrderAscending  SortOrder = "asc"
	SortOrderDescending SortOrder = "desc"
)
