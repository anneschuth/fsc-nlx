// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"bytes"
	"crypto"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"

	"github.com/pkg/errors"

	common_tls "go.nlx.io/nlx/common/tls"
)

// map[string] = peer id
type NewPeersCertificatesArgs map[string]RawDERPeerCertificates

// [][]byte = Certificate chains
type RawDERPeerCertificates [][]byte

// [][]byte = Certificate chains
type RawPEMPeerCertificates [][]byte

type CertificateThumbprints []CertificateThumbprint

const certificateThumbprintLength = 32

type CertificateThumbprint string

type PeerCertificate struct {
	thumbprint        CertificateThumbprint
	cert              *x509.Certificate
	intermediateCerts *x509.CertPool
	peer              *Peer
	raw               []byte
	rawDERs           [][]byte
	keyType           CertKeyType
}

type PeerCertificates map[CertificateThumbprint]*PeerCertificate

type PeersCertificates map[PeerID]*PeerCertificates

type CertKeyType int

const (
	CertKeyTypeUnspecified CertKeyType = iota
	CertKeyTypeEC
	CertKeyTypeRSA
)

const (
	maxAmountOfCertThumbprints = 100
)

func NewPeersCertificates(trustedRootCAs *x509.CertPool, keys NewPeersCertificatesArgs) (PeersCertificates, error) {
	peersCerts := make(PeersCertificates, 0)

	for id, peerCerts := range keys {
		peerID, err := NewPeerID(id)
		if err != nil {
			return nil, errors.Wrap(err, "could not create peers certificates, invalid peer id")
		}

		pc, err := NewPeerCertificates(trustedRootCAs, peerCerts)
		if err != nil {
			return nil, errors.Wrap(err, "could not create peers certificates, invalid peer certs")
		}

		peersCerts[peerID] = pc
	}

	return peersCerts, nil
}

func NewPeerCertificatesFromPEM(trustedRootCAs *x509.CertPool, peerCerts RawPEMPeerCertificates) (*PeerCertificates, error) {
	derCerts := make(RawDERPeerCertificates, len(peerCerts))

	for i, cert := range peerCerts {
		derCert := make([]byte, 0)

		certPEMBlock := cert

		for {
			var certDERBlock *pem.Block

			certDERBlock, certPEMBlock = pem.Decode(certPEMBlock)
			if certDERBlock == nil {
				break
			}

			if certDERBlock.Type == "CERTIFICATE" {
				derCert = append(derCert, certDERBlock.Bytes...)
			}
		}

		derCerts[i] = derCert
	}

	certs, err := NewPeerCertificates(trustedRootCAs, derCerts)
	if err != nil {
		return nil, errors.Wrap(err, "could not parse peer certificates from PEM")
	}

	return certs, nil
}

func NewPeerCertificates(trustedRootCAs *x509.CertPool, peerCerts RawDERPeerCertificates) (*PeerCertificates, error) {
	certs := make(PeerCertificates, 0)

	for _, cert := range peerCerts {
		peerCert, err := newPeerCert(trustedRootCAs, cert)
		if err != nil {
			return nil, errors.Wrap(err, "could not create peer certificates")
		}

		certs[peerCert.CertificateThumbprint()] = peerCert
	}

	return &certs, nil
}

func NewPeerCertFromCertificate(trustedRootCAs *x509.CertPool, cert [][]byte) (*PeerCertificate, error) {
	var buf bytes.Buffer

	for _, c := range cert {
		_, err := buf.Write(c)
		if err != nil {
			return nil, errors.Wrap(err, "could not combine certificate chain")
		}
	}

	return newPeerCert(trustedRootCAs, buf.Bytes())
}

func newPeerCert(trustedRootCAs *x509.CertPool, cert []byte) (*PeerCertificate, error) {
	if trustedRootCAs == nil {
		return nil, fmt.Errorf("could not create peer certificate, missing trusted root CAs")
	}

	certs, err := x509.ParseCertificates(cert)
	if err != nil {
		return nil, errors.Wrap(err, "could not create peer certificate, invalid certificate bytes")
	}

	if len(certs) == 0 {
		return nil, fmt.Errorf("no certificates returned when parsing certificate bytes")
	}

	peerCert := certs[0]

	rawDERs := make([][]byte, 0)
	rawDERs = append(rawDERs, peerCert.Raw)

	intermediates := x509.NewCertPool()
	for i := 1; i < len(certs); i++ {
		intermediates.AddCert(certs[i])
		rawDERs = append(rawDERs, certs[i].Raw)
	}

	_, err = peerCert.Verify(x509.VerifyOptions{
		Intermediates: intermediates,
		Roots:         trustedRootCAs,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not verify peer certificate")
	}

	if len(peerCert.Subject.Organization) == 0 {
		return nil, fmt.Errorf("subject organization missing in certificate")
	}

	p, err := NewPeer(&NewPeerArgs{
		ID:   peerCert.Subject.SerialNumber,
		Name: peerCert.Subject.Organization[0],
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create valid peer from certificate details")
	}

	thumbprint, err := newCertificateThumbprintFromCert(peerCert)
	if err != nil {
		return nil, errors.Wrap(err, "could not create thumbprint for certificate")
	}

	keyType, err := newKeyType(peerCert)
	if err != nil {
		return nil, errors.Wrap(err, "could not get key type for peer certificate")
	}

	return &PeerCertificate{
		thumbprint:        thumbprint,
		cert:              peerCert,
		intermediateCerts: intermediates,
		peer:              p,
		raw:               cert,
		rawDERs:           rawDERs,
		keyType:           keyType,
	}, nil
}

func newKeyType(c *x509.Certificate) (CertKeyType, error) {
	switch c.PublicKeyAlgorithm {
	case x509.RSA:
		return CertKeyTypeRSA, nil
	case x509.ECDSA:
		return CertKeyTypeEC, nil
	default:
		return CertKeyTypeUnspecified, fmt.Errorf("invalid certificate key type: %s", c.PublicKeyAlgorithm.String())
	}
}

func NewCertificateThumbprints(thumbprints []string) (CertificateThumbprints, error) {
	if len(thumbprints) == 0 {
		return nil, fmt.Errorf("at least one thumbprint must be specified")
	}

	if duplicateInList(thumbprints) {
		return nil, fmt.Errorf("thumbprints must be unique")
	}

	modelCertThumbprints := make(CertificateThumbprints, len(thumbprints))

	for i, f := range thumbprints {
		fp, err := NewCertificateThumbprint(f)
		if err != nil {
			return nil, err
		}

		modelCertThumbprints[i] = fp
	}

	return modelCertThumbprints, nil
}

func NewCertificateThumbprint(fp string) (CertificateThumbprint, error) {
	if fp == "" {
		return "", fmt.Errorf("certificate thumbprint cannot be empty")
	}

	f, err := base64.RawURLEncoding.DecodeString(fp)
	if err != nil {
		return "", errors.Wrap(err, "could not decode base64 encoded certificate thumbprint")
	}

	if len(f) != certificateThumbprintLength {
		return "", fmt.Errorf("decoded certificate thumbprint must be exactly %d bytes long", certificateThumbprintLength)
	}

	return CertificateThumbprint(fp), nil
}

func (p CertificateThumbprint) Value() string {
	return string(p)
}

func (p CertificateThumbprints) Value() []string {
	pbs := make([]string, len(p))
	for i, publicKey := range p {
		pbs[i] = publicKey.Value()
	}

	return pbs
}

func (p *PeerCertificate) PublicKey() crypto.PublicKey {
	return p.cert.PublicKey
}

func (p *PeerCertificate) CertificateThumbprint() CertificateThumbprint {
	return p.thumbprint
}

func (p CertificateThumbprint) IsEqual(other CertificateThumbprint) bool {
	return p.Value() == other.Value()
}

func (p *PeerCertificate) Peer() *Peer {
	return p.peer
}

func (p *PeerCertificate) Raw() []byte {
	return p.raw
}

func (p *PeerCertificate) KeyType() CertKeyType {
	return p.keyType
}

func (p *PeerCertificate) RawDERs() [][]byte {
	return p.rawDERs
}

func (p PeersCertificates) GetPeerCertificate(pf CertificateThumbprint) (*PeerCertificate, error) {
	for _, certs := range p {
		c, err := certs.GetCertificate(pf)
		if err != nil {
			continue
		}

		return c, nil
	}

	return nil, fmt.Errorf("no peer certificate found for certificate thumbprint: %s", pf)
}

func (p PeerCertificates) GetCertificate(tp CertificateThumbprint) (*PeerCertificate, error) {
	c, ok := p[tp]
	if !ok {
		return nil, fmt.Errorf("no peer certificate found for certificate thumbprint: %s", tp)
	}

	return c, nil
}

func newCertificateThumbprintFromCert(cert *x509.Certificate) (CertificateThumbprint, error) {
	str := common_tls.X509CertificateThumbprint(cert)

	return NewCertificateThumbprint(str)
}
