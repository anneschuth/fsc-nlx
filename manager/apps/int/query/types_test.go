// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"fmt"
	"log"
	"testing"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/adapters/manager"
	"go.nlx.io/nlx/manager/domain/contract"
	mock_repository "go.nlx.io/nlx/manager/domain/contract/mock"
)

type mockManager struct {
	address string
}

type mockManagerFactory struct {
}

func (m *mockManager) GetCertificates(ctx context.Context) (*contract.PeerCertificates, error) {
	panic(fmt.Sprintf("unintended call to GetCertificates %s", m.address))
}

func (m *mockManager) GetPeers(ctx context.Context, peerIDs []contract.PeerID) ([]*contract.Peer, error) {
	if ctx.Value(errorKey) != nil {
		return nil, errors.New("kabooom!")
	}

	peerOne, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             "1",
		Name:           "PeerOne",
		ManagerAddress: "https://manager.peer-one.example.com:443",
	})
	if err != nil {
		log.Fatalf("failed to crate new test peer %s", err)
	}

	peerTwo, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             "2",
		Name:           "PeerTwo",
		ManagerAddress: "https://manager.peer-two.example.com:443",
	})
	if err != nil {
		log.Fatalf("failed to crate new test peer %s", err)
	}

	peerThree, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             "3",
		Name:           "PeerThree",
		ManagerAddress: "https://manager.peer-three.example.com:443",
	})
	if err != nil {
		log.Fatalf("failed to create new test peer %s", err)
	}

	return []*contract.Peer{
		peerOne,
		peerTwo,
		peerThree,
	}, nil
}

func (m *mockManager) Announce(_ context.Context) error {
	panic(fmt.Sprintf("unintended call to announce %s", m.address))
}

func (m *mockManager) GetContracts(_ context.Context, _ *contract.GrantType, _ *[]string) ([]*contract.Contract, error) {
	panic(fmt.Sprintf("unintended call to get contracts %s", m.address))
}

func (m *mockManager) SubmitContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return nil
}

func (m *mockManager) AcceptContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	panic(fmt.Sprintf("unintended call to accept contract %s", m.address))
}

func (m *mockManager) RejectContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	panic(fmt.Sprintf("unintended call to reject contract %s", m.address))
}

func (m *mockManager) RevokeContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	panic(fmt.Sprintf("unintended call to revoke contract %s", m.address))
}

func (m *mockManager) GetPeerInfo(_ context.Context) (*contract.Peer, error) {
	panic(fmt.Sprintf("unintended call to get peer info %s", m.address))
}

func (m *mockManager) GetServices(_ context.Context, _ *contract.PeerID, _ string) ([]*contract.Service, error) {
	panic(fmt.Sprintf("unintended call to get services %s", m.address))
}

func (m *mockManager) GetToken(_ context.Context, _ string) (string, error) {
	panic(fmt.Sprintf("unintended call to get token %s", m.address))
}

func (m *mockManager) GetTXLogRecords(_ context.Context) (contract.TXLogRecords, error) {
	panic(fmt.Sprintf("unintended call to get txlog records %s", m.address))
}

func (m *mockManagerFactory) New(_ context.Context, address string) (manager.Manager, error) {
	return &mockManager{
		address: address,
	}, nil
}

type mocks struct {
	repository     *mock_repository.MockRepository
	managerFactory manager.Factory
	manager        manager.Manager
}

func newMocks(t *testing.T) *mocks {
	testManagerFactory := &mockManagerFactory{}
	testManager, _ := testManagerFactory.New(context.Background(), "https://test-manager.example.com:443")

	return &mocks{
		repository:     mock_repository.NewMockRepository(t),
		managerFactory: testManagerFactory,
		manager:        testManager,
	}
}
