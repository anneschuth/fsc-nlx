/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package query_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/manager/apps/int/query"
)

// nolint:funlen,dupl // these tests do not fit in 100 lines
func TestListPeers(t *testing.T) {
	t.Parallel()

	mocks := newMocks(t)

	h, err := query.NewListPeersHandler(mocks.managerFactory, "https://test-manager.example.com:443")
	assert.NoError(t, err)

	ctx := context.Background()

	response, err := h.Handle(ctx)
	assert.NoError(t, err)

	assert.Len(t, response, 3)

	expected := query.ListPeersPeers{
		{
			ID:   "1",
			Name: "PeerOne",
		},
		{
			ID:   "2",
			Name: "PeerTwo",
		},
		{
			ID:   "3",
			Name: "PeerThree",
		},
	}

	assert.Equal(t, expected, response)
}

func TestListPeersNoManager(t *testing.T) {
	t.Parallel()

	_, err := query.NewListPeersHandler(nil, "https://test-manager.example.com:443")
	assert.Error(t, err)
	assert.ErrorContains(t, err, "managerFactory is required")
}

func TestListPeersNoManagerAddress(t *testing.T) {
	t.Parallel()

	mocks := newMocks(t)

	_, err := query.NewListPeersHandler(mocks.managerFactory, "")
	assert.Error(t, err)
	assert.ErrorContains(t, err, "directoryPeerManagerAddress is required")
}

type errorFlag string

const (
	errorKey errorFlag = "errorFlag"
)

func TestListPeersReturnsError(t *testing.T) {
	t.Parallel()

	mocks := newMocks(t)

	h, err := query.NewListPeersHandler(mocks.managerFactory, "https://test-broken-manager.example.com:443")
	assert.NoError(t, err)

	ctx := context.Background()
	ctx = context.WithValue(ctx, errorKey, true)

	response, err := h.Handle(ctx)

	assert.Nil(t, response)

	assert.ErrorContains(t, err, "could not retrieve peers from directory")
}
