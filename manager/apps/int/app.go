// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package internalapp

import (
	"go.nlx.io/nlx/manager/apps/int/command"
	"go.nlx.io/nlx/manager/apps/int/query"
)

type Application struct {
	Queries  Queries
	Commands Commands
}

type Queries struct {
	GetToken              *query.GetTokenHandler
	GetServiceEndpointURL *query.GetServiceEndpointURLHandler
	GetServicesForOutway  *query.GetServicesForOutwayHandler
	GetCertificate        *query.GetCertificateHandler
	GetContracts          *query.ListContractsHandler
	GetServices           *query.GetServicesHandler
	GetTXLogRecords       *query.ListTXLogRecordsHandler
	GetPeerInfo           *query.GetPeerInfoHandler
	ListPeers             *query.ListPeersHandler
}

type Commands struct {
	AnnouncePeer      *command.AnnouncePeerHandler
	CreateContract    *command.CreateContractHandler
	AcceptContract    *command.AcceptContractHandler
	RejectContract    *command.RejectContractHandler
	RevokeContract    *command.RevokeContractHandler
	CreateCertificate *command.CreateCertificateHandler
}
