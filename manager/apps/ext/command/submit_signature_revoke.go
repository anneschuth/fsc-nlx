// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package command

import (
	"context"
	"crypto/x509"
	"fmt"
	"go.nlx.io/nlx/common/logger"

	"github.com/pkg/errors"
	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/manager/internal/peers"

	app_errors "go.nlx.io/nlx/manager/apps/ext/errors"
	"go.nlx.io/nlx/manager/domain/contract"
)

type SubmitRevokeSignatureHandler struct {
	groupID        string
	selfPeerID     contract.PeerID
	trustedRootCAs *x509.CertPool
	clock          clock.Clock
	repository     contract.Repository
	logger         *logger.Logger
	peers          *peers.Peers
}

type NewRevokeContractHandlerArgs struct {
	GroupID        string
	SelfPeerID     contract.PeerID
	TrustedRootCAs *x509.CertPool
	Repository     contract.Repository
	Peers          *peers.Peers
	Logger         *logger.Logger
	Clock          clock.Clock
}

func NewRevokeContractHandler(args *NewRevokeContractHandlerArgs) (*SubmitRevokeSignatureHandler, error) {
	if args.GroupID == "" {
		return nil, fmt.Errorf("groupID is required")
	}

	if args.TrustedRootCAs == nil {
		return nil, errors.New("trustedRootCAs is required")
	}

	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.Peers == nil {
		return nil, errors.New("peers is required")
	}

	return &SubmitRevokeSignatureHandler{
		groupID:        args.GroupID,
		selfPeerID:     args.SelfPeerID,
		trustedRootCAs: args.TrustedRootCAs,
		repository:     args.Repository,
		clock:          args.Clock,
		logger:         args.Logger,
		peers:          args.Peers,
	}, nil
}

type HandleRevokeContractArgs struct {
	ContractContent *ContractContentArgs
	Signature       string
	SubmittedByPeer *PeerArgs
}

func (h *SubmitRevokeSignatureHandler) Handle(ctx context.Context, args *HandleRevokeContractArgs) error {
	submittedByPeer, err := peerToModel(args.SubmittedByPeer)
	if err != nil {
		return app_errors.NewIncorrectInputError(err)
	}

	err = h.repository.UpsertPeer(ctx, submittedByPeer)
	if err != nil {
		return errors.Wrapf(err, "could not upsert peer: %s into repository", submittedByPeer.ID())
	}

	content, err := contentToModel(args.ContractContent)
	if err != nil {
		return app_errors.NewIncorrectInputError(err)
	}

	if content.GroupID() != h.groupID {
		return app_errors.NewIncorrectInputError(fmt.Errorf("wrong group ID in contract, want: %q, got: %q", h.groupID, content.GroupID()))
	}

	if !content.ContainsPeer(submittedByPeer.ID()) {
		return app_errors.NewIncorrectInputError(fmt.Errorf("submitting peer: %s is not found in contract", submittedByPeer.ID()))
	}

	if !content.ContainsPeer(h.selfPeerID) {
		return app_errors.NewIncorrectInputError(fmt.Errorf("self peer: %s is not found in contract", h.selfPeerID))
	}

	CertificateThumbprint, err := getCertificateThumbprintFromSignature(args.Signature)
	if err != nil {
		return app_errors.NewIncorrectInputError(fmt.Errorf("could not get certificate thumbprint from signature: %s.%s", args.Signature, err))
	}

	certsToRequest := make(map[contract.PeerID][]string)
	certsToRequest[submittedByPeer.ID()] = []string{CertificateThumbprint}

	certs, err := h.peers.GetCertificates(ctx, certsToRequest)
	if err != nil {
		h.logger.Error("unable to retrieve certificate from Peer", err)
		return err
	}

	sig, err := contract.NewSignature(
		&contract.NewSignatureArgs{
			SigType:    contract.SignatureTypeRevoke,
			Signature:  args.Signature,
			Content:    content,
			PeersCerts: certs,
		})
	if err != nil {
		return app_errors.NewIncorrectInputError(err)
	}

	if !sig.Peer().IsEqual(submittedByPeer) {
		return app_errors.NewIncorrectInputError(fmt.Errorf("signature submitted by peer: %s, but signature is signed by peer: %s", submittedByPeer.ID(), sig.Peer().ID().Value()))
	}

	err = h.repository.UpsertContent(ctx, content)
	if err != nil {
		h.logger.Error("could not upsert contract", err)
		return err
	}

	err = h.repository.UpsertSignature(ctx, sig)
	if err != nil {
		h.logger.Error("could not upsert signature", err)
		return err
	}

	return nil
}
