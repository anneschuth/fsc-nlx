// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package query

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/domain/contract"
)

type GetPeerInfoHandler struct{}

type GetPeerInfoHandlerArgs struct {
	SelfPeerID             string
	SelfPeerName           string
	SelfPeerManagerAddress string
}

func NewGetPeerInfoHandler() (*GetPeerInfoHandler, error) {
	return &GetPeerInfoHandler{}, nil
}

func (h *GetPeerInfoHandler) Handle(ctx context.Context, args *GetPeerInfoHandlerArgs) (*PeerInfo, error) {
	p, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             args.SelfPeerID,
		Name:           args.SelfPeerName,
		ManagerAddress: args.SelfPeerManagerAddress,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not get peer info, invalid peer args")
	}

	return &PeerInfo{
		Peer: &Peer{
			ID:             p.ID().Value(),
			Name:           p.Name().Value(),
			ManagerAddress: p.ManagerAddress().Value(),
		},
		FscCoreVersion: FSCCoreVersion1_0_0,
		EnabledExtensions: []*Extension{
			{
				Type:    ExtensionTypeDelegation,
				Version: FSCDelegationVersion1_0_0,
			},
			{
				Type:    ExtensionTypeLogging,
				Version: FSCLoggingVersion1_0_0,
			},
		},
	}, nil
}
