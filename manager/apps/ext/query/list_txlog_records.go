// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"time"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"go.nlx.io/nlx/manager/adapters/txlog"
	"go.nlx.io/nlx/txlog-api/domain/record"
)

type ListTransactionLogRecordsHandler struct {
	repository txlog.TXLog
	groupID    string
}

func NewListTransactionLogRecordsHandler(repository txlog.TXLog, groupID string) (*ListTransactionLogRecordsHandler, error) {
	if repository == nil {
		return nil, fmt.Errorf("repository is required")
	}

	if groupID == "" {
		return nil, fmt.Errorf("groupID is required")
	}

	return &ListTransactionLogRecordsHandler{
		repository: repository,
		groupID:    groupID,
	}, nil
}

type Pagination struct {
	StartID   string
	Limit     uint32
	SortOrder SortOrder
}

type Period struct {
	Start time.Time
	End   time.Time
}

type Filter struct {
	Period        *Period
	TransactionID string
	ServiceName   string
	GrantHash     string
	PeerID        string
}

type NewListTransactionLogRecordsHandlerArgs struct {
	ConnectingPeerID string
	Pagination       *Pagination
	Filters          []*Filter
}

type Record struct {
	TransactionID string
	GrantHash     string
	ServiceName   string
	Direction     record.Direction
	Source        interface{}
	Destination   interface{}
	CreatedAt     time.Time
}

type RecordSource struct {
	OutwayPeerID string
}

type RecordDelegatedSource struct {
	OutwayPeerID    string
	DelegatorPeerID string
}

type RecordDestination struct {
	ServicePeerID string
}

type RecordDelegatedDestination struct {
	ServicePeerID   string
	DelegatorPeerID string
}

func (h *ListTransactionLogRecordsHandler) Handle(ctx context.Context, args *NewListTransactionLogRecordsHandlerArgs) ([]*Record, error) {
	req, err := reqToRepo(args)
	if err != nil {
		return nil, errors.Wrap(err, "invalid request in transcation log records handler")
	}

	req.GroupID = h.groupID

	// The connecting peer may only see the records he participates in
	req.Filters = append(req.Filters, &txlog.Filter{
		PeerID: args.ConnectingPeerID,
	})

	records, err := h.repository.ListRecords(ctx, req)
	if err != nil {
		return nil, errors.Wrap(err, "could not get records from txlog repository")
	}

	return respToRecords(records)
}

func respToRecords(recs []*txlog.Record) ([]*Record, error) {
	records := make([]*Record, len(recs))

	for i, r := range recs {
		var src interface{}
		switch s := r.Source.(type) {
		case *txlog.RecordSource:
			src = &RecordSource{
				OutwayPeerID: s.OutwayPeerID,
			}
		case *txlog.RecordDelegatedSource:
			src = &RecordDelegatedSource{
				OutwayPeerID:    s.OutwayPeerID,
				DelegatorPeerID: s.DelegatorPeerID,
			}
		default:
			return nil, fmt.Errorf("unknown source type: %T", s)
		}

		var dest interface{}
		switch d := r.Destination.(type) {
		case *txlog.RecordDestination:
			dest = &RecordDestination{
				ServicePeerID: d.ServicePeerID,
			}
		case *txlog.RecordDelegatedDestination:
			dest = &RecordDelegatedDestination{
				ServicePeerID:   d.ServicePeerID,
				DelegatorPeerID: d.DelegatorPeerID,
			}
		default:
			return nil, fmt.Errorf("unknown destination type: %T", d)
		}

		records[i] = &Record{
			TransactionID: r.TransactionID,
			GrantHash:     r.GrantHash,
			Direction:     r.Direction,
			ServiceName:   r.ServiceName,
			Source:        src,
			Destination:   dest,
			CreatedAt:     r.CreatedAt,
		}
	}

	return records, nil
}

func reqToRepo(req *NewListTransactionLogRecordsHandlerArgs) (*txlog.ListRecordsRequest, error) {
	var startID uuid.UUID

	if req.Pagination.StartID != "" {
		var err error

		startID, err = uuid.FromString(req.Pagination.StartID)
		if err != nil {
			return nil, fmt.Errorf("invalid startID in pagination: %w", err)
		}
	}

	filters := make([]*txlog.Filter, len(req.Filters))

	for i, f := range req.Filters {
		var id *record.TransactionID

		if f.TransactionID != "" {
			var err error

			id, err = record.NewTransactionIDFromString(f.TransactionID)
			if err != nil {
				return nil, errors.Wrap(err, "invalid transaction ID in filter")
			}
		}

		filter := &txlog.Filter{
			TransactionID: id,
			ServiceName:   f.ServiceName,
			GrantHash:     f.GrantHash,
			PeerID:        f.PeerID,
		}

		if f.Period != nil {
			filter.Period = &txlog.Period{
				Start: f.Period.Start,
				End:   f.Period.End,
			}
		}

		filters[i] = filter
	}

	return &txlog.ListRecordsRequest{
		Pagination: &txlog.Pagination{
			StartID:   startID,
			Limit:     req.Pagination.Limit,
			SortOrder: sortOrderToProto(req.Pagination.SortOrder),
		},
		Filters: filters,
	}, nil
}

func sortOrderToProto(o SortOrder) txlog.SortOrder {
	switch o {
	case SortOrderAscending:
		return txlog.SortOrderAscending
	case SortOrderDescending:
		return txlog.SortOrderDescending
	default:
		return txlog.SortOrderUnspecified
	}
}
