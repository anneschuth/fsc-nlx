// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"time"
)

type HashAlg int32

const (
	HashAlgUnspecified HashAlg = iota
	HashAlgSHA3_512
)

type Contract struct {
	ID                                string
	GroupID                           string
	HashAlgorithm                     HashAlg
	ContractNotBefore                 time.Time
	ContractNotAfter                  time.Time
	CreatedAt                         time.Time
	PeerRegistrationGrants            []*PeerRegistrationGrant
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
	SignaturesAccepted                map[string]string
	SignaturesRevoked                 map[string]string
	SignaturesRejected                map[string]string
}

type PeerRegistrationGrant struct {
	Hash               string
	DirectoryPeerID    string
	PeerID             string
	PeerName           string
	PeerManagerAddress string
}

type ServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

type ServiceConnectionGrant struct {
	Hash                        string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
	ServiceName                 string
	ServicePeerID               string
}

type DelegatedServiceConnectionGrant struct {
	Hash                        string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
	ServiceName                 string
	ServicePeerID               string
	DelegatorPeerID             string
}

type DelegatedServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
	DelegatorPeerID string
}

type CertKeyType int

const (
	CertKeyTypeUnspecified CertKeyType = iota
	CertKeyTypeEC
	CertKeyTypeRSA
)

type Certificates map[string]*Certificate

type Certificate struct {
	KeyType CertKeyType
	RawDERs [][]byte
}

type SortOrder string

const (
	SortOrderAscending  SortOrder = "asc"
	SortOrderDescending SortOrder = "desc"
)

type GrantType string

const (
	GrantTypeUnspecified                 GrantType = "unspecified"
	GrantTypePeerRegistration            GrantType = "peer_registration"
	GrantTypeServicePublication          GrantType = "service_publication"
	GrantTypeServiceConnection           GrantType = "service_connection"
	GrantTypeDelegatedServiceConnection  GrantType = "delegated_service_connection"
	GrantTypeDelegatedServicePublication GrantType = "delegated_service_publication"
)

type Peer struct {
	ID             string
	Name           string
	ManagerAddress string
}

type Service struct {
	PeerID             string
	PeerManagerAddress string
	PeerName           string
	DelegatorPeerID    string
	DelegatorPeerName  string
	Name               string
	ContractID         string
}

type ServiceFilter struct {
	PeerID      string
	ServiceName string
}

type FSCCoreVersion int32

const (
	FSCCoreVersionUnspecified FSCCoreVersion = iota
	FSCCoreVersion1_0_0
)

type FSCDelegationVersion int32

const (
	FSCDelegationVersionUnspecified FSCDelegationVersion = iota
	FSCDelegationVersion1_0_0
)

type FSCLoggingVersion int32

const (
	FSCLoggingVersionUnspecified FSCLoggingVersion = iota
	FSCLoggingVersion1_0_0
)

type ExtensionType int32

const (
	ExtensionTypeUnspecified ExtensionType = iota
	ExtensionTypeDelegation
	ExtensionTypeLogging
)

type Extension struct {
	Type    ExtensionType
	Version interface{}
}

type PeerInfo struct {
	Peer              *Peer
	FscCoreVersion    FSCCoreVersion
	EnabledExtensions []*Extension
}

type ServiceDetails struct {
	InwayAddress string
	Provider     interface{}
}

type Provider struct {
	PeerID string
}

type DelegatedProvider struct {
	PeerID          string
	DelegatorPeerID string
}

type GetTokenResponse struct {
	Token     string
	TokenType OAuthTokenType
}
