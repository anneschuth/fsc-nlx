// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package peers

import (
	"context"
	"fmt"
	"sync"

	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"

	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/manager/adapters/manager"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/txlog-api/ports/logger"
)

type Peers struct {
	ctx               context.Context
	logger            logger.Logger
	cert              *common_tls.CertificateBundle
	selfPeerID        contract.PeerID
	directoryClient   manager.Manager
	selfManagerClient manager.Manager
	managerFactory    manager.Factory
	repository        contract.Repository
}

type actionType int

const (
	actionTypeAccept actionType = 0
	actionTypeSubmit actionType = iota
	actionTypeReject actionType = iota
	actionTypeRevoke actionType = iota
)

const maxConcurrentCalls = 5

type NewPeersArgs struct {
	Ctx                     context.Context
	Logger                  logger.Logger
	PeerCert                *common_tls.CertificateBundle
	SelfAddress             string
	DirectoryManagerAddress string
	ManagerFactory          manager.Factory
	Repository              contract.Repository
}

func New(args *NewPeersArgs) (*Peers, error) {
	if args.Ctx == nil {
		return nil, fmt.Errorf("ctx cannot be nil")
	}

	if args.Logger == nil {
		return nil, fmt.Errorf("logger cannot be nil")
	}

	if args.PeerCert == nil {
		return nil, fmt.Errorf("cert cannot be nil")
	}

	if args.Repository == nil {
		return nil, fmt.Errorf("repository cannot be nil")
	}

	selfPeerID, err := contract.NewPeerID(args.PeerCert.GetOrganizationInfo().SerialNumber)
	if err != nil {
		return nil, fmt.Errorf("invalid selfPeerID peer id in cert: %v", err)
	}

	directoryClient, err := args.ManagerFactory.New(context.Background(), args.DirectoryManagerAddress)
	if err != nil {
		return nil, errors.Wrap(err, "could not create directory client")
	}

	selfManagerClient, err := args.ManagerFactory.New(context.Background(), args.SelfAddress)
	if err != nil {
		return nil, errors.Wrap(err, "could not create self manager client")
	}

	return &Peers{
		ctx:               args.Ctx,
		logger:            args.Logger,
		cert:              args.PeerCert,
		selfPeerID:        selfPeerID,
		directoryClient:   directoryClient,
		selfManagerClient: selfManagerClient,
		managerFactory:    args.ManagerFactory,
		repository:        args.Repository,
	}, nil
}

func (g *Peers) Announce(ctx context.Context, peers contract.PeersIDs) error {
	peersClients, err := g.getPeersClients(peers)
	if err != nil {
		return errors.Wrap(err, "could not get clients for peers")
	}

	errs := errgroup.Group{}
	errs.SetLimit(maxConcurrentCalls)

	for peerID := range peers {
		peerIDToAnnounceTo := peerID

		errs.Go(func() error {
			if g.selfPeerID == peerIDToAnnounceTo {
				return nil
			}

			peerManagerClient, ok := peersClients[peerIDToAnnounceTo]
			if !ok {
				return fmt.Errorf("could not get client for peer: %s", peerIDToAnnounceTo)
			}

			err := peerManagerClient.Announce(ctx)
			if err != nil {
				return errors.Wrap(err, "could not submit contract")
			}

			return nil
		})
	}

	return errs.Wait()
}

func (g *Peers) SubmitContract(ctx context.Context, contractContent *contract.Content, sig *contract.Signature) error {
	return g.sendToPeers(ctx, contractContent, sig, actionTypeSubmit)
}

func (g *Peers) AcceptContract(ctx context.Context, contractContent *contract.Content, sig *contract.Signature) error {
	return g.sendToPeers(ctx, contractContent, sig, actionTypeAccept)
}

func (g *Peers) RejectContract(ctx context.Context, contractContent *contract.Content, sig *contract.Signature) error {
	return g.sendToPeers(ctx, contractContent, sig, actionTypeReject)
}

func (g *Peers) RevokeContract(ctx context.Context, contractContent *contract.Content, sig *contract.Signature) error {
	return g.sendToPeers(ctx, contractContent, sig, actionTypeRevoke)
}

func (g *Peers) GetCertificates(ctx context.Context, peerCertificateThumbprints map[contract.PeerID][]string) (contract.PeersCertificates, error) {
	peers := contract.PeersIDs{}

	for peerID := range peerCertificateThumbprints {
		peers[peerID] = true
	}

	peersClients, err := g.getPeersClients(peers)
	if err != nil {
		return nil, fmt.Errorf("could not get clients for peers: %v", err)
	}

	responseLock := sync.Mutex{}
	peersCertificates := contract.PeersCertificates{}

	errs := errgroup.Group{}
	errs.SetLimit(maxConcurrentCalls)

	for peerID := range peerCertificateThumbprints {
		peerIDToFetch := peerID

		errs.Go(func() error {
			peerClient, ok := peersClients[peerIDToFetch]
			if !ok {
				return fmt.Errorf("could not get client for peer: %s", peerIDToFetch)
			}

			peerCertificates, errGetCertificates := peerClient.GetCertificates(ctx)
			if errGetCertificates != nil {
				return errGetCertificates
			}

			responseLock.Lock()
			peersCertificates[peerIDToFetch] = peerCertificates
			responseLock.Unlock()

			return nil
		})
	}

	err = errs.Wait()
	if err != nil {
		return nil, err
	}

	return peersCertificates, nil
}

func (g *Peers) GetCertificate(ctx context.Context, peerID contract.PeerID, certificateThumbprint *contract.CertificateThumbprint) (*contract.PeerCertificate, error) {
	certificates, err := g.GetCertificates(ctx, map[contract.PeerID][]string{
		peerID: {
			certificateThumbprint.Value(),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not get certificate from Peer")
	}

	certs, ok := certificates[peerID]
	if !ok {
		return nil, fmt.Errorf("peer certificate not found")
	}

	cert, err := certs.GetCertificate(*certificateThumbprint)
	if err != nil {
		return nil, errors.Wrap(err, "certificate not found in PeerCertificates")
	}

	return cert, nil
}

func (g *Peers) GetToken(ctx context.Context, peerID contract.PeerID, grantHash string) (string, error) {
	peer, err := g.getPeerClient(peerID)
	if err != nil {
		return "", errors.Wrap(err, "could not get peer client to get token from")
	}

	token, err := peer.GetToken(ctx, grantHash)
	if err != nil {
		return "", errors.Wrapf(err, "could not get token from peer: %s", peerID.Value())
	}

	return token, nil
}

func (g *Peers) GetTXLogRecords(ctx context.Context, peerID contract.PeerID) (contract.TXLogRecords, error) {
	peer, err := g.getPeerClient(peerID)
	if err != nil {
		return nil, errors.Wrap(err, "could not get peer client to get tx log records from")
	}

	records, err := peer.GetTXLogRecords(ctx)
	if err != nil {
		return nil, errors.Wrapf(err, "could not get tx log records from peer: %s", peerID.Value())
	}

	return records, nil
}

func (g *Peers) sendToPeers(ctx context.Context, contractContent *contract.Content, sig *contract.Signature, action actionType) error {
	peers := contractContent.PeersIDs()

	if onlyReceiverIsSelf(peers, g.selfPeerID) {
		return nil
	}

	peersClients, err := g.getPeersClients(peers)
	if err != nil {
		return errors.Wrap(err, "could not get clients for peers")
	}

	errs := errgroup.Group{}
	errs.SetLimit(maxConcurrentCalls)

	for peerID := range peers {
		peerIDToSendTo := peerID

		errs.Go(func() error {
			// we don't have to send the contract to ourselves because we have it already
			if g.selfPeerID == peerIDToSendTo {
				return nil
			}

			peerManagerClient, ok := peersClients[peerIDToSendTo]
			if !ok {
				return fmt.Errorf("could not get client for peer: %s", peerIDToSendTo)
			}

			switch action {
			case actionTypeSubmit:
				errClient := peerManagerClient.SubmitContract(ctx, contractContent, sig)
				if errClient != nil {
					return errors.Wrap(errClient, "could not submit contract")
				}
			case actionTypeAccept:
				errClient := peerManagerClient.AcceptContract(ctx, contractContent, sig)
				if errClient != nil {
					return errors.Wrap(errClient, "could not accept contract")
				}
			case actionTypeRevoke:
				errClient := peerManagerClient.RevokeContract(ctx, contractContent, sig)
				if errClient != nil {
					return errors.Wrap(errClient, "could not accept contract")
				}
			case actionTypeReject:
				errClient := peerManagerClient.RejectContract(ctx, contractContent, sig)
				if errClient != nil {
					return errors.Wrap(errClient, "could not accept contract")
				}
			default:
				return fmt.Errorf("unsupported action type: %d", action)
			}

			return nil
		})
	}

	return errs.Wait()
}

func (g *Peers) getPeersClients(peerIDs contract.PeersIDs) (map[contract.PeerID]manager.Manager, error) {
	if onlyReceiverIsSelf(peerIDs, g.selfPeerID) {
		return map[contract.PeerID]manager.Manager{
			g.selfPeerID: g.selfManagerClient,
		}, nil
	}

	peersIDsToGet := make([]contract.PeerID, len(peerIDs))
	i := 0

	for peerID := range peerIDs {
		peersIDsToGet[i] = peerID
		i++
	}

	peers, err := g.directoryClient.GetPeers(context.Background(), peersIDsToGet)
	if err != nil {
		return nil, errors.Wrap(err, "could not get peers from directory")
	}

	clientsToReturn := make(map[contract.PeerID]manager.Manager)

	for _, peer := range peers {
		peerClient, err := g.managerFactory.New(context.Background(), peer.ManagerAddress().Value())
		if err != nil {
			return nil, errors.Wrap(err, "could not create peer client")
		}

		errUpsertPeer := g.repository.UpsertPeer(g.ctx, peer)
		if errUpsertPeer != nil {
			g.logger.Warn("could not upsert peer", errUpsertPeer)
		}

		clientsToReturn[peer.ID()] = peerClient
	}

	return clientsToReturn, nil
}

func (g *Peers) getPeerClient(peerID contract.PeerID) (manager.Manager, error) {
	if peerID == g.selfPeerID {
		return g.selfManagerClient, nil
	}

	clients, err := g.getPeersClients(contract.PeersIDs{peerID: true})
	if err != nil {
		return nil, errors.Wrap(err, "could not get peer client")
	}

	c, ok := clients[peerID]
	if !ok {
		return nil, fmt.Errorf("could not get peer client for peer: %s", peerID.Value())
	}

	return c, nil
}

type Service struct {
	InwayAddress string
	Provider     interface{}
}

type Provider struct {
	PeerID string
}

type DelegatedProvider struct {
	PeerID          string
	DelegatorPeerID string
}

func onlyReceiverIsSelf(peers contract.PeersIDs, selfPeerID contract.PeerID) bool {
	return len(peers) == 1 && peers[selfPeerID]
}
