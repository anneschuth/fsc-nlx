// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"

	managerapi "go.nlx.io/nlx/manager/ports/int/grpc/api"
)

const oneYear = time.Hour * 24 * 265

func parseRequestAccessBodyBody(r *http.Request) RequestAccessToServiceFields {
	decoder := json.NewDecoder(r.Body)

	var t RequestAccessToServiceFields

	err := decoder.Decode(&t)
	if err != nil {
		panic(err)
	}

	return t
}

func requestAccessToService(w http.ResponseWriter, r *http.Request) {
	requestBody := parseRequestAccessBodyBody(r)
	client := setupInternalManagerClient(requestBody.ManagerAddress)

	id := uuid.New()

	response, err := client.CreateContract(context.TODO(), &managerapi.CreateContractRequest{
		ContractContent: &managerapi.ContractContent{
			Id:      id[:],
			GroupId: "https://directory.shared.nlx.local:8443",
			Validity: &managerapi.Validity{
				NotBefore: time.Now().Unix(),
				NotAfter:  time.Now().Add(oneYear).Unix(),
			},
			Grants: []*managerapi.Grant{
				{
					Data: &managerapi.Grant_ServiceConnection{
						ServiceConnection: &managerapi.GrantServiceConnection{
							Outway: &managerapi.GrantServiceConnection_Outway{
								PeerId:                requestBody.OutwayPeerID,
								CertificateThumbprint: requestBody.OutwayCertificateThumbprint,
							},
							Service: &managerapi.GrantServiceConnection_Service{
								PeerId: requestBody.ServicePeerID,
								Name:   requestBody.ServiceName,
							},
						},
					},
				},
			},
			HashAlgorithm: managerapi.HashAlgorithm_HASH_ALGORITHM_SHA3_512,
			CreatedAt:     time.Now().Unix(),
		},
	})

	if err != nil {
		fmt.Fprintf(w, "failed to create contract error: %s", err)
		return
	}

	fmt.Fprintf(w, response.ContentHash)
}
