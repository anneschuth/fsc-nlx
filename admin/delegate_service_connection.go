// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"

	managerapi "go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func parseDelegateServiceConnectionBody(r *http.Request) DelegateServiceConnectionFields {
	decoder := json.NewDecoder(r.Body)

	var t DelegateServiceConnectionFields

	err := decoder.Decode(&t)
	if err != nil {
		panic(err)
	}

	return t
}

func delegateServiceConnection(w http.ResponseWriter, r *http.Request) {
	requestBody := parseDelegateServiceConnectionBody(r)
	client := setupInternalManagerClient(requestBody.ManagerAddress)

	id := uuid.New()

	response, err := client.CreateContract(context.TODO(), &managerapi.CreateContractRequest{
		ContractContent: &managerapi.ContractContent{
			HashAlgorithm: managerapi.HashAlgorithm_HASH_ALGORITHM_SHA3_512,
			Id:            id[:],
			GroupId:       "https://directory.shared.nlx.local:8443",
			Validity: &managerapi.Validity{
				NotBefore: time.Now().Unix(),
				NotAfter:  time.Now().Add(oneYear).Unix(),
			},
			Grants: []*managerapi.Grant{
				{
					Data: &managerapi.Grant_DelegatedServiceConnection{
						DelegatedServiceConnection: &managerapi.GrantDelegatedServiceConnection{
							Outway: &managerapi.GrantDelegatedServiceConnection_Outway{
								PeerId:                requestBody.OutwayPeerID,
								CertificateThumbprint: requestBody.OutwayCertificateThumbprint,
							},
							Service: &managerapi.GrantDelegatedServiceConnection_Service{
								PeerId: requestBody.ServicePeerID,
								Name:   requestBody.ServiceName,
							},
							Delegator: &managerapi.GrantDelegatedServiceConnection_Delegator{PeerId: requestBody.Delegator},
						},
					},
				},
			},
		},
	})
	if err != nil {
		fmt.Fprintf(w, "failed to create contract error: %s", err)

		return
	}

	fmt.Fprintf(w, response.ContentHash)
}
