// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package discardlogger

import (
	"io"
	"log/slog"

	"go.nlx.io/nlx/common/logger"
)

func New() *logger.Logger {
	discardHandlerConfig := slog.NewJSONHandler(io.Discard, nil)

	return &logger.Logger{
		Logger: slog.New(discardHandlerConfig),
	}
}
