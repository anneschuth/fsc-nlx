apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "nlx-outway.fullname" . }}
  labels:
    {{- include "nlx-outway.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
  selector:
    matchLabels:
      {{- include "nlx-outway.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "nlx-outway.selectorLabels" . | nindent 8 }}
      annotations:
        checksum/configmap: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
        checksum/secret-tls: {{ include (print $.Template.BasePath "/secret-tls.yaml") . | sha256sum }}
        checksum/secret-tls-server: {{ include (print $.Template.BasePath "/secret-tls-server.yaml") . | sha256sum }}
    spec:
    {{- with .Values.image.pullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      serviceAccountName: {{ include "nlx-outway.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: nlx-outway
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: {{ template "nlx-outway.image" . }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
          {{- if .Values.https.enabled }}
            - name: https
              containerPort: 8443
              protocol: TCP
          {{- else }}
            - name: http
              containerPort: 8080
              protocol: TCP
          {{- end }}
            - name: monitoring
              containerPort: 8081
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /health/live
              port: monitoring
            initialDelaySeconds: 3
            periodSeconds: 10
          readinessProbe:
            httpGet:
              path: /health/ready
              port: monitoring
            initialDelaySeconds: 3
            periodSeconds: 10
          env:
            - name: GROUP_ID
              value: {{ default .Values.config.groupID .Values.global.groupID }}
            - name: OUTWAY_NAME
              value: {{ default (include "nlx-outway.fullname" .) .Values.config.name }}
            - name: CONTROLLER_API_ADDRESS
              value: {{ required "Controller API address is required" .Values.config.controllerApiAddress }}
            - name: MANAGER_INTERNAL_ADDRESS
              value: {{ .Values.config.managerInternalAddress }}
            - name: TLS_ROOT_CERT
              value: /config/root-internal.pem
            - name: TLS_CERT
              value: /certificate-internal/tls.crt
            - name: TLS_KEY
              value: /certificate-internal/tls.key
          {{- if .Values.https.enabled }}
            - name: LISTEN_ADDRESS
              value: 0.0.0.0:8443
            - name: LISTEN_HTTPS
              value: "1"
            - name: TLS_SERVER_CERT
              value: /certificate-server/tls.crt
            - name: TLS_SERVER_KEY
              value: /certificate-server/tls.key
          {{- else }}
            - name: LISTEN_ADDRESS
              value: 0.0.0.0:8080
          {{- end }}
            - name: MONITORING_ADDRESS
              value: 0.0.0.0:8081
          {{- if .Values.config.authorizationService.enabled }}
            - name: AUTHORIZATION_SERVICE_ADDRESS
              value: {{ .Values.config.authorizationService.url }}
            - name: AUTHORIZATION_ROOT_CA
              value: /config/root-internal.pem
          {{- end }}
            - name: TLS_GROUP_ROOT_CERT
              value: /config/root-organization.pem
            - name: TLS_GROUP_CERT
              value: /certificate-group/tls.crt
            - name: TLS_GROUP_KEY
              value: /certificate-group/tls.key
            - name: TX_LOG_API_ADDRESS
              value: {{ required "Transaction Log API address is required" .Values.config.transactionLogApiAddress }}
            - name: LOG_TYPE
              value: {{ .Values.config.logType }}
            - name: LOG_LEVEL
              value: {{ .Values.config.logLevel }}
          volumeMounts:
            - name: certificate-group
              mountPath: /certificate-group
              readOnly: true
          {{- if .Values.https.enabled }}
            - name: certificate-server
              mountPath: /certificate-server
              readOnly: true
          {{- end }}
            - name: certificate-internal
              mountPath: /certificate-internal
            - name: config
              mountPath: /config
              readOnly: true
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      volumes:
        - name: certificate-group
          secret:
            secretName: {{ default (printf "%s-group-tls" (include  "nlx-outway.fullname" .)) .Values.certificates.group.existingSecret }}
            defaultMode: 0640
        - name: certificate-internal
          secret:
            secretName: {{ default (printf "%s-internal-tls" (include "nlx-outway.fullname" .)) .Values.certificates.internal.existingSecret }}
            defaultMode: 0640
        - name: config
          configMap:
            name: {{ template "nlx-outway.fullname" . }}
      {{- if .Values.https.enabled }}
        - name: certificates-server
          secret:
            secretName: {{ template "nlx-outway.fullname" . }}-tls-server
      {{- end }}
    {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
