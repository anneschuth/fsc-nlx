# apps-overview

This is the Chart for the app-overview page. It contains links to all apps within the environment.

## Prerequisites

- Kubernetes 1.11+

## Installing the Chart

This chart is created for internal use only.

## Parameters

### Global parameters

| Name                   | Description                  | Value |
| ---------------------- | ---------------------------- | ----- |
| `global.imageRegistry` | Global Docker Image registry | `""`  |
| `global.imageTag`      | Global Docker Image tag      | `""`  |

### Deployment Parameters

| Name                 | Description                                                                                                                                                                                | Value                     |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------- |
| `image.registry`     | Image registry (ignored if 'global.imageRegistry' is set)                                                                                                                                  | `docker.io`               |
| `image.repository`   | Image repository of the controller API.                                                                                                                                                    | `nlxio/fsc-apps-overview` |
| `image.tag`          | Image tag (ignored if 'global.imageTag' is set). When set to null, the AppVersion from the chart is used                                                                                   | `""`                      |
| `image.pullPolicy`   | Image pull policy                                                                                                                                                                          | `Always`                  |
| `image.pullSecrets`  | Secrets for the image repository                                                                                                                                                           | `[]`                      |
| `replicaCount`       | Number of controller replicas                                                                                                                                                              | `1`                       |
| `podSecurityContext` | Security context for the pod                                                                                                                                                               | `{}`                      |
| `securityContext`    | Optional security context. The YAML block should adhere to the [SecurityContext spec](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.16/#securitycontext-v1-core) | `{}` | `{}`                      |
| `resources`          | Pod resource requests & limits                                                                                                                                                             | `{}`                      |
| `nodeSelector`       | Node labels for pod assignment                                                                                                                                                             | `{}`                      |
| `tolerations`        | Node tolerations for pod assignment                                                                                                                                                        | `[]`                      |
| `affinity`           | Node affinity for pod assignment                                                                                                                                                           | `{}`                      |

### Common Parameters

| Name               | Description                   | Value |
| ------------------ | ----------------------------- | ----- |
| `nameOverride`     | Override deployment name      | `""`  |
| `fullnameOverride` | Override full deployment name | `""`  |

### Apps Overview parameters

| Name                          | Description                                                                                                                              | Value       |
| ----------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `config.reviewSlugWithDomain` | In the review environment, the slug with domain is added as a suffix to the URLs of the websites linked on the review apps overview page | `""`        |
| `config.environmentSubdomain` | For the demo and acceptance environments. This is used to create the URLs of the websites linked on the apps overview page               | `localhost` |
| `serviceAccount.create`       | Specifies whether a service account should be created                                                                                    | `true`      |
| `serviceAccount.annotations`  | Annotations to add to the service account                                                                                                | `{}`        |
| `serviceAccount.name`         | The name of the service account to use. If not set and create is true, a name is generated using the fullname template                   | `""`        |

### Exposure parameters

| Name                  | Description                                       | Value       |
| --------------------- | ------------------------------------------------- | ----------- |
| `service.type`        | Service Type (ClusterIP, NodePort, LoadBalancer)  | `ClusterIP` |
| `service.port`        | Port exposed by the service for the apps-overview | `80`        |
| `ingress.enabled`     | Enable ingress                                    | `false`     |
| `ingress.class`       | Ingress class                                     | `""`        |
| `ingress.annotations` | Ingress annotations                               | `{}`        |
| `ingress.hosts`       | Ingress accepted hostnames                        | `[]`        |
| `ingress.tls`         | Ingress TLS configuration                         | `[]`        |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart.

```console
$ helm install apps-overview -f values.yaml .
```
> **Tip**: You can use the default [values.yaml](https://gitlab.com/commonground/nlx/fsc-nlx/blob/main/helm/charts/apps-overview/values.yaml)
