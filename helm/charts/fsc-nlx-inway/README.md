# inway

NLX is an open source peer-to-peer system facilitating federated authentication, secure connecting and protocolling in a large-scale, dynamic API ecosystem with many organizations.
An organization can provide services to the NLX ecosystem through an Inway. The Inway is usually deployed centrally within the organization, although it is possible for one organization to deploy multiple instances on different locations.

## Prerequisites

-   Kubernetes 1.11+

## Installing the Chart

To install the Chart with the release name `inway`:

```console
## add the Common Ground Helm repository
$ helm repo add commonground https://charts.commonground.nl

## Install the nlx-inway helm Chart
$ helm install inway commonground/nlx-inway
```

> **Tip**: List all releases using `helm list`

## Upgrading the Chart

Currently, our Helm charts use the same release version as the NLX release version.
To know what has changed for the Helm charts, look at the changes in our [CHANGELOG](https://gitlab.com/commonground/nlx/fsc-nlx/-/blob/main/CHANGELOG.md)
that are prefixed with 'Helm'.

## Uninstalling the Chart

To uninstall or delete the `inway` deployment:

```console
$ helm delete inway
```

## Parameters

### Global parameters

| Name                                            | Description                                                                                                     | Value |
| ----------------------------------------------- | --------------------------------------------------------------------------------------------------------------- | ----- |
| `global.imageRegistry`                          | Global Docker Image registry                                                                                    | `""`  |
| `global.imageTag`                               | Global Docker Image tag                                                                                         | `""`  |
| `global.groupID`                                | Global FSC Group ID                                                                                             | `""`  |
| `global.certificates.group.caCertificatePEM`    | Global FSC NLX CA root certificate. If not set the value of 'tls.organization.rootCertificatePEM' is used       | `""`  |
| `global.certificates.internal.caCertificatePEM` | Global root certificate of your internal PKI. If not set the value of 'tls.internal.rootCertificatePEM' is used | `""`  |

### Deployment Parameters

| Name                         | Description                                                                                                                                                                                | Value             |
| ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------------- |
| `image.registry`             | Image registry (ignored if 'global.imageRegistry' is set)                                                                                                                                  | `docker.io`       |
| `image.repository`           | Image repository of the docs API.                                                                                                                                                          | `nlxio/fsc-inway` |
| `image.tag`                  | Image tag (ignored if 'global.imageTag' is set). When set to null, the AppVersion from the chart is used                                                                                   | `""`              |
| `image.pullPolicy`           | Image pull policy                                                                                                                                                                          | `Always`          |
| `image.pullSecrets`          | Secrets for the image repository                                                                                                                                                           | `[]`              |
| `replicaCount`               | Number of controller replicas                                                                                                                                                              | `1`               |
| `serviceAccount.create`      | Specifies whether a service account should be created                                                                                                                                      | `true`            |
| `serviceAccount.annotations` | Annotations to add to the service account                                                                                                                                                  | `{}`              |
| `serviceAccount.name`        | The name of the service account to use. If not set and create is true, a name is generated using the fullname template                                                                     | `""`              |
| `securityContext`            | Optional security context. The YAML block should adhere to the [SecurityContext spec](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.16/#securitycontext-v1-core) | `{}` | `{}`              |
| `podSecurityContext.fsGroup` | GroupID under which the pod should be started                                                                                                                                              | `1001`            |
| `resources`                  | Pod resource requests & limits                                                                                                                                                             | `{}`              |
| `nodeSelector`               | Node labels for pod assignment                                                                                                                                                             | `{}`              |
| `affinity`                   | Node affinity for pod assignment                                                                                                                                                           | `{}`              |
| `tolerations`                | Node tolerations for pod assignment                                                                                                                                                        | `[]`              |

### Common Parameters

| Name               | Description                   | Value |
| ------------------ | ----------------------------- | ----- |
| `nameOverride`     | Override deployment name      | `""`  |
| `fullnameOverride` | Override full deployment name | `""`  |

### FSC NLX Inway parameters

| Name                                  | Description                                                                                                                               | Value   |
| ------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------- | ------- |
| `config.logType`                      | Possible values 'live', 'local'. Affects the log output. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger | `live`  |
| `config.logLevel`                     | Possible values 'debug', 'warn', 'info'. Override the default logLevel set by 'config.logType'                                            | `info`  |
| `config.groupID`                      | FSC Group ID                                                                                                                              | `""`    |
| `config.name`                         | Name of the Inway                                                                                                                         | `""`    |
| `config.selfAddress`                  | The Address that can be used by the FSC NLX network to reach this Inway                                                                   | `""`    |
| `config.managerInternalAddress`       | Internal address of the Manager                                                                                                           | `""`    |
| `config.controllerApiAddress`         | The address of the Controller API                                                                                                         | `""`    |
| `config.transactionLogApiAddress`     | The Address of the Transaction Log API                                                                                                    | `""`    |
| `config.authorizationService.enabled` | If 'true', the Inway will use the authorization service                                                                                   | `false` |
| `config.authorizationService.url`     | URL of the authorization service to use                                                                                                   | `""`    |

### TLS certificates used by FSC NLX components for communications

| Name                                     | Description                                                                                                                                                                                    | Value |
| ---------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----- |
| `certificates.group.caCertificatePEM`    | The CA certificate of the Group                                                                                                                                                                | `""`  |
| `certificates.group.certificatePEM`      | The Group certificate                                                                                                                                                                          | `""`  |
| `certificates.group.keyPEM`              | Private Key of 'certificates.group.certificatePEM'                                                                                                                                             | `""`  |
| `certificates.group.existingSecret`      | Use existing secret with your FSC NLX keypair (`certificates.group.certificatePEM` and `certificates.group.keyPEM` will be ignored and picked up from the secret)                              | `""`  |
| `certificates.internal.caCertificatePEM` | The CA root certificate of your internal PKI                                                                                                                                                   | `""`  |
| `certificates.internal.certificatePEM`   | The certificate signed by your internal PKI                                                                                                                                                    | `""`  |
| `certificates.internal.keyPEM`           | the private key of 'certificates.internal.certificatePEM'                                                                                                                                      | `""`  |
| `certificates.internal.existingSecret`   | Use of existing secret with your FSC NLX keypair ('certificates.internal.certificatePEM' and 'certificates.internal.keyPEM'. will be ingored and picked up from this secret)                   | `""`  |
| `certificates.serviceCA.certificatePEM`  | If a service offered by this Inway uses certificates signed by your own CA you can use this value to mount the CA certificate on the Inway so the Inway is able to validate the TLS connection | `""`  |

### Exposure parameters

| Name                     | Description                                                                                                                                                                                                                                                                                                                                      | Value          |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------- |
| `service.type`           | Service Type (ClusterIP, NodePort, LoadBalancer)                                                                                                                                                                                                                                                                                                 | `LoadBalancer` |
| `service.annotations`    | Annotations to be included in the                                                                                                                                                                                                                                                                                                                | `{}`           |
| `service.port`           | Port exposed by the service                                                                                                                                                                                                                                                                                                                      | `443`          |
| `service.nodePort`       | Port exposed for Inway traffic if 'service.type' is 'NodePort'                                                                                                                                                                                                                                                                                   | `443`          |
| `service.loadBalancerIP` | Only applies when using 'service.type' 'LoadBalancer'. A loadBalancer will get created with the IP specified in this field. This feature depends on whether the underlying cloud-provider supports specifying the loadbalancerIP when a load balancer is created. This field will be ignored if the cloud-provider does not support the feature. | `""`           |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart.

```console
$ helm install inway -f values.yaml .
```

> **Tip**: You can use the default [values.yaml](https://gitlab.com/commonground/nlx/fsc-nlx/blob/main/helm/charts/nlx-inway/values.yaml)
