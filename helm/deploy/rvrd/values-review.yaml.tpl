###########
## Chart ##
###########
global:
  groupID: "fsc-default"

postgresql:
  storageSize: 256Mi

opa:
  enabled: true

################
## Sub-charts ##
################
fsc-nlx-manager:
  config:
    selfAddress: "https://rvrd-fsc-nlx-manager-external:8443"
    directoryPeerManagerAddress: "https://shared-fsc-nlx-manager-external:8443"

fsc-nlx-controller:
  config:
    directoryAddress: "https://shared-fsc-nlx-manager-external:8443"
    authn:
      oidc:
        discoveryUrl: https://dex-rvrd-{{DOMAIN_SUFFIX}}
        redirectUrl: https://nlx-controller-rvrd-{{DOMAIN_SUFFIX}}/oidc/callback
  ingress:
    hosts:
      - "nlx-controller-rvrd-{{DOMAIN_SUFFIX}}"

dex:
  config:
    issuer: https://dex-rvrd-{{DOMAIN_SUFFIX}}
    staticClients:
      - id: nlx-controller
        name: NLX Controller
        secret: 99DbIk7FqlUYqbyD3qSX4Wmf
        redirectURIs:
          - https://nlx-controller-rvrd-{{DOMAIN_SUFFIX}}/oidc/callback
  ingress:
    hosts:
      - host: dex-rvrd-{{DOMAIN_SUFFIX}}
        paths:
          - path: /
            pathType: ImplementationSpecific
