package httpapi.inway

default allow = false

# Allow organizations with enough budget left
allow {
    peerId := input.headers["X-Nlx-Request-Organization"][0]

    data.organizations[peerId].budget_left >= input.service.request_costs
}
